package com.legacy.nethercraft.item;

import javax.annotation.Nullable;

import net.minecraft.core.Direction;
import net.minecraft.core.Position;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.entity.projectile.Arrow;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.item.ArrowItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class NetherArrowItem extends ArrowItem
{
	public NetherArrowItem(Item.Properties builder)
	{
		super(builder);
	}

	public AbstractArrow createArrow(Level level, ItemStack ammo, LivingEntity shooter, @Nullable ItemStack weapon)
	{
		return new Arrow(level, shooter, ammo.copyWithCount(1), weapon);
	}

	@Override
	public Projectile asProjectile(Level level, Position pos, ItemStack stack, Direction direction)
	{
		Arrow arrow = new Arrow(level, pos.x(), pos.y(), pos.z(), stack.copyWithCount(1), null);
		arrow.pickup = AbstractArrow.Pickup.ALLOWED;
		return arrow;
	}

	// FIXME
	/*@Override
	public AbstractArrow createArrow(Level worldIn, ItemStack stack, LivingEntity shooter)
	{
		if (this.getItem() == NetherItems.neridium_arrow)
			return new NeridiumArrowEntity(worldIn, shooter);
		else if (this.getItem() == NetherItems.pyridium_arrow)
			return new PyridiumArrowEntity(worldIn, shooter);
		else if (this.getItem() == NetherItems.linium_arrow)
			return new LiniumArrowEntity(worldIn, shooter);
		else
			return new NetherrackArrowEntity(worldIn, shooter);
	}*/

	@Override
	public boolean isInfinite(ItemStack ammo, ItemStack bow, LivingEntity livingEntity)
	{
		// FIXME
		return super.isInfinite(ammo, bow, livingEntity);
	}

	/*@Override
	public boolean isInfinite(ItemStack stack, ItemStack bow, Player player)
	{
		int enchant = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.INFINITY_ARROWS, bow);
		return enchant <= 0 ? false : this.getClass() == NetherArrowItem.class && this.getItem() != NetherItems.pyridium_arrow && this.getItem() != NetherItems.linium_arrow;
	
	}*/
}