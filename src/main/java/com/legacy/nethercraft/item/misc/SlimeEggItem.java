package com.legacy.nethercraft.item.misc;

import com.legacy.nethercraft.entity.projectile.SlimeEggEntity;

import net.minecraft.core.Direction;
import net.minecraft.core.Position;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.entity.projectile.Snowball;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ProjectileItem;
import net.minecraft.world.level.Level;

public class SlimeEggItem extends Item implements ProjectileItem
{
	public SlimeEggItem(Item.Properties builder)
	{
		super(builder);
	}

	@Override
	public InteractionResult use(Level level, Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);
		level.playSound(null, player.getX(), player.getY(), player.getZ(), SoundEvents.SNOWBALL_THROW, SoundSource.NEUTRAL, 0.5F, 0.4F / (level.getRandom().nextFloat() * 0.4F + 0.8F));

		if (level instanceof ServerLevel serverlevel)
			Projectile.spawnProjectileFromRotation(Snowball::new, serverlevel, itemstack, player, 0.0F, 1.5F, 1.0F);

		player.awardStat(Stats.ITEM_USED.get(this));
		itemstack.consume(1, player);
		return InteractionResult.SUCCESS;
	}

	@Override
	public Projectile asProjectile(Level level, Position pos, ItemStack stack, Direction direction)
	{
		return new SlimeEggEntity(level, pos.x(), pos.y(), pos.z(), stack);
	}
}