package com.legacy.nethercraft.item.util;

import java.util.EnumMap;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.registry.NCEquipmentAssets;

import net.minecraft.Util;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.equipment.ArmorMaterial;
import net.minecraft.world.item.equipment.ArmorType;

public interface NCArmorMaterial
{
	ArmorMaterial IMP_SKIN = new ArmorMaterial(5, Util.make(new EnumMap<>(ArmorType.class), values ->
	{
		values.put(ArmorType.BOOTS, 1);
		values.put(ArmorType.LEGGINGS, 2);
		values.put(ArmorType.CHESTPLATE, 3);
		values.put(ArmorType.HELMET, 1);
		values.put(ArmorType.BODY, 3);
	}), 15, SoundEvents.ARMOR_EQUIP_LEATHER, 0.0F, 0.0F, ItemTags.REPAIRS_LEATHER_ARMOR, NCEquipmentAssets.IMP_SKIN);

	ResourceLocation NERIDIUM_MODEL = Nethercraft.locate("neridium");
	ArmorMaterial NERIDIUM = new ArmorMaterial(22, Util.make(new EnumMap<>(ArmorType.class), values ->
	{
		values.put(ArmorType.BOOTS, 1);
		values.put(ArmorType.LEGGINGS, 3);
		values.put(ArmorType.CHESTPLATE, 5);
		values.put(ArmorType.HELMET, 2);
		values.put(ArmorType.BODY, 7);
	}), 9, SoundEvents.ARMOR_EQUIP_IRON, 0.0F, 0.0F, ItemTags.REPAIRS_IRON_ARMOR, NCEquipmentAssets.NERIDIUM);

	ResourceLocation W_OBSIDIAN_MODEL = Nethercraft.locate("w_obsidian");
	ArmorMaterial W_OBSIDIAN = new ArmorMaterial(40, Util.make(new EnumMap<>(ArmorType.class), values ->
	{
		values.put(ArmorType.BOOTS, 3);
		values.put(ArmorType.LEGGINGS, 6);
		values.put(ArmorType.CHESTPLATE, 8);
		values.put(ArmorType.HELMET, 3);
		values.put(ArmorType.BODY, 11);
	}), 10, SoundEvents.ARMOR_EQUIP_DIAMOND, 2.0F, 2.0F, ItemTags.REPAIRS_DIAMOND_ARMOR, NCEquipmentAssets.W_OBSIDIAN);
}