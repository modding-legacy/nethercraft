package com.legacy.nethercraft.item.util;

import net.minecraft.world.food.FoodProperties;

public class NCFoods
{
	public static final FoodProperties DEVIL_BREAD = new FoodProperties.Builder().nutrition(5).saturationModifier(0.6F).build();
	public static final FoodProperties GLOW_APPLE = new FoodProperties.Builder().nutrition(4).saturationModifier(0.3F).build();
	public static final FoodProperties GLOW_STEW = new FoodProperties.Builder().nutrition(6).saturationModifier(0.6F).build();
}