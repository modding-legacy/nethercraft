package com.legacy.nethercraft.item.util;

import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.ToolMaterial;

public interface NCItemTier
{
	// FIXME: TAGS
	ToolMaterial GLOWWOOD = new ToolMaterial(BlockTags.INCORRECT_FOR_WOODEN_TOOL, 160, 2.0F, 0.0F, 15, ItemTags.PLANKS);
	ToolMaterial NETHERRACK = new ToolMaterial(BlockTags.INCORRECT_FOR_STONE_TOOL, 320, 4.0F, 1.0F, 5, ItemTags.STONE_TOOL_MATERIALS);
	ToolMaterial NERIDIUM = new ToolMaterial(BlockTags.INCORRECT_FOR_IRON_TOOL, 540, 8.0F, 2.0F, 14, ItemTags.IRON_TOOL_MATERIALS);
	ToolMaterial PYRIDIUM = new ToolMaterial(BlockTags.INCORRECT_FOR_DIAMOND_TOOL, 2064, 7.0F, 3.0F, 10, ItemTags.DIAMOND_TOOL_MATERIALS);
	ToolMaterial LINIUM = new ToolMaterial(BlockTags.INCORRECT_FOR_NETHERITE_TOOL, 320, 12.0F, 4.0F, 20, ItemTags.NETHERITE_TOOL_MATERIALS);

	/*TagKey<Block> incorrectBlocksForDrops, int durability, float speed, float attackDamageBonus, int enchantmentValue, TagKey<Item> repairItems*/
	/*int harvestLevelIn, int maxUsesIn, float efficiencyIn, float attackDamageIn, int enchantabilityIn, Supplier<Ingredient> repairMaterialIn*/
}