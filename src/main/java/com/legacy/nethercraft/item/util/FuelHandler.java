package com.legacy.nethercraft.item.util;

import com.legacy.nethercraft.registry.NCBlocks;
import com.legacy.nethercraft.registry.NCItems;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.event.furnace.FurnaceFuelBurnTimeEvent;

public class FuelHandler
{
	@SubscribeEvent
	public static void getBurnTime(FurnaceFuelBurnTimeEvent event)
	{
		Item item = event.getItemStack().getItem();

		if (item.equals(NCItems.neridium_lava_bucket))
			event.setBurnTime(20000);

		if (item.equals(NCItems.glowood_stick))
			event.setBurnTime(100);
		
		if (item.equals(NCItems.foulite_dust))
			event.setBurnTime(4800);

		if (item.equals(Items.GLOWSTONE_DUST))
			event.setBurnTime(2400);

		//else if (item.equals(NetherBlocks.glowood_fence_gate.asItem()))
			//event.setBurnTime(300);
		
		else if (item.equals(NCBlocks.glowood_sapling.asItem()))
			event.setBurnTime(150);

		else if (item.equals(NCBlocks.glowood_fence.asItem()))
			event.setBurnTime(300);
		
		else if (item.equals(NCBlocks.glowood_planks.asItem()))
			event.setBurnTime(300);
		else if (item.equals(NCBlocks.glowood_ladder.asItem()))
			event.setBurnTime(300);
		else if (item.equals(NCBlocks.glowood_crafting_table.asItem()))
			event.setBurnTime(300);
		else if (item.equals(NCBlocks.glowood_bookshelf.asItem()))
			event.setBurnTime(300);
		else if (item.equals(NCItems.glowood_sword) || item.equals(NCItems.glowood_pickaxe) || item.equals(NCItems.glowood_axe) || item.equals(NCItems.glowood_shovel) || item.equals(NCItems.glowood_hoe))
			event.setBurnTime(200);
	}
}
