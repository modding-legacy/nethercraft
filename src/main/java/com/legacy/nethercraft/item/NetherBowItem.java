package com.legacy.nethercraft.item;

import java.util.List;

import com.legacy.nethercraft.registry.NCItems;

import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.BowItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.neoforged.neoforge.event.EventHooks;

public class NetherBowItem extends BowItem
{
	public NetherBowItem(Item.Properties builder)
	{
		super(builder);
	}

	@Override
	public boolean releaseUsing(ItemStack stack, Level level, LivingEntity shooter, int useTime)
	{
		if (!(shooter instanceof Player player))
			return false;

		ItemStack itemstack = player.getProjectile(stack);
		if (itemstack.isEmpty())
		{
			return false;
		}
		else
		{
			int i = this.getUseDuration(stack, shooter) - useTime;
			i = EventHooks.onArrowLoose(stack, level, player, i, !itemstack.isEmpty());
			if (i < 0)
				return false;

			float maxVelocity = stack.getItem() == NCItems.pyridium_bow ? 1.25F : stack.getItem() == NCItems.linium_bow ? 1.5F : 1.0F;
			float f = getVelocityWithCharge(i, getChargeDuration(this)) * maxVelocity;

			if ((double) f < 0.1)
				return false;

			List<ItemStack> list = draw(stack, itemstack, player);
			if (level instanceof ServerLevel serverlevel && !list.isEmpty())
			{
				if (f > 0.7F && stack.getItem() == NCItems.netherrack_bow)
					f = 0.7F;

				this.shoot(serverlevel, player, player.getUsedItemHand(), stack, list, f * 3.0F, 1.0F, f == 1.0F, null);
			}

			level.playSound(null, player.getX(), player.getY(), player.getZ(), SoundEvents.ARROW_SHOOT, SoundSource.PLAYERS, 1.0F, 1.0F / (level.getRandom().nextFloat() * 0.4F + 1.2F) + f * 0.5F);
			player.awardStat(Stats.ITEM_USED.get(this));
			return true;
		}
	}

	private static float getVelocityWithCharge(int charge, float chargeTime)
	{
		float f = (float) charge / chargeTime;
		f = (f * f + f * 2.0F) / 3.0F;
		if (f > 1.0F)
		{
			f = 1.0F;
		}
		return f;
	}

	@Override
	public int getUseDuration(ItemStack stack, LivingEntity entity)
	{
		return stack.getItem() == NCItems.linium_bow ? 144000 : 72000;
	}

	public static float getChargeDuration(Item item)
	{
		return item == NCItems.linium_bow ? 40.0F : item == NCItems.pyridium_bow ? 30.0F : item == NCItems.neridium_bow ? 20.0F : 10.0F;
	}
}