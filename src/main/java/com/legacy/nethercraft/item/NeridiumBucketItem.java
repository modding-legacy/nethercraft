package com.legacy.nethercraft.item;

import com.legacy.nethercraft.registry.NCItems;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.BucketItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ItemUtils;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BucketPickup;
import net.minecraft.world.level.block.LiquidBlockContainer;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.HitResult;
import net.neoforged.neoforge.common.NeoForgeMod;

public class NeridiumBucketItem extends BucketItem
{
	public NeridiumBucketItem(Fluid fluid, Properties builder)
	{
		super(fluid, builder);
	}

	@SuppressWarnings("deprecation")
	@Override
	public InteractionResult use(Level level, Player player, InteractionHand hand)
	{
		ItemStack handStack = player.getItemInHand(hand);
		BlockHitResult hitResult = getPlayerPOVHitResult(level, player, this.content == Fluids.EMPTY ? ClipContext.Fluid.SOURCE_ONLY : ClipContext.Fluid.NONE);

		if (hitResult.getType() == HitResult.Type.MISS || hitResult.getType() != HitResult.Type.BLOCK)
		{
			return InteractionResult.PASS;
		}
		else
		{
			BlockPos hitPos = hitResult.getBlockPos();
			Direction offset = hitResult.getDirection();
			BlockPos offsetPos = hitPos.relative(offset);
			if (level.mayInteract(player, hitPos) && player.mayUseItemAt(offsetPos, offset, handStack))
			{
				BlockState hitState = level.getBlockState(hitPos);

				if (this.content == Fluids.EMPTY)
				{
					// Check for valid fluid
					if (hitState.getBlock() instanceof BucketPickup bucketPickup && NeridiumBucketItem.isValidFluid(hitState.getFluidState().getType()))
					{
						// Change to neridium Bucket
						ItemStack fullBucket = convertBucket(bucketPickup.pickupBlock(player, level, hitPos, hitState));

						if (!fullBucket.isEmpty())
						{
							player.awardStat(Stats.ITEM_USED.get(this));
							// Use custom sounds
							Fluid fluid = hitState.getFluidState().getType();
							(fluid == Fluids.WATER ? bucketPickup.getPickupSound(hitState) : fluid.getPickupSound()).ifPresent((sound) ->
							{
								player.playSound(sound, 1.0F, 1.0F);
							});

							level.gameEvent(player, GameEvent.FLUID_PICKUP, hitPos);
							ItemStack filledResult = ItemUtils.createFilledResult(handStack, player, fullBucket);
							if (!level.isClientSide)
							{
								CriteriaTriggers.FILLED_BUCKET.trigger((ServerPlayer) player, fullBucket);
							}

							return InteractionResult.SUCCESS.heldItemTransformedTo(filledResult);
						}
					}

					return InteractionResult.FAIL;
				}
				else
				{
					BlockPos placePos = this.canBlockContainFluid(player, level, hitPos, hitState) ? hitPos : offsetPos;
					if (this.emptyContents(player, level, placePos, hitResult))
					{
						this.checkExtraContent(player, level, handStack, placePos);
						if (player instanceof ServerPlayer)
						{
							CriteriaTriggers.PLACED_BLOCK.trigger((ServerPlayer) player, placePos, handStack);
						}

						player.awardStat(Stats.ITEM_USED.get(this));
						// neridium bucket instead
						return InteractionResult.SUCCESS.heldItemTransformedTo(NeridiumBucketItem.getEmptySuccessItem(handStack, player));
					}
					else
					{
						return InteractionResult.FAIL;
					}
				}
			}
			else
			{
				return InteractionResult.FAIL;
			}
		}
	}

	public static ItemStack getEmptySuccessItem(ItemStack stack, Player player)
	{
		// Switch vanilla bucket with neridium bucket
		return !player.getAbilities().instabuild ? new ItemStack(NCItems.neridium_bucket) : stack;
	}

	@Override
	protected boolean canBlockContainFluid(Player player, Level level, BlockPos pos, BlockState state)
	{
		return state.getBlock() instanceof LiquidBlockContainer && ((LiquidBlockContainer) state.getBlock()).canPlaceLiquid(player, level, pos, state, this.content);
	}

	/**
	 * Attempts to convert the passed stack to a neridium bucket version
	 * 
	 * @param stack
	 * @return
	 */
	public static ItemStack convertBucket(ItemStack stack)
	{
		Item vanillaBucket = stack.getItem();
		if (vanillaBucket == Items.LAVA_BUCKET)
			return new ItemStack(NCItems.neridium_lava_bucket);
		return stack;
	}

	/**
	 * Checks if the passed fluid is valid for this bucket
	 * 
	 * @param fluid
	 * @return
	 */
	public static boolean isValidFluid(Fluid fluid)
	{
		return fluid.getFluidType() == NeoForgeMod.LAVA_TYPE;
	}
}
