package com.legacy.nethercraft.mixin;

import java.util.Optional;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.legacy.nethercraft.world.NCNoiseSettings;

import net.minecraft.core.Holder;
import net.minecraft.core.Holder.Reference;
import net.minecraft.core.registries.Registries;
import net.minecraft.server.level.WorldGenRegion;
import net.minecraft.world.level.StructureManager;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.levelgen.NoiseBasedChunkGenerator;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.RandomState;

@Mixin(NoiseBasedChunkGenerator.class)
public class NoiseBasedChunkGeneratorMixin
{
	@Final
	@Shadow
	private Holder<NoiseGeneratorSettings> settings;

	/**
	 * Best way I could find to do this. It happens earlier to make sure other mods
	 * grab it if they do something similar. If there's a more safe and general way
	 * that doesn't look like a war crime, please do give constructive criticism.
	 */
	@Inject(at = @At("HEAD"), order = 995, method = "buildSurface(Lnet/minecraft/server/level/WorldGenRegion;Lnet/minecraft/world/level/StructureManager;Lnet/minecraft/world/level/levelgen/RandomState;Lnet/minecraft/world/level/chunk/ChunkAccess;)V")
	private void buildSurface(WorldGenRegion level, StructureManager structureManager, RandomState random, ChunkAccess chunk, CallbackInfo callback)
	{
		var settings = this.settings.value();

		if ((Object) settings instanceof NCNoiseSettings ncSettings)
		{
			Optional<Reference<NoiseGeneratorSettings>> netherNoiseSettings = level.registryAccess().lookupOrThrow(Registries.NOISE_SETTINGS).get(NoiseGeneratorSettings.NETHER);

			if (netherNoiseSettings.isPresent() && settings.equals(netherNoiseSettings.get().value()))
				ncSettings.nethercraft$flagAsNether(true);
		}
	}
}
