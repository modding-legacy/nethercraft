package com.legacy.nethercraft.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

import com.legacy.nethercraft.event.NCEvents;
import com.legacy.nethercraft.world.NCNoiseSettings;
import com.llamalad7.mixinextras.injector.ModifyReturnValue;

import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.SurfaceRules;

@Mixin(NoiseGeneratorSettings.class)
public class NoiseGeneratorSettingsMixin implements NCNoiseSettings
{
	private boolean nethercraft$isNether;

	/**
	 * Best way I could find to do this. If there's a more safe and general way that
	 * doesn't look like a war crime, please do give constructive criticism.
	 */
	@ModifyReturnValue(method = "surfaceRule()Lnet/minecraft/world/level/levelgen/SurfaceRules$RuleSource;", at = @At("RETURN"))
	private SurfaceRules.RuleSource nethercraft$netherSurface(SurfaceRules.RuleSource original)
	{
		return nethercraft$isNether ? NCEvents.pain(original) : original;
	}

	@Override
	public void nethercraft$flagAsNether(boolean nether)
	{
		nethercraft$isNether = nether;
	}
}
