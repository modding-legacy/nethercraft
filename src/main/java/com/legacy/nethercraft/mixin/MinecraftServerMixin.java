package com.legacy.nethercraft.mixin;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Streams;
import com.legacy.nethercraft.registry.NCBiomes;
import com.mojang.datafixers.util.Either;
import com.mojang.datafixers.util.Pair;

import net.minecraft.core.Holder;
import net.minecraft.core.LayeredRegistryAccess;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.RegistryLayer;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Climate;
import net.minecraft.world.level.biome.Climate.ParameterList;
import net.minecraft.world.level.biome.Climate.ParameterPoint;
import net.minecraft.world.level.biome.MultiNoiseBiomeSource;
import net.minecraft.world.level.biome.MultiNoiseBiomeSourceParameterList;
import net.minecraft.world.level.dimension.LevelStem;

//FIXME
@Mixin(MinecraftServer.class)
public class MinecraftServerMixin
{
	@Shadow
	@Final
	private LayeredRegistryAccess<RegistryLayer> registries;

	/*
	 * MinecraftServer#loadLevel
	 */
	@Inject(at = @At("HEAD"), method = "loadLevel()V")
	private void initServer(CallbackInfo callback)
	{
		Registry<LevelStem> dimRegistry = this.registries.compositeAccess().lookupOrThrow(Registries.LEVEL_STEM);
		Registry<Biome> biomeLookup = this.registries.compositeAccess().lookupOrThrow(Registries.BIOME);
		Supplier<LevelStem> nether = () -> dimRegistry.get(LevelStem.NETHER).get().value();

		//@formatter:off
		List<Pair<Climate.ParameterPoint, Holder.Reference<Biome>>> biomes = List.of(
				Pair.of(Climate.parameters(0.9F, 0.3F, 0.0F, 0.0F, 0.0F,  0.12F, 0.05F), biomeLookup.getOrThrow(NCBiomes.GLOWING_GROVE.getKey())), 
				Pair.of(Climate.parameters(0.6F, -0.1F, 0.2F,0.0F, 0.0F,  0.24F, 0.0F), biomeLookup.getOrThrow(NCBiomes.GLOWSHROOM_GARDEN.getKey())), 
				Pair.of(Climate.parameters(-0.8F, -0.4F, 0.2F,0.0F, 0.0F,  0.0F, 0.026F), biomeLookup.getOrThrow(NCBiomes.VOLCANIC_RUSHES.getKey())));
		//@formatter:on

		var source = nether.get().generator().getBiomeSource();
		if (source instanceof MultiNoiseBiomeSource multiNoise)
		{
			// TODO: Make this not awful somehow. I absolutely DISPISE generic types.
			List<Pair<ParameterPoint, ? extends Holder<Biome>>> a = Streams.concat(multiNoise.parameters().values().stream(), biomes.stream()).collect(Collectors.toList());
			@SuppressWarnings({ "rawtypes", "unchecked" })
			Climate.ParameterList<Holder<Biome>> paramList = new Climate.ParameterList(a);
			Either<ParameterList<Holder<Biome>>, Holder<MultiNoiseBiomeSourceParameterList>> params = Either.left(paramList);
			multiNoise.parameters = params;
			source.possibleBiomes = Suppliers.memoize(() -> multiNoise.parameters().values().stream().map(Pair::getSecond).distinct().collect(ImmutableSet.toImmutableSet()));
		}
	}
}
