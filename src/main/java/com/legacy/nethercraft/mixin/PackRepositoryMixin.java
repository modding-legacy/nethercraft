package com.legacy.nethercraft.mixin;

import java.util.ArrayList;
import java.util.List;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;

import com.legacy.nethercraft.client.resource_pack.NCResourcePackHandler;
import com.llamalad7.mixinextras.injector.ModifyReturnValue;

import net.minecraft.server.packs.repository.Pack;
import net.minecraft.server.packs.repository.PackRepository;

@Mixin(PackRepository.class)
public abstract class PackRepositoryMixin
{
	/**
	 * Pseudo injects the mod's programmer art pack into the vanilla one. The pack
	 * itself is hidden and cannot be selected on its own. This prevents
	 * unsuspecting users enabling it thinking it's required (true and common story)
	 */
	@ModifyReturnValue(method = "rebuildSelected", at = @At("RETURN"))
	private List<Pack> gns$rebuildSelected(List<Pack> original)
	{
		List<Pack> selected = new ArrayList<>(original);
		Pack legacy = NCResourcePackHandler.LEGACY_PACK;
		Pack pa = this.getPack("programmer_art");

		if (selected.contains(pa))
		{
			if (!selected.contains(legacy))
			{
				for (int i = 0; i < selected.size(); ++i)
				{
					if (selected.get(i) == pa)
					{
						selected.add(i + 1, legacy);
						break;
					}
				}
			}

			return selected;
		}
		else if (selected.contains(legacy))
		{
			selected.remove(legacy);
			return selected;
		}

		return original;
	}

	@Shadow
	public Pack getPack(String id)
	{
		throw new IllegalStateException();
	}
}
