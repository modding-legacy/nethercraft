package com.legacy.nethercraft.tile_entity;

import com.legacy.nethercraft.registry.NCBlockEntityTypes;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class NetherChestBlockEntity extends ChestBlockEntity
{
	protected NetherChestBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState blockState)
	{
		super(type, pos, blockState);
	}

	public NetherChestBlockEntity(BlockPos pos, BlockState state)
	{
		this(NCBlockEntityTypes.NETHER_CHEST.get(), pos, state);
	}

	@Override
	protected Component getDefaultName()
	{
		return this.getBlockState().getBlock().getName();
	}
}
