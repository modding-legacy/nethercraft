package com.legacy.nethercraft.tile_entity;

import com.legacy.nethercraft.registry.NCBlockEntityTypes;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.FurnaceMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.block.entity.AbstractFurnaceBlockEntity;
import net.minecraft.world.level.block.entity.FuelValues;
import net.minecraft.world.level.block.state.BlockState;

public class NetherrackFurnaceBlockEntity extends AbstractFurnaceBlockEntity
{
	public NetherrackFurnaceBlockEntity(BlockPos pos, BlockState blockState)
	{
		super(NCBlockEntityTypes.NETHERRACK_FURNACE.get(), pos, blockState, RecipeType.SMELTING);
	}

	@Override
	protected Component getDefaultName()
	{
		return this.getBlockState().getBlock().getName();
	}

	@Override
	protected AbstractContainerMenu createMenu(int id, Inventory player)
	{
		return new FurnaceMenu(id, player, this, this.dataAccess);
	}

	/**
	 * 20% faster
	 * 
	 * TODO: Maybe make this in the Nether only? Or reduced outside the Nether?
	 */
	@Override
	protected int getBurnDuration(FuelValues fuel, ItemStack stack)
	{
		return (int) (super.getBurnDuration(fuel, stack) * 0.8F);
	}
}