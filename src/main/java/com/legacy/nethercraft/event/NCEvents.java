package com.legacy.nethercraft.event;

import com.legacy.nethercraft.registry.NCBiomes;
import com.legacy.nethercraft.registry.NCParticles;

import net.minecraft.world.level.levelgen.Noises;
import net.minecraft.world.level.levelgen.SurfaceRules;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.client.event.RegisterParticleProvidersEvent;

public class NCEvents
{
	/*if (noise < 3.0F && noise > 2.0D + (random.nextInt(2) - 1) && random.nextFloat() < 0.7F || noise < 0.5D && noise > -2.5D) old rushes config, the rest was sand
		random.nextFloat() < 0.2F ? MAGMA_SURFACE_BUILDER : SurfaceBuilder.CONFIG_HELL*/
	public static SurfaceRules.RuleSource pain(SurfaceRules.RuleSource original)
	{
		SurfaceRules.ConditionSource surfacerules$conditionsource9 = SurfaceRules.noiseCondition(Noises.NETHERRACK, 0.54);

		// @formatter:off
		var dirtSeq = SurfaceRules.sequence(
				SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, SurfaceRules.ifTrue(SurfaceRules.not(surfacerules$conditionsource9), NCBiomes.RuleSources.NETHER_DIRT.get())),
				SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, NCBiomes.RuleSources.NETHER_DIRT.get()));

		return SurfaceRules.sequence( 
				SurfaceRules.sequence(
		                SurfaceRules.sequence(
		                    SurfaceRules.ifTrue(
		                    		SurfaceRules.isBiome(NCBiomes.GLOWING_GROVE.getKey()), dirtSeq),
		                    SurfaceRules.ifTrue(
		                    		SurfaceRules.isBiome(NCBiomes.GLOWSHROOM_GARDEN.getKey()), dirtSeq),
		                    SurfaceRules.ifTrue(
			                        SurfaceRules.isBiome(NCBiomes.VOLCANIC_RUSHES.getKey()),
			                        SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR,
			                        		SurfaceRules.ifTrue(SurfaceRules.not(surfacerules$conditionsource9), NCBiomes.RuleSources.HEAT_SAND.get()
			                        )
			                    )
		                    ))
		                )
		            , original);
		//@formatter:on
	}

	public static class ModEvents
	{
		@SubscribeEvent
		public static void registerParticleFactories(RegisterParticleProvidersEvent event)
		{
			NCParticles.Factory.init(event);
		}
	}
	// FIXME
	/*public static void setupNetherFeatures(BiomeLoadingEvent event)
	{
		if (event.getCategory() == Biome.BiomeCategory.NETHER)
		{
			BiomeGenerationSettingsBuilder biome = event.getGeneration();
			MobSpawnInfoBuilder spawns = event.getSpawns();
	
			Extra biome decoration, and mob spawning in vanilla and other modded Nether biomes.
			NetherFeatures.addNetherOres(biome);
			// NetherFeatures.addOldNetherSpawns(spawns);
			NetherFeatures.addLavaReeds(biome);
	
			if (doesBiomeMatch(event.getName(), Biomes.NETHER_WASTES)) // Nether Wastes
			{
				NetherFeatures.addSmallGlowshrooms(biome);
				spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NetherEntityTypes.CAMOUFLAGE_SPIDER, 60, 1, 3));
			}
		}
	}*/

	// FIXME
	/*@SubscribeEvent
	public static void livingSpawn(EntityJoinWorldEvent event)
	{
		if (event.getEntity() instanceof ZombifiedPiglin)
		{
			ZombifiedPiglin zombifiedPiglin = (ZombifiedPiglin) event.getEntity();
			Level world = event.getWorld();
			// Add the ranged Zombie Pigmen... er, piglins
			if (world.random.nextInt(5) == 0 && !((ZombifiedPiglin) event.getEntity()).isBaby())
			{
				event.getEntity().setItemSlot(EquipmentSlot.MAINHAND, new ItemStack(NetherItems.pyridium_bow));
				zombifiedPiglin.goalSelector.addGoal(1, new PiglinRangedBowAttackGoal<ZombifiedPiglin>(zombifiedPiglin, 1.0D, 20, 15.0F));
				zombifiedPiglin.getAttribute(Attributes.MAX_HEALTH).setBaseValue(30.0F);
				zombifiedPiglin.setHealth(30.0F);
			}
	
			// zombifiedPiglin.targetSelector.addGoal(3, new
			// NearestAttackableTargetGoal<TribalEntity>(zombifiedPiglin,
			// TribalEntity.class, true));
		}
		else if (event.getEntity() instanceof Piglin)
		{
			Piglin piglin = (Piglin) event.getEntity();
			piglin.targetSelector.addGoal(2, new NearestAttackableTargetGoal<TribalEntity>(piglin, TribalEntity.class, true));
		}
	}*/

	// FIXME
	/*@SubscribeEvent
	public static void onEntityUpdate(LivingUpdateEvent event)
	{
		if (event.getEntityLiving() instanceof ZombifiedPiglin)
		{
			ZombifiedPiglin pigman = (ZombifiedPiglin) event.getEntityLiving();
	
			if (pigman.getMainHandItem().getItem() instanceof BowItem && !pigman.isBaby() && uuidMeleeRemoved.containsKey(pigman.getUUID().toString()))
			{
				if (uuidMeleeRemoved.get(pigman.getUUID().toString()))
					return;
	
				pigman.goalSelector.removeGoal(new ZombieAttackGoal(pigman, 1.0D, false));
				uuidMeleeRemoved.put(pigman.getUUID().toString(), true);
			}
			else
			{
				uuidMeleeRemoved.put(pigman.getUUID().toString(), false);
			}
		}
	}*/

	// FIXME
	/*@SubscribeEvent
	public static void onEntityDeath(LivingDeathEvent event)
	{
		if (event.getEntityLiving() instanceof Ghast && event.getEntityLiving().level.random.nextInt(3) == 0 && event.getEntityLiving().level.getGameRules().getBoolean(GameRules.RULE_DOENTITYDROPS))
		{
			ItemStack bones = new ItemStack(NetherItems.ghast_bones);
			bones.setCount(event.getEntityLiving().level.random.nextInt(2) + 1);
			event.getEntityLiving().spawnAtLocation(bones);
		}
	}*/

	// FIXME
	/*@SubscribeEvent
	public static void onEntityAttack(LivingAttackEvent event)
	{
		if (event.getEntityLiving() instanceof ZombifiedPiglin)
		{
			// So Pigmen can't hurt eachother
			if (event.getSource() instanceof IndirectEntityDamageSource && event.getSource().getEntity() instanceof ZombifiedPiglin)
			{
				event.getSource().getDirectEntity().remove();
				event.setCanceled(true);
			}
		}
	}*/
}