package com.legacy.nethercraft.event;

import com.legacy.nethercraft.block.ToolCompat;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.common.ItemAbilities;
import net.neoforged.neoforge.event.level.BlockEvent.BlockToolModificationEvent;

public class NCPlayerEvents
{
	@SubscribeEvent
	public static void onToolUse(final BlockToolModificationEvent event)
	{
		var toolAction = event.getItemAbility();
		if (toolAction == ItemAbilities.AXE_STRIP)
		{
			BlockState finalState = event.getFinalState();
			Block result = ToolCompat.AXE_STRIPPING.get(finalState.getBlock());
			if (result != null)
				event.setFinalState(result.withPropertiesOf(finalState));
		}
		else if (toolAction == ItemAbilities.HOE_TILL)
		{
			Block result = ToolCompat.HOE_TILLING.get(event.getFinalState().getBlock());
			if (result != null)
				event.setFinalState(result.defaultBlockState());
		}
	}

	/*@SubscribeEvent FIXME
	public static void onPlayerRightClickHoe(UseHoeEvent event)
	{
		Player player = event.getPlayer();
		Level world = event.getContext().getLevel();
		BlockPos pos = event.getContext().getClickedPos();
		BlockState dirtState = world.getBlockState(pos);
		BlockState resultState = HOE_LOOKUP.get(dirtState.getBlock());
		if (resultState != null && event.getContext().getClickedFace() != Direction.DOWN && world.isEmptyBlock(pos.above()))
		{
			if (world.random.nextInt(10) == 0)
			{
				ItemEntity itementity = new ItemEntity(world, pos.getX(), pos.getY() + 1, pos.getZ(), new ItemStack(NetherItems.dark_seeds));
				itementity.setDefaultPickUpDelay();
				world.addFreshEntity(itementity);
			}
	
			player.getCommandSenderWorld().playSound(player, pos, SoundEvents.HOE_TILL, SoundSource.BLOCKS, 1.0F, 1.0F);
			player.swing(event.getContext().getHand());
			if (!world.isClientSide)
			{
				world.setBlock(pos, resultState, 11);
				if (player != null)
				{
					event.getContext().getItemInHand().hurtAndBreak(1, player, (playerEntity) ->
					{
						playerEntity.broadcastBreakEvent(event.getContext().getHand());
					});
				}
			}
			event.setCanceled(true);
		}
	}*/
}
