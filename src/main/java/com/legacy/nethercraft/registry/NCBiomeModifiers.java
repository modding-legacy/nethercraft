package com.legacy.nethercraft.registry;

import com.legacy.nethercraft.Pointer;
import com.legacy.nethercraft.data.NCTags;

import net.minecraft.core.HolderSet;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.GenerationStep.Decoration;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.neoforged.neoforge.common.world.BiomeModifier;
import net.neoforged.neoforge.common.world.BiomeModifiers;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

public class NCBiomeModifiers
{
	/*public static final RegistrarHandler<BiomeModifier> HANDLER = RegistrarHandler.getOrCreate(NeoForgeRegistries.Keys.BIOME_MODIFIERS, Nethercraft.MODID);*/

	public static final Pointer<BiomeModifier> ADD_FOULITE_ORE = new Pointer<>(NeoForgeRegistries.Keys.BIOME_MODIFIERS, "add_foulite_ore", c -> addFeature(c, Decoration.UNDERGROUND_ORES, NCTags.Biomes.HAS_FOULITE_ORE, NCFeatures.Placements.FOULITE_ORE));
	public static final Pointer<BiomeModifier> ADD_NERIDIUM_ORE = new Pointer<>(NeoForgeRegistries.Keys.BIOME_MODIFIERS, "add_neridium_ore", c -> addFeature(c, Decoration.UNDERGROUND_ORES, NCTags.Biomes.HAS_NERIDIUM_ORE, NCFeatures.Placements.NERIDIUM_ORE));
	public static final Pointer<BiomeModifier> ADD_PYRIDIUM_ORE = new Pointer<>(NeoForgeRegistries.Keys.BIOME_MODIFIERS, "add_pyridium_ore", c -> addFeature(c, Decoration.UNDERGROUND_ORES, NCTags.Biomes.HAS_PYRIDIUM_ORE, NCFeatures.Placements.PYRIDIUM_ORE));
	public static final Pointer<BiomeModifier> ADD_LINIUM_ORE = new Pointer<>(NeoForgeRegistries.Keys.BIOME_MODIFIERS, "add_linium_ore", c -> addFeature(c, Decoration.UNDERGROUND_ORES, NCTags.Biomes.HAS_LINIUM_ORE, NCFeatures.Placements.LINIUM_ORE));
	public static final Pointer<BiomeModifier> ADD_W_ORE = new Pointer<>(NeoForgeRegistries.Keys.BIOME_MODIFIERS, "add_w_ore", c -> addFeature(c, Decoration.UNDERGROUND_ORES, NCTags.Biomes.HAS_W_ORE, NCFeatures.Placements.W_ORE));

	private static BiomeModifier addFeature(BootstrapContext<?> bootstrap, GenerationStep.Decoration step, TagKey<Biome> spawnTag, Pointer<PlacedFeature> feature)
	{
		return addFeature(bootstrap, step, spawnTag, feature.getKey());
	}

	private static BiomeModifier addFeature(BootstrapContext<?> bootstrap, GenerationStep.Decoration step, TagKey<Biome> spawnTag, ResourceKey<PlacedFeature> feature)
	{
		return new BiomeModifiers.AddFeaturesBiomeModifier(bootstrap.lookup(Registries.BIOME).getOrThrow(spawnTag), HolderSet.direct(bootstrap.lookup(Registries.PLACED_FEATURE).getOrThrow(feature)), step);
	}

	public static void bootstrap(BootstrapContext<BiomeModifier> bootstrap)
	{
		register(bootstrap, ADD_FOULITE_ORE, ADD_NERIDIUM_ORE, ADD_PYRIDIUM_ORE, ADD_LINIUM_ORE, ADD_W_ORE);
	}

	@SafeVarargs
	private static void register(BootstrapContext<BiomeModifier> bootstrap, Pointer<BiomeModifier>... pointers)
	{
		for (var pointer : pointers)
			bootstrap.register(pointer.getKey(), pointer.getInstance().apply(bootstrap));
	}
}
