package com.legacy.nethercraft.registry;

import com.legacy.nethercraft.Nethercraft;

import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.equipment.EquipmentAsset;

public interface NCEquipmentAssets
{
	ResourceKey<? extends Registry<EquipmentAsset>> ROOT_ID = ResourceKey.createRegistryKey(Nethercraft.locate("equipment_asset"));
	ResourceKey<EquipmentAsset> IMP_SKIN = createId("imp_skin");
	ResourceKey<EquipmentAsset> NERIDIUM = createId("neridium");
	ResourceKey<EquipmentAsset> W_OBSIDIAN = createId("w_obsidian");

	static ResourceKey<EquipmentAsset> createId(String key)
	{
		return ResourceKey.create(ROOT_ID, Nethercraft.locate(key));
	}
}
