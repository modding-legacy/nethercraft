package com.legacy.nethercraft.registry;

import java.util.Map.Entry;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.item.NeridiumBucketItem;
import com.legacy.nethercraft.item.NetherArrowItem;
import com.legacy.nethercraft.item.NetherBowItem;
import com.legacy.nethercraft.item.misc.SlimeEggItem;
import com.legacy.nethercraft.item.util.NCArmorMaterial;
import com.legacy.nethercraft.item.util.NCFoods;
import com.legacy.nethercraft.item.util.NCItemTier;

import net.minecraft.core.Direction;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.AxeItem;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.BoneMealItem;
import net.minecraft.world.item.FlintAndSteelItem;
import net.minecraft.world.item.HoeItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.JukeboxSongs;
import net.minecraft.world.item.PickaxeItem;
import net.minecraft.world.item.ShovelItem;
import net.minecraft.world.item.SpawnEggItem;
import net.minecraft.world.item.StandingAndWallBlockItem;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.equipment.ArmorType;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.material.Fluids;
import net.neoforged.neoforge.registries.RegisterEvent;

public class NCItems
{
	private static RegisterEvent registryEvent;

	public static Item pyridium_pickaxe, pyridium_shovel, pyridium_axe, pyridium_hoe, pyridium_sword;

	public static Item netherrack_pickaxe, netherrack_shovel, netherrack_axe, netherrack_hoe, netherrack_sword;

	public static Item glowood_pickaxe, glowood_shovel, glowood_axe, glowood_hoe, glowood_sword;

	public static Item linium_pickaxe, linium_shovel, linium_axe, linium_hoe, linium_sword;

	public static Item neridium_pickaxe, neridium_shovel, neridium_axe, neridium_hoe, neridium_sword;

	public static Item glowood_stick, foulite_dust, magma_caramel, imp_skin, ghast_rod, red_feather, lava_book,
			lava_paper;

	public static Item ghast_bones, ghast_marrow;

	public static Item neridium_ingot, pyridium_ingot, linium_ingot, w_dust, w_obsidian_ingot;

	public static Item dark_seeds, dark_wheat, lava_reeds;

	public static Item devil_bread, glow_stew, glow_apple;

	public static Item glowood_bowl;

	public static Item netherrack_bow, neridium_bow, pyridium_bow, linium_bow, netherrack_arrow, neridium_arrow,
			pyridium_arrow, linium_arrow;

	public static Item neridium_lighter, slime_eggs;

	public static Item imp_helmet, imp_chestplate, imp_leggings, imp_boots;

	public static Item neridium_helmet, neridium_chestplate, neridium_leggings, neridium_boots;

	public static Item w_obsidian_helmet, w_obsidian_chestplate, w_obsidian_leggings, w_obsidian_boots;

	public static Item neridium_bucket, neridium_lava_bucket;

	public static Item foulite_torch, charcoal_torch;

	public static Item imp_spawn_egg, dark_zombie_spawn_egg, bloody_zombie_spawn_egg, lava_slime_spawn_egg,
			camouflage_spider_spawn_egg, tribal_warrior_spawn_egg, tribal_trainee_spawn_egg, tribal_archer_spawn_egg;

	public static Item imp_disc;

	public static void init(RegisterEvent event)
	{
		registryEvent = event;
		registerBlockItems();

		imp_spawn_egg = register("imp_spawn_egg", p -> new SpawnEggItem(NCEntityTypes.IMP, /*0x1f4d14, 0x8e3801,*/ p));
		dark_zombie_spawn_egg = register("dark_zombie_spawn_egg", p -> new SpawnEggItem(NCEntityTypes.DARK_ZOMBIE, /*0x700c0c, 0x323131,*/ p));
		bloody_zombie_spawn_egg = register("bloody_zombie_spawn_egg", p -> new SpawnEggItem(NCEntityTypes.BLOODY_ZOMBIE, /*0x700c0c, 0x351212,*/ p));
		lava_slime_spawn_egg = register("lava_slime_spawn_egg", p -> new SpawnEggItem(NCEntityTypes.LAVA_SLIME, /*0xa43434, 0xbb5a29,*/ p));
		camouflage_spider_spawn_egg = register("camouflage_spider_spawn_egg", p -> new SpawnEggItem(NCEntityTypes.CAMOUFLAGE_SPIDER, /*0x3f172f, 0x710e20,*/ p));
		tribal_trainee_spawn_egg = register("tribal_trainee_spawn_egg", p -> new SpawnEggItem(NCEntityTypes.TRIBAL_TRAINEE, /*0x787878, 0x710e20,*/ p));
		tribal_warrior_spawn_egg = register("tribal_warrior_spawn_egg", p -> new SpawnEggItem(NCEntityTypes.TRIBAL_WARRIOR, /*0x787878, 0x500e71,*/ p));
		tribal_archer_spawn_egg = register("tribal_archer_spawn_egg", p -> new SpawnEggItem(NCEntityTypes.TRIBAL_ARCHER, /*0x787878, 0x7a5b89,*/ p));

		glowood_stick = register("glowood_stick", p -> new Item(p));
		foulite_dust = register("foulite_dust", p -> new Item(p));
		imp_skin = register("imp_skin", p -> new Item(p));
		w_dust = register("w_dust", p -> new Item(p));
		w_obsidian_ingot = register("w_obsidian_ingot", p -> new Item(p));
		neridium_ingot = register("neridium_ingot", p -> new Item(p));
		pyridium_ingot = register("pyridium_ingot", p -> new Item(p));
		linium_ingot = register("linium_ingot", p -> new Item(p));
		red_feather = register("red_feather", p -> new Item(p));
		lava_book = register("lava_book", p -> new Item(p));
		lava_paper = register("lava_paper", p -> new Item(p));
		ghast_rod = register("ghast_rod", p -> new Item(p));
		ghast_bones = register("ghast_bones", p -> new Item(p));
		ghast_marrow = register("ghast_marrow", p -> new BoneMealItem(p));
		magma_caramel = register("magma_caramel", p -> new Item(p));

		glowood_sword = register("glowood_sword", p -> new SwordItem(NCItemTier.GLOWWOOD, 3, -2.4F, (p)));
		glowood_pickaxe = register("glowood_pickaxe", p -> new PickaxeItem(NCItemTier.GLOWWOOD, 1, -2.8F, (p)));
		glowood_axe = register("glowood_axe", p -> new AxeItem(NCItemTier.GLOWWOOD, 6.0F, -3.2F, (p)));
		glowood_shovel = register("glowood_shovel", p -> new ShovelItem(NCItemTier.GLOWWOOD, 1.5F, -3.0F, (p)));
		glowood_hoe = register("glowood_hoe", p -> new HoeItem(NCItemTier.GLOWWOOD, 0, -3.0F, (p)));

		netherrack_sword = register("netherrack_sword", p -> new SwordItem(NCItemTier.NETHERRACK, 3, -2.4F, (p)));
		netherrack_pickaxe = register("netherrack_pickaxe", p -> new PickaxeItem(NCItemTier.NETHERRACK, 1, -2.8F, (p)));
		netherrack_axe = register("netherrack_axe", p -> new AxeItem(NCItemTier.NETHERRACK, 7.0F, -3.2F, (p)));
		netherrack_shovel = register("netherrack_shovel", p -> new ShovelItem(NCItemTier.NETHERRACK, 1.5F, -3.0F, (p)));
		netherrack_hoe = register("netherrack_hoe", p -> new HoeItem(NCItemTier.NETHERRACK, -1, -2.0F, (p)));

		neridium_sword = register("neridium_sword", p -> new SwordItem(NCItemTier.NERIDIUM, 3, -2.4F, (p)));
		neridium_pickaxe = register("neridium_pickaxe", p -> new PickaxeItem(NCItemTier.NERIDIUM, 1, -2.8F, (p)));
		neridium_axe = register("neridium_axe", p -> new AxeItem(NCItemTier.NERIDIUM, 6.0F, -3.1F, (p)));
		neridium_shovel = register("neridium_shovel", p -> new ShovelItem(NCItemTier.NERIDIUM, 1.5F, -3.0F, (p)));
		neridium_hoe = register("neridium_hoe", p -> new HoeItem(NCItemTier.NERIDIUM, -2, -1.0F, (p)));

		pyridium_sword = register("pyridium_sword", p -> new SwordItem(NCItemTier.PYRIDIUM, 3, -2.4F, (p)));
		pyridium_pickaxe = register("pyridium_pickaxe", p -> new PickaxeItem(NCItemTier.PYRIDIUM, 1, -2.8F, (p)));
		pyridium_axe = register("pyridium_axe", p -> new AxeItem(NCItemTier.PYRIDIUM, 5.0F, -3.0F, (p)));
		pyridium_shovel = register("pyridium_shovel", p -> new ShovelItem(NCItemTier.PYRIDIUM, 1.5F, -3.0F, (p)));
		pyridium_hoe = register("pyridium_hoe", p -> new HoeItem(NCItemTier.PYRIDIUM, -3, -0.0F, (p)));

		linium_sword = register("linium_sword", p -> new SwordItem(NCItemTier.LINIUM, 3, -2.3F, (p)));
		linium_pickaxe = register("linium_pickaxe", p -> new PickaxeItem(NCItemTier.LINIUM, 1, -2.8F, (p)));
		linium_axe = register("linium_axe", p -> new AxeItem(NCItemTier.LINIUM, 5.0F, -3.0F, (p)));
		linium_shovel = register("linium_shovel", p -> new ShovelItem(NCItemTier.LINIUM, 1.5F, -3.0F, (p)));
		linium_hoe = register("linium_hoe", p -> new HoeItem(NCItemTier.LINIUM, -4, -0.0F, (p)));

		netherrack_bow = register("netherrack_bow", p -> new NetherBowItem(p), () -> props().durability(256));
		neridium_bow = register("neridium_bow", p -> new NetherBowItem(p), () -> props().durability(431));
		pyridium_bow = register("pyridium_bow", p -> new NetherBowItem(p), () -> props().durability(816));
		linium_bow = register("linium_bow", p -> new NetherBowItem(p), () -> props().durability(280));

		netherrack_arrow = register("netherrack_arrow", p -> new NetherArrowItem(p));
		neridium_arrow = register("neridium_arrow", p -> new NetherArrowItem(p));
		pyridium_arrow = register("pyridium_arrow", p -> new NetherArrowItem(p));
		linium_arrow = register("linium_arrow", p -> new NetherArrowItem(p));

		slime_eggs = register("slime_eggs", p -> new SlimeEggItem(p));
		neridium_lighter = register("neridium_lighter", p -> new FlintAndSteelItem(p.durability(32)));

		// Items
		dark_seeds = register("dark_seeds", p -> new BlockItem(NCBlocks.dark_wheat, p), () -> props().useBlockDescriptionPrefix());
		dark_wheat = register("dark_wheat", p -> new Item(p));
		devil_bread = register("devil_bread", p -> new Item(p.food(NCFoods.DEVIL_BREAD)));
		glowood_bowl = register("glowood_bowl", p -> new Item(p));
		glow_stew = register("glow_stew", p -> new Item(p), () -> new Item.Properties().stacksTo(1).food(NCFoods.GLOW_STEW).usingConvertsTo(glowood_bowl));
		glow_apple = register("glow_apple", p -> new Item(p.food(NCFoods.GLOW_APPLE)));

		ArmorType helmet = ArmorType.HELMET, chestplate = ArmorType.CHESTPLATE, leggings = ArmorType.LEGGINGS,
				boots = ArmorType.BOOTS;

		imp_helmet = register("imp_helmet", p -> new ArmorItem(NCArmorMaterial.IMP_SKIN, helmet, p));
		imp_chestplate = register("imp_chestplate", p -> new ArmorItem(NCArmorMaterial.IMP_SKIN, chestplate, p));
		imp_leggings = register("imp_leggings", p -> new ArmorItem(NCArmorMaterial.IMP_SKIN, leggings, p));
		imp_boots = register("imp_boots", p -> new ArmorItem(NCArmorMaterial.IMP_SKIN, boots, p));

		neridium_helmet = register("neridium_helmet", p -> new ArmorItem(NCArmorMaterial.NERIDIUM, helmet, p));
		neridium_chestplate = register("neridium_chestplate", p -> new ArmorItem(NCArmorMaterial.NERIDIUM, chestplate, p));
		neridium_leggings = register("neridium_leggings", p -> new ArmorItem(NCArmorMaterial.NERIDIUM, leggings, p));
		neridium_boots = register("neridium_boots", p -> new ArmorItem(NCArmorMaterial.NERIDIUM, boots, p));

		w_obsidian_helmet = register("w_obsidian_helmet", p -> new ArmorItem(NCArmorMaterial.W_OBSIDIAN, helmet, p));
		w_obsidian_chestplate = register("w_obsidian_chestplate", p -> new ArmorItem(NCArmorMaterial.W_OBSIDIAN, chestplate, p));
		w_obsidian_leggings = register("w_obsidian_leggings", p -> new ArmorItem(NCArmorMaterial.W_OBSIDIAN, leggings, p));
		w_obsidian_boots = register("w_obsidian_boots", p -> new ArmorItem(NCArmorMaterial.W_OBSIDIAN, boots, p));

		neridium_bucket = register("neridium_bucket", p -> new NeridiumBucketItem(Fluids.EMPTY, p.stacksTo(16)));
		neridium_lava_bucket = register("neridium_lava_bucket", p -> new NeridiumBucketItem(Fluids.LAVA, p.stacksTo(1).craftRemainder(neridium_bucket)));

		/*JukeboxSounds FIXME WHY NMOJABFG WGT DIUD YOU DO TYHISD I CAN:RT EVEN BEOUVE THIS 3*/
		imp_disc = register("imp_disc", p -> new Item(p), () -> props().stacksTo(1).jukeboxPlayable(JukeboxSongs.PIGSTEP));
	}

	/*private static void registerBlockItems()
	{
		for (int i3 = 0; i3 < NCBlocks.blockList.size(); ++i3)
			NethercraftRegistry.register(iItemRegistry, NCBlocks.blockList.get(i3).getRegistryName().toString().replace("nethercraft:", ""),p ->  new BlockItem(NCBlocks.blockList.get(i3), (new Item.Properties().tab(NetherItemGroup.BLOCKS))));
	
		lava_reeds = register("lava_reeds",p ->  new BlockItem(NCBlocks.lava_reeds, (new Item.Properties())));
	
		foulite_torch = register("foulite_torch",p ->  new StandingAndWallBlockItem(NCBlocks.foulite_torch, NCBlocks.foulite_wall_torch, (new Item.Properties()).tab(NetherItemGroup.BLOCKS)));
		charcoal_torch = register("charcoal_torch",p ->  new StandingAndWallBlockItem(NCBlocks.charcoal_torch, NCBlocks.charcoal_wall_torch, (new Item.Properties()).tab(NetherItemGroup.BLOCKS)));
	}*/

	private static void registerBlockItems()
	{
		for (Entry<String, Block> block : NCBlocks.blockItemList.entrySet())
			register(block.getKey(), p -> new BlockItem(block.getValue(), p), () -> props().useBlockDescriptionPrefix());

		lava_reeds = register("lava_reeds", p -> new BlockItem(NCBlocks.lava_reeds, p), () -> props().useBlockDescriptionPrefix());

		foulite_torch = register("foulite_torch", p -> new StandingAndWallBlockItem(NCBlocks.foulite_torch, NCBlocks.foulite_wall_torch, Direction.DOWN, p), () -> props().useBlockDescriptionPrefix());
		charcoal_torch = register("charcoal_torch", p -> new StandingAndWallBlockItem(NCBlocks.charcoal_torch, NCBlocks.charcoal_wall_torch, Direction.DOWN, p), () -> props().useBlockDescriptionPrefix());

		NCBlocks.blockItemList.clear();
	}

	private static Item register(String name, Function<Item.Properties, Item> itemFunc)
	{
		return register(name, itemFunc, null);
	}

	private static Item register(String name, Function<Item.Properties, Item> itemFunc, @Nullable Supplier<Item.Properties> newProps)
	{
		if (newProps == null)
			newProps = () -> props();

		Item.Properties props = newProps.get().setId(key(name));
		Item item = itemFunc.apply(props);

		// System.out.println("registering item " + name);

		registryEvent.register(Registries.ITEM, Nethercraft.locate(name), () -> item);
		return item;
	}

	private static ResourceKey<Item> key(String path)
	{
		return ResourceKey.create(Registries.ITEM, Nethercraft.locate(path));
	}

	private static final Item.Properties props()
	{
		return new Item.Properties();
	}
}