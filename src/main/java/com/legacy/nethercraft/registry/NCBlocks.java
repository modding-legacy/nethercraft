package com.legacy.nethercraft.registry;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.block.construction.DarkWheatBlock;
import com.legacy.nethercraft.block.construction.GhastBombBlock;
import com.legacy.nethercraft.block.construction.NCWorkbenchBlock;
import com.legacy.nethercraft.block.construction.NetherBookshelfBlock;
import com.legacy.nethercraft.block.construction.NetherChestBlock;
import com.legacy.nethercraft.block.construction.NetherrackFurnaceBlock;
import com.legacy.nethercraft.block.natural.GlowshroomBlock;
import com.legacy.nethercraft.block.natural.HeatSandBlock;
import com.legacy.nethercraft.block.natural.LavaReedsBlock;
import com.legacy.nethercraft.block.natural.NetherDirtBlock;
import com.legacy.nethercraft.block.natural.NetherFarmlandBlock;
import com.legacy.nethercraft.block.natural.NetherSaplingBlock;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.HugeMushroomBlock;
import net.minecraft.world.level.block.LadderBlock;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.NetherSproutsBlock;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.StainedGlassBlock;
import net.minecraft.world.level.block.StainedGlassPaneBlock;
import net.minecraft.world.level.block.TorchBlock;
import net.minecraft.world.level.block.WallTorchBlock;
import net.minecraft.world.level.block.grower.TreeGrower;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.registries.RegisterEvent;

public class NCBlocks
{
	public static Map<String, Block> blockItemList = new HashMap<>();

	public static final Block nether_dirt = createBlock("nether_dirt", NetherDirtBlock::new, () -> rawCopy(Blocks.COARSE_DIRT).strength(0.5F, 2.0F), true);
	public static final Block heat_sand = createBlock("heat_sand", HeatSandBlock::new, glowingCopy(Blocks.RED_SAND, 9), true);

	public static Block glowroots, nether_farmland, lava_reeds;

	public static Block glowood_chest, netherrack_furnace;

	public static Block glowood_crafting_table, glowood_leaves, glowood_log, glowood_wood, stripped_glowood_log,
			stripped_glowood_wood, glowood_planks, glowood_stairs, glowood_slab, glowood_fence, glowood_fence_gate,
			glowood_door, glowood_trapdoor, glowood_pressure_plate, glowood_button, glowood_sapling, glowood_bookshelf,
			glowood_sign, glowood_wall_sign, glowood_hanging_sign, glowood_wall_hanging_sign, glowood_ladder;

	public static Block w_ore, foulite_ore, neridium_ore, pyridium_ore, linium_ore;

	public static Block foulite_torch, charcoal_torch, foulite_wall_torch, charcoal_wall_torch;

	public static Block neridium_block, pyridium_block, linium_block;

	public static Block soul_glass, heat_glass, soul_glass_pane, heat_glass_pane;

	public static Block green_glowshroom, purple_glowshroom, green_glowshroom_block, purple_glowshroom_block,
			glowshroom_stem;

	public static Block ghast_bomb;

	public static Block dark_wheat;

	public static Block smooth_netherrack, smooth_netherrack_slab, netherrack_stairs;

	public static Block potted_glowood_sapling, potted_green_glowshroom, potted_purple_glowshroom;

	// NetherFeatures.NETHER_TREE.configured(NetherFeatures.GLOWOOD_TREE_CONFIG);
	public static final Lazy<TreeGrower> GLOWOOD_GROWER = Lazy.of(() -> new TreeGrower("glowood", Optional.empty(), Optional.of(NCFeatures.Configured.GLOWOOD_TREE.getKey()), Optional.empty()));

	private static RegisterEvent registryEvent;

	public static void init(RegisterEvent event)
	{
		NCBlocks.registryEvent = event;

		registryEvent.register(Registries.BLOCK, Nethercraft.locate("nether_dirt"), () -> nether_dirt);
		registryEvent.register(Registries.BLOCK, Nethercraft.locate("heat_sand"), () -> heat_sand);

		glowood_crafting_table = register("glowood_crafting_table", NCWorkbenchBlock::new, glowingCopy(Blocks.CRAFTING_TABLE, 8));
		glowood_leaves = register("glowood_leaves", LeavesBlock::new, () -> rawGlowingCopy(Blocks.OAK_LEAVES, 13).strength(0.2F, 400.0F));
		glowood_log = register("glowood_log", RotatedPillarBlock::new, () -> (rawGlowingCopy(Blocks.CHERRY_LOG, 6).strength(2.0F, 1000.0F)));
		glowood_wood = register("glowood_wood", RotatedPillarBlock::new, copy(glowood_log));
		stripped_glowood_log = register("stripped_glowood_log", RotatedPillarBlock::new, copy(glowood_log));
		stripped_glowood_wood = register("stripped_glowood_wood", RotatedPillarBlock::new, copy(glowood_log));
		glowood_planks = register("glowood_planks", Block::new, glowingCopy(Blocks.CHERRY_PLANKS, 8));
		glowood_bookshelf = register("glowood_bookshelf", NetherBookshelfBlock::new, glowingCopy(Blocks.BOOKSHELF, 8));
		glowood_stairs = BlockReg.stairs("glowood_stairs", glowood_planks);
		glowood_slab = BlockReg.slab("glowood_slab", glowood_planks);
		glowood_fence = BlockReg.fence("glowood_fence");
		glowood_fence_gate = BlockReg.gate("glowood_fence_gate", NCWoodTypes.GLOWOOD);
		glowood_door = register("glowood_door", p -> new DoorBlock(NCBlockSets.GLOWOOD, p), () -> rawGlowingCopy(Blocks.CHERRY_DOOR, 8).sound(SoundType.CHERRY_WOOD));
		glowood_trapdoor = BlockReg.trapdoor("glowood_trapdoor", NCBlockSets.GLOWOOD);
		glowood_pressure_plate = BlockReg.woodenPressurePlate("glowood_pressure_plate", NCBlockSets.GLOWOOD);
		glowood_button = BlockReg.woodenButton("glowood_button", NCBlockSets.GLOWOOD);
		glowood_sapling = register("glowood_sapling", p -> new NetherSaplingBlock(GLOWOOD_GROWER.get(), p), () -> rawGlowingCopy(Blocks.CHERRY_SAPLING, 4).emissiveRendering(NCBlocks::always)); // FIXME
		/*glowood_sign = registerBlock("glowood_sign", p -> new PremiumStandingSignBlock(PWWoodTypes.glowood, p), signProps);
		glowood_wall_sign = registerBlock("glowood_wall_sign", p -> new PremiumWallSignBlock(PWWoodTypes.glowood, p), signProps);
		glowood_hanging_sign = registerBlock("glowood_hanging_sign", p -> new PremiumCeilingHangingSignBlock(PWWoodTypes.glowood, p), hangingSignProps);
		glowood_wall_hanging_sign = registerBlock("glowood_wall_hanging_sign", p -> new PremiumWallHangingSignBlock(PWWoodTypes.glowood, p), hangingSignProps);*/
		glowood_ladder = register("glowood_ladder", LadderBlock::new, () -> rawGlowingCopy((Blocks.LADDER), 6).sound(SoundType.CHERRY_WOOD));
		glowood_chest = register("glowood_chest", NetherChestBlock::new, () -> rawGlowingCopy(Blocks.CHEST, 8).sound(SoundType.CHERRY_WOOD));

		/*nether_dirt = register("nether_dirt", NetherDirtBlock::new, () -> rawCopy(Blocks.COARSE_DIRT).strength(0.5F, 2.0F));*/
		nether_farmland = register("nether_farmland", NetherFarmlandBlock::new, copy(nether_dirt));
		/*heat_sand = register("heat_sand", HeatSandBlock::new, createGlowingProperties(Blocks.RED_SAND, 9));*/
		glowroots = register("glowroots", NetherSproutsBlock::new, () -> rawGlowingCopy(Blocks.NETHER_SPROUTS, 8).emissiveRendering(NCBlocks::always));
		foulite_ore = register("foulite_ore", Block::new, () -> rawCopy(Blocks.COAL_ORE).sound(SoundType.NETHER_ORE));
		neridium_ore = register("neridium_ore", Block::new, () -> rawCopy(Blocks.IRON_ORE).sound(SoundType.NETHER_ORE));
		pyridium_ore = register("pyridium_ore", Block::new, () -> rawCopy(Blocks.DIAMOND_ORE).sound(SoundType.NETHER_ORE));
		linium_ore = register("linium_ore", Block::new, () -> rawCopy(Blocks.EMERALD_ORE).sound(SoundType.NETHER_ORE));
		w_ore = register("w_ore", Block::new, () -> rawCopy(Blocks.DEEPSLATE_EMERALD_ORE).sound(SoundType.NETHER_ORE));
		neridium_block = register("neridium_block", Block::new, copy(Blocks.IRON_BLOCK));
		pyridium_block = register("pyridium_block", Block::new, copy((Blocks.DIAMOND_BLOCK)));
		linium_block = register("linium_block", Block::new, (glowingCopy((Blocks.DIAMOND_BLOCK), 15)));
		soul_glass = register("slow_glass", p -> new StainedGlassBlock(DyeColor.BROWN, p), copy(Blocks.GLASS)); // FIXME
		heat_glass = register("heat_glass", p -> new StainedGlassBlock(DyeColor.ORANGE, p), glowingCopy(Blocks.GLASS, 9));
		soul_glass_pane = register("slow_glass_pane", p -> new StainedGlassPaneBlock(DyeColor.BROWN, p), copy(Blocks.GLASS_PANE)); // FIXME
		heat_glass_pane = register("heat_glass_pane", p -> new StainedGlassPaneBlock(DyeColor.ORANGE, p), glowingCopy(Blocks.GLASS_PANE, 9));
		green_glowshroom = register("green_glowshroom", p -> new GlowshroomBlock(NCFeatures.Configured.HUGE_GREEN_GLOWSHROOM.getKey(), p), () -> rawGlowingCopy(Blocks.RED_MUSHROOM, 7).emissiveRendering(NCBlocks::always).sound(SoundType.FUNGUS));
		purple_glowshroom = register("purple_glowshroom", p -> new GlowshroomBlock(NCFeatures.Configured.HUGE_PURPLE_GLOWSHROOM.getKey(), p), () -> rawGlowingCopy(Blocks.BROWN_MUSHROOM, 5).emissiveRendering(NCBlocks::always).sound(SoundType.FUNGUS));

		netherrack_furnace = register("netherrack_furnace", NetherrackFurnaceBlock::new, copy(Blocks.FURNACE));
		ghast_bomb = register("ghast_bomb", GhastBombBlock::new, glowingCopy(Blocks.RED_SAND, 9));
		foulite_torch = registerBlock("foulite_torch", p -> new TorchBlock(ParticleTypes.SMOKE, p), copy(Blocks.TORCH));
		charcoal_torch = registerBlock("charcoal_torch", p -> new TorchBlock(ParticleTypes.FLAME, p), copy(Blocks.TORCH));
		foulite_wall_torch = registerBlock("foulite_wall_torch", p -> new WallTorchBlock(ParticleTypes.SMOKE, p), copy(Blocks.WALL_TORCH));
		charcoal_wall_torch = registerBlock("charcoal_wall_torch", p -> new WallTorchBlock(ParticleTypes.FLAME, p), copy(Blocks.WALL_TORCH));
		lava_reeds = registerBlock("lava_reeds", LavaReedsBlock::new, copy(Blocks.SUGAR_CANE));
		dark_wheat = registerBlock("dark_wheat", DarkWheatBlock::new, copy(Blocks.WHEAT));
		smooth_netherrack = register("smooth_netherrack", Block::new, () -> rawCopy(Blocks.NETHERRACK).sound(SoundType.STONE));
		smooth_netherrack_slab = BlockReg.slab("smooth_netherrack_slab", smooth_netherrack);

		green_glowshroom_block = register("green_glowshroom_block", HugeMushroomBlock::new, () -> rawGlowingCopy(Blocks.RED_MUSHROOM_BLOCK, 15).sound(SoundType.STEM));
		purple_glowshroom_block = register("purple_glowshroom_block", HugeMushroomBlock::new, () -> rawGlowingCopy(Blocks.BROWN_MUSHROOM_BLOCK, 15).sound(SoundType.STEM));
		glowshroom_stem = register("glowshroom_stem", HugeMushroomBlock::new, () -> rawCopy(Blocks.MUSHROOM_STEM).sound(SoundType.STEM));

		potted_glowood_sapling = BlockReg.flowerPot("potted_glowood_sapling", () -> glowood_sapling);
		potted_green_glowshroom = BlockReg.flowerPot("potted_green_glowshroom", () -> green_glowshroom);
		potted_purple_glowshroom = BlockReg.flowerPot("potted_purple_glowshroom", () -> purple_glowshroom);
	}

	private static Supplier<BlockBehaviour.Properties> glowingCopy(Block copy, int lightAmount)
	{
		return () -> rawGlowingCopy(copy, lightAmount);
	}

	private static BlockBehaviour.Properties rawGlowingCopy(Block copy, int lightAmount)
	{
		return rawCopy(copy).lightLevel((value) -> lightAmount);
	}

	private static Supplier<BlockBehaviour.Properties> copy(Block from)
	{
		return () -> rawCopy(from);
	}

	private static BlockBehaviour.Properties rawCopy(Block from)
	{
		return BlockBehaviour.Properties.ofFullCopy(from);
	}

	public static Block register(String key, Function<BlockBehaviour.Properties, Block> blockFunc, Supplier<BlockBehaviour.Properties> newProps)
	{
		Block block = registerBlock(key, blockFunc, newProps, true);
		return block;
	}

	public static Block registerBlock(String name, Function<BlockBehaviour.Properties, Block> blockFunc, Supplier<BlockBehaviour.Properties> newProps)
	{
		return registerBlock(name, blockFunc, newProps, false);
	}

	public static Block registerBlock(String name, Function<BlockBehaviour.Properties, Block> blockFunc, Supplier<BlockBehaviour.Properties> newProps, boolean item)
	{
		if (registryEvent != null)
		{
			Block block = createBlock(name, blockFunc, newProps, item);

			// System.out.println("registering " + name);
			registryEvent.register(Registries.BLOCK, Nethercraft.locate(name), () -> block);

			return block;
		}

		return Blocks.STONE;
	}

	public static final Block createBlock(String key, Function<BlockBehaviour.Properties, Block> blockFunc, Supplier<BlockBehaviour.Properties> newProps, boolean item)
	{
		BlockBehaviour.Properties props = newProps.get().setId(key(key));
		Block block = blockFunc.apply(props);

		if (item)
			blockItemList.put(key, block);

		return block;
	}

	private static ResourceKey<Block> key(String path)
	{
		return ResourceKey.create(Registries.BLOCK, Nethercraft.locate(path));
	}

	private static boolean always(BlockState state, BlockGetter blockGetter, BlockPos pos)
	{
		return true;
	}

	@SuppressWarnings("unused")
	private static boolean never(BlockState state, BlockGetter blockGetter, BlockPos pos)
	{
		return false;
	}
}