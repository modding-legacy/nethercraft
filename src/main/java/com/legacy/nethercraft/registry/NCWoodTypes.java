package com.legacy.nethercraft.registry;

import com.legacy.nethercraft.Nethercraft;

import net.minecraft.world.level.block.state.properties.BlockSetType;
import net.minecraft.world.level.block.state.properties.WoodType;

public class NCWoodTypes
{
	public static final WoodType GLOWOOD = register("glowood", NCBlockSets.GLOWOOD);

	public static WoodType register(String key, BlockSetType blockSet)
	{	
		/*p_273766_, p_273104_, SoundType.WOOD, SoundType.HANGING_SIGN, SoundEvents.FENCE_GATE_CLOSE, SoundEvents.FENCE_GATE_OPEN*/
		return WoodType.register(new WoodType(Nethercraft.find(key), blockSet));
	}

	public static void init()
	{
	}
}