package com.legacy.nethercraft.registry;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.entity.BloodyZombieEntity;
import com.legacy.nethercraft.entity.CamouflageSpiderEntity;
import com.legacy.nethercraft.entity.DarkZombieEntity;
import com.legacy.nethercraft.entity.ImpEntity;
import com.legacy.nethercraft.entity.LavaSlimeEntity;
import com.legacy.nethercraft.entity.misc.GhastBombEntity;
import com.legacy.nethercraft.entity.projectile.NCArrowEntity;
import com.legacy.nethercraft.entity.projectile.SlimeEggEntity;
import com.legacy.nethercraft.entity.tribal.TribalArcherEntity;
import com.legacy.nethercraft.entity.tribal.TribalTraineeEntity;
import com.legacy.nethercraft.entity.tribal.TribalWarriorEntity;

import net.minecraft.core.BlockPos;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.Difficulty;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntitySpawnReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.SpawnPlacementTypes;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.levelgen.Heightmap;
import net.neoforged.neoforge.event.entity.EntityAttributeCreationEvent;
import net.neoforged.neoforge.event.entity.RegisterSpawnPlacementsEvent;
import net.neoforged.neoforge.registries.RegisterEvent;

public class NCEntityTypes
{
	public static final EntityType<ImpEntity> IMP = buildEntity("imp", EntityType.Builder.of(ImpEntity::new, MobCategory.CREATURE).fireImmune().sized(1.0F, 1.8F));

	public static final EntityType<DarkZombieEntity> DARK_ZOMBIE = buildEntity("dark_zombie", EntityType.Builder.of(DarkZombieEntity::new, MobCategory.MONSTER).sized(0.6F, 1.95F));
	public static final EntityType<BloodyZombieEntity> BLOODY_ZOMBIE = buildEntity("bloody_zombie", EntityType.Builder.of(BloodyZombieEntity::new, MobCategory.MONSTER).sized(0.6F, 1.95F));
	public static final EntityType<LavaSlimeEntity> LAVA_SLIME = buildEntity("lava_slime", EntityType.Builder.of(LavaSlimeEntity::new, MobCategory.MONSTER).fireImmune().sized(2.04F, 2.04F));
	public static final EntityType<CamouflageSpiderEntity> CAMOUFLAGE_SPIDER = buildEntity("camouflage_spider", EntityType.Builder.of(CamouflageSpiderEntity::new, MobCategory.MONSTER).fireImmune().sized(1.4F, 0.9F));

	public static final EntityType<TribalWarriorEntity> TRIBAL_WARRIOR = buildEntity("tribal_warrior", EntityType.Builder.of(TribalWarriorEntity::new, MobCategory.MONSTER).fireImmune().sized(0.6F, 1.95F));
	public static final EntityType<TribalArcherEntity> TRIBAL_ARCHER = buildEntity("tribal_archer", EntityType.Builder.of(TribalArcherEntity::new, MobCategory.MONSTER).fireImmune().sized(0.6F, 1.95F));
	public static final EntityType<TribalTraineeEntity> TRIBAL_TRAINEE = buildEntity("tribal_trainee", EntityType.Builder.of(TribalTraineeEntity::new, MobCategory.MONSTER).fireImmune().sized(0.4F, 1.3F));

	public static final EntityType<NCArrowEntity.Netherrack> NETHERRACK_ARROW = buildEntity("netherrack_arrow", EntityType.Builder.<NCArrowEntity.Netherrack>of(NCArrowEntity.Netherrack::new, MobCategory.MISC).setShouldReceiveVelocityUpdates(true).sized(0.5F, 0.5F));
	public static final EntityType<NCArrowEntity.Neridium> NERIDIUM_ARROW = buildEntity("neridium_arrow", EntityType.Builder.<NCArrowEntity.Neridium>of(NCArrowEntity.Neridium::new, MobCategory.MISC).setShouldReceiveVelocityUpdates(true).sized(0.5F, 0.5F));
	public static final EntityType<NCArrowEntity.Pyridium> PYRIDIUM_ARROW = buildEntity("pyridium_arrow", EntityType.Builder.<NCArrowEntity.Pyridium>of(NCArrowEntity.Pyridium::new, MobCategory.MISC).setShouldReceiveVelocityUpdates(true).sized(0.5F, 0.5F));
	public static final EntityType<NCArrowEntity.Linium> LINIUM_ARROW = buildEntity("linium_arrow", EntityType.Builder.<NCArrowEntity.Linium>of(NCArrowEntity.Linium::new, MobCategory.MISC).setShouldReceiveVelocityUpdates(true).sized(0.5F, 0.5F));
	public static final EntityType<SlimeEggEntity> SLIME_EGGS = buildEntity("slime_eggs", EntityType.Builder.<SlimeEggEntity>of(SlimeEggEntity::new, MobCategory.MISC).setShouldReceiveVelocityUpdates(true).sized(0.25F, 0.25F));
	public static final EntityType<GhastBombEntity> GHAST_BOMB = buildEntity("ghast_bomb", EntityType.Builder.<GhastBombEntity>of(GhastBombEntity::new, MobCategory.MISC).setShouldReceiveVelocityUpdates(true).fireImmune().sized(0.98F, 0.98F));

	private static RegisterEvent registerEvent;

	public static void init(RegisterEvent event)
	{
		registerEvent = event;

		register("imp", IMP);
		register("dark_zombie", DARK_ZOMBIE);
		register("bloody_zombie", BLOODY_ZOMBIE);
		register("lava_slime", LAVA_SLIME);
		register("camouflage_spider", CAMOUFLAGE_SPIDER);
		register("tribal_warrior", TRIBAL_WARRIOR);
		register("tribal_archer", TRIBAL_ARCHER);
		register("tribal_trainee", TRIBAL_TRAINEE);
		register("netherrack_arrow", NETHERRACK_ARROW);
		register("neridium_arrow", NERIDIUM_ARROW);
		register("pyridium_arrow", PYRIDIUM_ARROW);
		register("linium_arrow", LINIUM_ARROW);
		register("slime_eggs", SLIME_EGGS);
		register("ghast_bomb", GHAST_BOMB);
	}

	private static void register(String name, EntityType<?> type)
	{
		if (registerEvent == null)
			return;

		registerEvent.register(Registries.ENTITY_TYPE, Nethercraft.locate(name), () -> type);
	}

	public static void onAttributesRegistered(EntityAttributeCreationEvent event)
	{
		event.put(IMP, ImpEntity.createAttributes().build());
		event.put(DARK_ZOMBIE, DarkZombieEntity.createDarkZombieAttributes().build());
		event.put(BLOODY_ZOMBIE, BloodyZombieEntity.createAttributes().build());
		event.put(LAVA_SLIME, Monster.createMonsterAttributes().build());
		event.put(CAMOUFLAGE_SPIDER, CamouflageSpiderEntity.createAttributes().build());
		event.put(TRIBAL_WARRIOR, TribalWarriorEntity.createAttributes().build());
		event.put(TRIBAL_ARCHER, TribalArcherEntity.createAttributes().build());
		event.put(TRIBAL_TRAINEE, TribalTraineeEntity.createAttributes().build());
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(ResourceKey.create(Registries.ENTITY_TYPE, Nethercraft.locate(key)));
	}

	public static void registerPlacements(RegisterSpawnPlacementsEvent event)
	{
		event.register(IMP, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, NCEntityTypes::canSpawn, RegisterSpawnPlacementsEvent.Operation.REPLACE);
		event.register(DARK_ZOMBIE, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, NCEntityTypes::canSpawn, RegisterSpawnPlacementsEvent.Operation.REPLACE);
		event.register(BLOODY_ZOMBIE, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, NCEntityTypes::canSpawn, RegisterSpawnPlacementsEvent.Operation.REPLACE);
		event.register(LAVA_SLIME, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, NCEntityTypes::canSpawn, RegisterSpawnPlacementsEvent.Operation.REPLACE);
		event.register(CAMOUFLAGE_SPIDER, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, NCEntityTypes::canSpawn, RegisterSpawnPlacementsEvent.Operation.REPLACE);
		event.register(TRIBAL_WARRIOR, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, NCEntityTypes::canSpawn, RegisterSpawnPlacementsEvent.Operation.REPLACE);
		event.register(TRIBAL_ARCHER, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, NCEntityTypes::canSpawn, RegisterSpawnPlacementsEvent.Operation.REPLACE);
		event.register(TRIBAL_TRAINEE, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, NCEntityTypes::canSpawn, RegisterSpawnPlacementsEvent.Operation.REPLACE);
	}

	public static void registerPlacementOverrides(RegisterSpawnPlacementsEvent event)
	{
	}

	public static boolean canSpawn(EntityType<? extends Mob> type, ServerLevelAccessor world, EntitySpawnReason reason, BlockPos pos, RandomSource random)
	{
		return world.getDifficulty() != Difficulty.PEACEFUL;
	}

	public static boolean canSpawnSand(EntityType<? extends Mob> type, ServerLevelAccessor world, EntitySpawnReason reason, BlockPos pos, RandomSource random)
	{
		return canSpawn(type, world, reason, pos, random) && world.getBlockState(pos.below()).getBlock() == NCBlocks.heat_sand;
	}
}
