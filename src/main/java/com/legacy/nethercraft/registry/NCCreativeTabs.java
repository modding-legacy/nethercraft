package com.legacy.nethercraft.registry;

import java.util.List;
import java.util.stream.Collectors;

import com.legacy.nethercraft.Nethercraft;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.SpawnEggItem;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.common.EventBusSubscriber.Bus;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;
import net.neoforged.neoforge.registries.RegisterEvent;

@EventBusSubscriber(modid = Nethercraft.MODID, bus = Bus.MOD)
public class NCCreativeTabs
{
	public static final Lazy<List<Item>> SPAWN_EGGS = Lazy.of(() -> BuiltInRegistries.ITEM.stream().filter(item -> BuiltInRegistries.ITEM.getKey(item).getNamespace().equals(Nethercraft.MODID) && item instanceof SpawnEggItem).sorted((o, n) -> BuiltInRegistries.ITEM.getKey(o).compareTo(BuiltInRegistries.ITEM.getKey(n))).collect(Collectors.toList()));

	@SubscribeEvent
	public static void modifyExisting(BuildCreativeModeTabContentsEvent event)
	{
		if (event.getTabKey() == CreativeModeTabs.SPAWN_EGGS)
		{
			for (Item item : SPAWN_EGGS.get())
				event.accept(item);
		}
	}

	@SubscribeEvent
	public static void init(RegisterEvent event)
	{
		// @formatter:off
		event.register(Registries.CREATIVE_MODE_TAB, Nethercraft.locate("all_items"), () -> CreativeModeTab.builder().icon(() -> NCBlocks.nether_dirt.asItem().getDefaultInstance()).title(Component.translatable("advancements.nethercraft.root.title"))
				.displayItems((params, output) ->
				{
					List<Item> all = BuiltInRegistries.ITEM.stream().filter(item -> BuiltInRegistries.ITEM.getKey(item).getNamespace().equals(Nethercraft.MODID) && !(item instanceof SpawnEggItem)).sorted((o, n) -> BuiltInRegistries.ITEM.getKey(o).compareTo(BuiltInRegistries.ITEM.getKey(n))).collect(Collectors.toList());

					for (Item item : all)
						output.accept(item);
					
					for (Item item : SPAWN_EGGS.get())
						output.accept(item);
				}).build());
		// @formatter:on
	}

	/*private static void insertAfter(BuildCreativeModeTabContentsEvent event, List<ItemLike> items, ItemLike target)
	{
		ItemStack currentStack = null;
	
		for (ItemStack e : event.getParentEntries())
		{
			if (e.getItem() == target)
			{
				currentStack = e;
				break;
			}
		}
	
		for (var item : items)
			event.insertAfter(currentStack, currentStack = item.asItem().getDefaultInstance(), TabVisibility.PARENT_AND_SEARCH_TABS);
	}*/

	/*protected static void printMissingItems(BuildCreativeModeTabContentsEvent event)
	{
		try
		{
			Set<Item> items = new HashSet<>();
	
			for (var e : event.getEntries())
				items.add(e.getKey().getItem());
	
			List<Item> missing = BuiltInRegistries.ITEM.stream().filter(block -> BuiltInRegistries.ITEM.getKey(block).getNamespace().equals(GoodNightSleep.MODID) && !items.contains(block)).collect(Collectors.toList());
	
			for (var e : missing)
				System.out.println(BuiltInRegistries.ITEM.getKey(e));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}*/
}
