package com.legacy.nethercraft.registry;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.client.particle.GlowshroomSporeParticle;

import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.particles.SimpleParticleType;
import net.minecraft.core.registries.Registries;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.registries.RegisterEvent;

public class NCParticles
{
	public static final SimpleParticleType GLOWSHROOM_SPORE = new SimpleParticleType(false);

	public static void init(RegisterEvent event)
	{
		register(event, "glowshroom_spore", GLOWSHROOM_SPORE);
	}

	private static void register(RegisterEvent event, String key, ParticleType<?> particle)
	{
		event.register(Registries.PARTICLE_TYPE, Nethercraft.locate(key), () -> particle);
	}

	@OnlyIn(Dist.CLIENT)
	@OnlyIn(Dist.CLIENT)
	public static class Factory
	{
		public static void init(net.neoforged.neoforge.client.event.RegisterParticleProvidersEvent event)
		{
			event.registerSpriteSet(GLOWSHROOM_SPORE, GlowshroomSporeParticle.Factory::new);
		}
	}
}
