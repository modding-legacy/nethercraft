package com.legacy.nethercraft.registry;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

import org.apache.commons.lang3.tuple.Pair;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.Pointer;
import com.legacy.nethercraft.world.feature.HugeGreenGlowshroomFeature;
import com.legacy.nethercraft.world.feature.HugePurpleGlowshroomFeature;
import com.legacy.nethercraft.world.feature.VolcanicGeyserFeature;
import com.legacy.nethercraft.world.feature.util.GreenGlowshroomPieces;
import com.legacy.nethercraft.world.feature.util.PurpleGlowshroomPieces;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BiomeDefaultFeatures;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.placement.MiscOverworldPlacements;
import net.minecraft.data.worldgen.placement.NetherPlacements;
import net.minecraft.data.worldgen.placement.OrePlacements;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.data.worldgen.placement.VegetationPlacements;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.random.SimpleWeightedRandomList;
import net.minecraft.util.valueproviders.BiasedToBottomInt;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.WeightedPlacedFeature;
import net.minecraft.world.level.levelgen.feature.configurations.BlockColumnConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.OreConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.RandomFeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.RandomPatchConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.SimpleBlockConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;
import net.minecraft.world.level.levelgen.feature.featuresize.TwoLayersFeatureSize;
import net.minecraft.world.level.levelgen.feature.foliageplacers.BlobFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.FancyFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.stateproviders.WeightedStateProvider;
import net.minecraft.world.level.levelgen.feature.trunkplacers.ForkingTrunkPlacer;
import net.minecraft.world.level.levelgen.feature.trunkplacers.StraightTrunkPlacer;
import net.minecraft.world.level.levelgen.placement.BiomeFilter;
import net.minecraft.world.level.levelgen.placement.BlockPredicateFilter;
import net.minecraft.world.level.levelgen.placement.CountOnEveryLayerPlacement;
import net.minecraft.world.level.levelgen.placement.CountPlacement;
import net.minecraft.world.level.levelgen.placement.HeightRangePlacement;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.NoiseThresholdCountPlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;
import net.minecraft.world.level.levelgen.placement.RarityFilter;
import net.minecraft.world.level.levelgen.structure.templatesystem.TagMatchTest;
import net.minecraft.world.level.material.Fluids;
import net.neoforged.neoforge.data.loading.DatagenModLoader;
import net.neoforged.neoforge.registries.RegisterEvent;

public class NCFeatures
{
	public static final Feature<NoneFeatureConfiguration> HUGE_PURPLE_GLOWSHROOM = new HugePurpleGlowshroomFeature(NoneFeatureConfiguration.CODEC);
	public static final Feature<NoneFeatureConfiguration> HUGE_GREEN_GLOWSHROOM = new HugeGreenGlowshroomFeature(NoneFeatureConfiguration.CODEC);
	public static final Feature<NoneFeatureConfiguration> VOLCANIC_GEYSER = new VolcanicGeyserFeature(NoneFeatureConfiguration.CODEC);

	public static RegisterEvent registerEvent;

	public static void init(RegisterEvent event)
	{
		registerEvent = event;

		GreenGlowshroomPieces.init();
		PurpleGlowshroomPieces.init();

		register("huge_purple_glowshroom", HUGE_PURPLE_GLOWSHROOM);
		register("huge_green_glowshroom", HUGE_GREEN_GLOWSHROOM);
		register("volcanic_geyser", VOLCANIC_GEYSER);

		Configured.init();
		Placements.init();
	}

	private static void register(String key, Feature<?> feature)
	{
		registerEvent.register(Registries.FEATURE, Nethercraft.locate(key), () -> feature);
	}

	public static void addNetherOres(NCBiomes.Builders.RegistrarBuilder biomeIn)
	{
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.FOULITE_ORE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.NERIDIUM_ORE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.PYRIDIUM_ORE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.LINIUM_ORE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.W_ORE);
	}

	public static void addGroveFeatures(NCBiomes.Builders.RegistrarBuilder biomeIn)
	{
		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.GLOWING_GROVE_VEGETATION);
		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.GLOWING_GROVE_FOLIAGE);

		NCFeatures.addSmallGlowshrooms(biomeIn);
	}

	public static void addGardenFeatures(NCBiomes.Builders.RegistrarBuilder biomeIn)
	{
		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.GLOWSHROOM_GARDEN_VEGETATION);

		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.GLOWSHROOM_64);
		NCFeatures.addSmallGlowshrooms(biomeIn);
	}

	public static void addRushesFeatures(NCBiomes.Builders.RegistrarBuilder biomeIn)
	{
		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.VOLCANIC_RUSHES_GEYSERS);
		biomeIn.addFeature(GenerationStep.Decoration.LOCAL_MODIFICATIONS, NetherPlacements.BASALT_PILLAR);
	}

	public static void addSmallGlowshrooms(NCBiomes.Builders.RegistrarBuilder biomeIn)
	{
		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.GLOWSHROOM_10);
	}

	public static void addLavaReeds(NCBiomes.Builders.RegistrarBuilder biomeIn)
	{
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, Placements.LAVA_REEDS);
	}

	/*The original Nethercraft spawns, er... at least the previous version anyway. Will be removed.*/
	/*@Deprecated
	public static void addOldNetherSpawns(MobSpawnInfoBuilder biomeIn)
	{
		biomeIn.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(NCEntityTypes.IMP, 30, 2, 3));
	
		biomeIn.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NCEntityTypes.DARK_ZOMBIE, 50, 0, 2));
		biomeIn.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NCEntityTypes.BLOODY_ZOMBIE, 50, 0, 2));
		biomeIn.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NCEntityTypes.CAMOUFLAGE_SPIDER, 60, 1, 3));
		biomeIn.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NCEntityTypes.LAVA_SLIME, 40, 1, 2));
	
		biomeIn.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NCEntityTypes.TRIBAL_WARRIOR, 50, 0, 3));
		biomeIn.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NCEntityTypes.TRIBAL_ARCHER, 50, 1, 2));
		biomeIn.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NCEntityTypes.TRIBAL_TRAINEE, 50, 1, 3));
	}*/

	// Based off of the Nether Wastes biome
	public static void addDefaultNetherFeatures(NCBiomes.Builders.RegistrarBuilder biomeIn)
	{
		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, MiscOverworldPlacements.SPRING_LAVA);
		BiomeDefaultFeatures.addDefaultMushrooms(biomeIn);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, NetherPlacements.SPRING_OPEN);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, NetherPlacements.PATCH_FIRE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, NetherPlacements.PATCH_SOUL_FIRE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, NetherPlacements.GLOWSTONE_EXTRA);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, NetherPlacements.GLOWSTONE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, VegetationPlacements.BROWN_MUSHROOM_NETHER);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, VegetationPlacements.RED_MUSHROOM_NETHER);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, OrePlacements.ORE_MAGMA);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, NetherPlacements.SPRING_CLOSED);
		BiomeDefaultFeatures.addNetherDefaultOres(biomeIn);
		// addNetherOres(biomeIn);
	}

	public static class Configured
	{

		/*public static final RegistrarHandler<ConfiguredFeature<?, ?>> HANDLER = RegistrarHandler.getOrCreate(Registries.CONFIGURED_FEATURE, Nethercraft.MODID);*/
		private static final Map<ResourceLocation, Pointer<ConfiguredFeature<?, ?>>> POINTERS = new HashMap<>();

		public static final TreeConfiguration GLOWOOD_TREE_CONFIG = (new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(NCBlocks.glowood_log.defaultBlockState()), new ForkingTrunkPlacer(4, 3, 2), BlockStateProvider.simple(NCBlocks.glowood_leaves.defaultBlockState()), new FancyFoliagePlacer(ConstantInt.of(2), UniformInt.of(1, 2), 4), new TwoLayersFeatureSize(3, 0, 3))).dirt(BlockStateProvider.simple(NCBlocks.nether_dirt.defaultBlockState())).ignoreVines().build();

		public static final Pointer<ConfiguredFeature<?, ?>> GLOWOOD_TREE = register("glowood_tree", Feature.TREE, () -> GLOWOOD_TREE_CONFIG);
		public static final Pointer<ConfiguredFeature<?, ?>> HUGE_PURPLE_GLOWSHROOM = register("huge_purple_glowshroom", NCFeatures.HUGE_PURPLE_GLOWSHROOM, () -> FeatureConfiguration.NONE);
		public static final Pointer<ConfiguredFeature<?, ?>> HUGE_GREEN_GLOWSHROOM = register("huge_green_glowshroom", NCFeatures.HUGE_GREEN_GLOWSHROOM, () -> FeatureConfiguration.NONE);
		public static final Pointer<ConfiguredFeature<?, ?>> VOLCANIC_GEYSER = register("volcanic_geyser", NCFeatures.VOLCANIC_GEYSER, () -> FeatureConfiguration.NONE);

		public static final Pointer<ConfiguredFeature<?, ?>> GLOWSHROOM_GARDEN_VEGETATION = register("glowshroom_garden_vegetation", Feature.RANDOM_SELECTOR, (c) ->
		{
			var featureReg = c.lookup(Registries.CONFIGURED_FEATURE);
			var purple = PlacementUtils.inlinePlaced(featureReg.getOrThrow(HUGE_PURPLE_GLOWSHROOM.getKey()));
			var green = PlacementUtils.inlinePlaced(featureReg.getOrThrow(HUGE_GREEN_GLOWSHROOM.getKey()));
			return () -> new RandomFeatureConfiguration(List.of(new WeightedPlacedFeature(purple, 0.5F)), green);
		});

		private static final Supplier<TagMatchTest> STONE_RULE_TEST = () -> new TagMatchTest(BlockTags.BASE_STONE_NETHER);

		public static final Pointer<ConfiguredFeature<?, ?>> FOULITE_ORE = register("foulite_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(STONE_RULE_TEST.get(), NCBlocks.foulite_ore.defaultBlockState())), 14));
		public static final Pointer<ConfiguredFeature<?, ?>> NERIDIUM_ORE = register("neridium_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(STONE_RULE_TEST.get(), NCBlocks.neridium_ore.defaultBlockState())), 8));
		public static final Pointer<ConfiguredFeature<?, ?>> PYRIDIUM_ORE = register("pyridium_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(STONE_RULE_TEST.get(), NCBlocks.pyridium_ore.defaultBlockState())), 5));
		public static final Pointer<ConfiguredFeature<?, ?>> LINIUM_ORE = register("linium_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(STONE_RULE_TEST.get(), NCBlocks.linium_ore.defaultBlockState())), 3));
		public static final Pointer<ConfiguredFeature<?, ?>> W_ORE = register("w_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(STONE_RULE_TEST.get(), NCBlocks.w_ore.defaultBlockState())), 4));

		public static final Pointer<ConfiguredFeature<?, ?>> GLOWROOTS = register("glowroots", Feature.RANDOM_PATCH, () -> Configs.GLOWROOTS_CONFIG);
		public static final Pointer<ConfiguredFeature<?, ?>> LAVA_REEDS = register("lava_reeds", Feature.RANDOM_PATCH, () -> Configs.LAVA_REEDS_PATCH_CONFIG);

		// default
		public static final Pointer<ConfiguredFeature<?, ?>> GLOWSHROOM_PATCH_64 = register("glowshroom_patch_64", Feature.RANDOM_PATCH, () -> Configs.GLOWSHROOMS_64);

		// common
		public static final Pointer<ConfiguredFeature<?, ?>> GLOWSHROOM_PATCH_10 = register("glowshroom_patch_10", Feature.RANDOM_PATCH, () -> Configs.GLOWSHROOMS_10);

		// garden
		public static final Pointer<ConfiguredFeature<?, ?>> GLOWSHROOM_PATCH_30 = register("glowshroom_patch_30", Feature.RANDOM_PATCH, () -> Configs.GLOWSHROOMS_30);

		public static void bootstrap(BootstrapContext<ConfiguredFeature<?, ?>> bootstrap)
		{

			POINTERS.forEach((key, pointer) ->
			{
				System.out.println("Registering " + pointer.getKey().toString());
				bootstrap.register(pointer.getKey(), pointer.getInstance().apply(bootstrap));
			});
		}

		public static void init()
		{
		}

		private static <FC extends FeatureConfiguration, F extends Feature<FC>> Pointer<ConfiguredFeature<?, ?>> register(String key, F feature, Supplier<FC> config)
		{
			return register(key, feature, (c) -> config);
		}

		private static <FC extends FeatureConfiguration, F extends Feature<FC>> Pointer<ConfiguredFeature<?, ?>> register(String key, F feature, Function<BootstrapContext<?>, Supplier<FC>> config)
		{
			Pointer<ConfiguredFeature<?, ?>> pointer = new Pointer<>(Registries.CONFIGURED_FEATURE, key, (c) -> new ConfiguredFeature<>(feature, config.apply(c).get()));

			if (DatagenModLoader.isRunningDataGen())
				POINTERS.put(Nethercraft.locate(key), pointer);

			return pointer;
		}

		public static Pointer<ConfiguredFeature<?, ?>> createBasicTree(String key, BlockState log, BlockState leaves, int height)
		{
			return createBasicTree(key, log, leaves, height, 2);
		}

		public static Pointer<ConfiguredFeature<?, ?>> createBasicTree(String key, BlockState log, BlockState leaves, int height, int randHeight)
		{
			return register(key, Feature.TREE, () -> (new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(log), new StraightTrunkPlacer(height, randHeight, 0), BlockStateProvider.simple(leaves), new BlobFoliagePlacer(ConstantInt.of(2), ConstantInt.of(0), 3), new TwoLayersFeatureSize(1, 0, 1))).ignoreVines().build());
		}

		protected static class Configs
		{
			// formerly "common"
			public static final RandomPatchConfiguration GLOWSHROOMS_10 = weightedPatch("glowshrooms_10", weighted().add(NCBlocks.purple_glowshroom.defaultBlockState(), 1).add(NCBlocks.green_glowshroom.defaultBlockState(), 1), 64);
			// formerly "garden"
			public static final RandomPatchConfiguration GLOWSHROOMS_30 = weightedPatch("glowshrooms_30", weighted().add(NCBlocks.purple_glowshroom.defaultBlockState(), 1).add(NCBlocks.green_glowshroom.defaultBlockState(), 1), 64);
			// formerly "small"
			public static final RandomPatchConfiguration GLOWSHROOMS_64 = weightedPatch("glowshrooms_64", weighted().add(NCBlocks.purple_glowshroom.defaultBlockState(), 1).add(NCBlocks.green_glowshroom.defaultBlockState(), 1), 64);

			public static final RandomPatchConfiguration GLOWROOTS_CONFIG = simplePatch("glowroots_30", NCBlocks.glowroots.defaultBlockState(), 30);
			public static final RandomPatchConfiguration LAVA_REEDS_PATCH_CONFIG = new RandomPatchConfiguration(20, 4, 0, PlacementUtils.inlinePlaced(Feature.BLOCK_COLUMN, BlockColumnConfiguration.simple(BiasedToBottomInt.of(2, 4), BlockStateProvider.simple(NCBlocks.lava_reeds)), BlockPredicateFilter.forPredicate(BlockPredicate.allOf(BlockPredicate.ONLY_IN_AIR_PREDICATE, BlockPredicate.wouldSurvive(NCBlocks.lava_reeds.defaultBlockState(), BlockPos.ZERO), BlockPredicate.anyOf(BlockPredicate.matchesFluids(new BlockPos(1, -1, 0), Fluids.LAVA, Fluids.FLOWING_LAVA), BlockPredicate.matchesFluids(new BlockPos(-1, -1, 0), Fluids.LAVA, Fluids.FLOWING_LAVA), BlockPredicate.matchesFluids(new BlockPos(0, -1, 1), Fluids.LAVA, Fluids.FLOWING_LAVA), BlockPredicate.matchesFluids(new BlockPos(0, -1, -1), Fluids.LAVA, Fluids.FLOWING_LAVA))))));

			public static RandomPatchConfiguration simplePatch(String name, BlockState state, int tries)
			{
				return simplePatch(name, state, tries, p -> PlacementUtils.onlyWhenEmpty(p.getLeft(), p.getRight()));
			}

			public static RandomPatchConfiguration filteredPatch(String name, BlockState state, int tries, Block notBlock)
			{
				return simplePatch(name, state, tries, p -> PlacementUtils.filtered(p.getLeft(), p.getRight(), BlockPredicate.allOf(BlockPredicate.matchesBlocks(Blocks.AIR), BlockPredicate.not(BlockPredicate.matchesBlocks(BlockPos.ZERO.below(), notBlock)))));
			}

			@SuppressWarnings("unchecked")
			public static <FC extends FeatureConfiguration, F extends Feature<FC>> RandomPatchConfiguration simplePatch(String name, BlockState state, int tries, Function<Pair<F, FC>, Holder<PlacedFeature>> cons)
			{
				return randomPatchConfig(name, tries, cons.apply(Pair.of((F) Feature.SIMPLE_BLOCK, (FC) new SimpleBlockConfiguration(BlockStateProvider.simple(state)))));
			}

			public static RandomPatchConfiguration weightedPatch(String name, SimpleWeightedRandomList.Builder<BlockState> builder, int tries)
			{
				return weightedPatch(name, builder, tries, p -> PlacementUtils.onlyWhenEmpty(p.getLeft(), p.getRight()));
			}

			@SuppressWarnings("unchecked")
			public static <FC extends FeatureConfiguration, F extends Feature<FC>> RandomPatchConfiguration weightedPatch(String name, SimpleWeightedRandomList.Builder<BlockState> builder, int tries, Function<Pair<F, FC>, Holder<PlacedFeature>> cons)
			{
				return randomPatchConfig(name, tries, cons.apply(Pair.of((F) Feature.SIMPLE_BLOCK, (FC) new SimpleBlockConfiguration(new WeightedStateProvider(builder)))));
			}

			public static <FC extends FeatureConfiguration, F extends Feature<FC>> RandomPatchConfiguration randomPatchConfig(String name, int tries, Holder<PlacedFeature> placedHolder)
			{
				return FeatureUtils.simpleRandomPatchConfiguration(tries, placedHolder);
			}

			public static SimpleWeightedRandomList.Builder<BlockState> weighted()
			{
				return SimpleWeightedRandomList.<BlockState>builder();
			}
		}
	}

	@SuppressWarnings("deprecation")
	public static class Placements
	{

		/*public static final RegistrarHandler<PlacedFeature> HANDLER = RegistrarHandler.getOrCreate(Registries.PLACED_FEATURE, Nethercraft.MODID);*/
		private static final Map<ResourceLocation, Pointer<PlacedFeature>> POINTERS = new HashMap<>();

		/*FIXME: Compare counts to GNS's port to make sure they're ported correctly here*/
		public static final Pointer<PlacedFeature> FOULITE_ORE = register("foulite_ore", Configured.FOULITE_ORE, countRange(127, 20));
		public static final Pointer<PlacedFeature> NERIDIUM_ORE = register("neridium_ore", Configured.NERIDIUM_ORE, countRange(127, 8));
		public static final Pointer<PlacedFeature> PYRIDIUM_ORE = register("pyridium_ore", Configured.PYRIDIUM_ORE, countRange(127, 4));
		public static final Pointer<PlacedFeature> LINIUM_ORE = register("linium_ore", Configured.LINIUM_ORE, countRange(127, 5));
		public static final Pointer<PlacedFeature> W_ORE = register("w_ore", Configured.W_ORE, countRange(127, 2));

		public static final Pointer<PlacedFeature> GLOWING_GROVE_VEGETATION = register("glowing_grove_trees", Configured.GLOWOOD_TREE, List.of(CountOnEveryLayerPlacement.of(4), BiomeFilter.biome()));
		public static final Pointer<PlacedFeature> GLOWING_GROVE_FOLIAGE = register("glowing_grove_foliage", Configured.GLOWROOTS, List.of(CountOnEveryLayerPlacement.of(2), BiomeFilter.biome()));

		public static final Pointer<PlacedFeature> GLOWSHROOM_GARDEN_VEGETATION = register("glowshroom_garden_vegetation", Configured.GLOWSHROOM_GARDEN_VEGETATION, List.of(CountOnEveryLayerPlacement.of(3), BiomeFilter.biome()));

		public static final Pointer<PlacedFeature> VOLCANIC_RUSHES_GEYSERS = register("volcanic_rushes_geysers", Configured.VOLCANIC_GEYSER, List.of(CountOnEveryLayerPlacement.of(1), BiomeFilter.biome()));

		public static final Pointer<PlacedFeature> GLOWSHROOM_10 = register("glowshroom_patch_10", Configured.GLOWSHROOM_PATCH_10, List.of(CountOnEveryLayerPlacement.of(1), BiomeFilter.biome()));
		public static final Pointer<PlacedFeature> GLOWSHROOM_30 = register("glowshroom_patch_30", Configured.GLOWSHROOM_PATCH_30, List.of(CountOnEveryLayerPlacement.of(1), BiomeFilter.biome()));
		public static final Pointer<PlacedFeature> GLOWSHROOM_64 = register("glowshroom_patch_64", Configured.GLOWSHROOM_PATCH_64, List.of(CountOnEveryLayerPlacement.of(1), BiomeFilter.biome()));

		public static final Pointer<PlacedFeature> LAVA_REEDS = register("lava_reeds", Configured.LAVA_REEDS, List.of(CountOnEveryLayerPlacement.of(10), BiomeFilter.biome()));

		public static void bootstrap(BootstrapContext<PlacedFeature> bootstrap)
		{
			POINTERS.forEach((key, pointer) -> bootstrap.register(pointer.getKey(), pointer.getInstance().apply(bootstrap)));
		}

		public static void init()
		{
		}

		private static Pointer<PlacedFeature> register(String key, Pointer<ConfiguredFeature<?, ?>> feature, List<PlacementModifier> mods)
		{
			/*return HANDLER.createPointer(key, (b) -> new PlacedFeature(feature.getHolder(b).get(), List.copyOf(mods)));*/

			Pointer<PlacedFeature> pointer = new Pointer<>(Registries.PLACED_FEATURE, key, (c) -> new PlacedFeature(feature.getHolder(c).get(), List.copyOf(mods)));

			if (DatagenModLoader.isRunningDataGen())
				POINTERS.put(Nethercraft.locate(key), pointer);

			return pointer;
		}

		private static List<PlacementModifier> countRange(int height, int count)
		{
			return List.of(CountPlacement.of(count), InSquarePlacement.spread(), HeightRangePlacement.uniform(VerticalAnchor.absolute(0), VerticalAnchor.absolute(height)), BiomeFilter.biome());
		}

		private static List<PlacementModifier> countRange(HeightRangePlacement heightRange, int count)
		{
			return List.of(CountPlacement.of(count), InSquarePlacement.spread(), heightRange, BiomeFilter.biome());
		}

		/*private static PlacementModifier range(int height)
		{
			// still from 0
			return HeightRangePlacement.uniform(VerticalAnchor.absolute(0), VerticalAnchor.absolute(height));
		}
		
		private static List<PlacementModifier> count(int count)
		{
			return flower(count, false);
		}*/

		private static List<PlacementModifier> flower(int count, boolean average)
		{
			PlacementModifier modifier = average ? RarityFilter.onAverageOnceEvery(count) : CountPlacement.of(count);
			return List.of(modifier, InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome());
		}

		private static List<PlacementModifier> countNoise(double noise, int x, int y)
		{
			return countNoise(noise, x, y, PlacementUtils.HEIGHTMAP);
		}

		private static List<PlacementModifier> countNoise(double noise, int x, int y, PlacementModifier heightmap)
		{
			return List.of(NoiseThresholdCountPlacement.of(noise, x, y), InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome());
		}
	}
}
