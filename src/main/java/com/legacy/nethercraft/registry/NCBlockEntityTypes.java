package com.legacy.nethercraft.registry;

import java.util.Set;
import java.util.function.Supplier;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.tile_entity.NetherChestBlockEntity;
import com.legacy.nethercraft.tile_entity.NetherrackFurnaceBlockEntity;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.registries.RegisterEvent;

public class NCBlockEntityTypes
{
	public static final Supplier<BlockEntityType<NetherrackFurnaceBlockEntity>> NETHERRACK_FURNACE = Lazy.of(() -> create(NetherrackFurnaceBlockEntity::new, NCBlocks.netherrack_furnace));
	public static final Supplier<BlockEntityType<? extends ChestBlockEntity>> NETHER_CHEST = Lazy.of(() -> create(NetherChestBlockEntity::new,  NCBlocks.glowood_chest));

	public static void init(RegisterEvent event)
	{
		register(event, "netherrack_furnace", NETHERRACK_FURNACE);
		register(event, "glowood_chest", NETHER_CHEST);
	}

	@SuppressWarnings("unchecked")
	private static <T extends BlockEntity> void register(RegisterEvent event, String name, Object blockEntity)
	{
		event.register(Registries.BLOCK_ENTITY_TYPE, Nethercraft.locate(name), (Supplier<BlockEntityType<?>>) blockEntity);
	}

	private static <T extends BlockEntity> BlockEntityType<T> create(BlockEntityType.BlockEntitySupplier<? extends T> blockEntity, Block... validBlocks)
	{
		return new BlockEntityType<>(blockEntity, Set.of(validBlocks));
	}
}