package com.legacy.nethercraft.registry;

import com.legacy.nethercraft.Nethercraft;

import net.minecraft.world.level.block.state.properties.BlockSetType;

public class NCBlockSets
{
	public static final BlockSetType GLOWOOD = register("glowood");

	public static BlockSetType register(String key)
	{
		return register(new BlockSetType(Nethercraft.find(key)));
	}

	public static BlockSetType register(BlockSetType type)
	{
		return BlockSetType.register(type);
	}

	public static void init()
	{
	}
}
