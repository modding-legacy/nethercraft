package com.legacy.nethercraft.registry;

import com.legacy.nethercraft.Nethercraft;

import net.minecraft.core.Holder;
import net.minecraft.core.Holder.Reference;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;

public class NCSounds
{
	public static final Reference<SoundEvent> AMBIENT_GLOWING_GROVE_LOOP = createForHolder("ambient.glowing_grove.loop");
	public static final Reference<SoundEvent> AMBIENT_GLOWSHROOM_GARDEN_LOOP = createForHolder("ambient.glowshroom_garden.loop");
	public static final Reference<SoundEvent> AMBIENT_VOLCANIC_RUSHES_LOOP = createForHolder("ambient.volcanic_rushes.loop");

	public static final Reference<SoundEvent> MUSIC_NETHER_CLASSIC = createForHolder("music.nether.classic");

	public static final Reference<SoundEvent> RECORD_IMP = createForHolder("record.imp");

	public static void init()
	{
	}

	private static SoundEvent create(String name)
	{
		ResourceLocation location = Nethercraft.locate(name);
		return Registry.register(BuiltInRegistries.SOUND_EVENT, location, SoundEvent.createVariableRangeEvent(location));
	}

	private static Holder.Reference<SoundEvent> createForHolder(String name)
	{
		ResourceLocation location = Nethercraft.locate(name);
		return Registry.registerForHolder(BuiltInRegistries.SOUND_EVENT, location, SoundEvent.createVariableRangeEvent(location));
	}
}