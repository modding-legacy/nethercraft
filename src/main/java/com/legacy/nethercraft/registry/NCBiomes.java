package com.legacy.nethercraft.registry;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.Pointer;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.data.worldgen.Carvers;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.Musics;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.level.biome.AmbientAdditionsSettings;
import net.minecraft.world.level.biome.AmbientMoodSettings;
import net.minecraft.world.level.biome.AmbientParticleSettings;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeGenerationSettings;
import net.minecraft.world.level.biome.BiomeSpecialEffects;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.SurfaceRules;
import net.minecraft.world.level.levelgen.carver.ConfiguredWorldCarver;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.data.loading.DatagenModLoader;
import net.neoforged.neoforge.registries.RegisterEvent;

public class NCBiomes
{
	private static final Map<ResourceLocation, Pointer<Biome>> POINTERS = new HashMap<>();

	public static final Pointer<Biome> GLOWING_GROVE = create("glowing_grove", (c) -> Builders.createGlowingGroveBiome(c));
	public static final Pointer<Biome> GLOWSHROOM_GARDEN = create("glowshroom_garden", (c) -> Builders.createGlowshroomGardenBiome(c));
	public static final Pointer<Biome> VOLCANIC_RUSHES = create("volcanic_rushes", (c) -> Builders.createVolcanicRushesBiome(c));


	public static void bootstrap(BootstrapContext<Biome> bootstrap)
	{
		POINTERS.forEach((key, pointer) -> bootstrap.register(pointer.getKey(), pointer.getInstance().apply(bootstrap)));
	}

	public static void init(RegisterEvent event)
	{
	}

	private static Pointer<Biome> create(String name, Function<BootstrapContext<?>, Biome> biome)
	{
		Pointer<Biome> pointer = new Pointer<>(Registries.BIOME, name, biome);

		if (DatagenModLoader.isRunningDataGen())
			POINTERS.put(Nethercraft.locate(name), pointer);

		return pointer;
	}

	public static class Builders
	{
		// BiomeMaker
		public static Biome createGlowingGroveBiome(BootstrapContext<?> bootstrap)
		{
			MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

			spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.GHAST, 50, 4, 4));
			spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.ZOMBIFIED_PIGLIN, 100, 4, 4));

			spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NCEntityTypes.DARK_ZOMBIE, 60, 4, 4));
			spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NCEntityTypes.BLOODY_ZOMBIE, 40, 2, 2));
			spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NCEntityTypes.CAMOUFLAGE_SPIDER, 50, 1, 3));

			spawns.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(EntityType.STRIDER, 60, 1, 2));
			spawns.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(NCEntityTypes.IMP, 50, 2, 3));

			// NetherFeatureConfig.HEAT_SAND_NETHER, NetherFeatureConfig.NETHER_DIRT_CONFIG
			RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));
			builder.addCarver(Carvers.NETHER_CAVE);

			NCFeatures.addGroveFeatures(builder);
			NCFeatures.addDefaultNetherFeatures(builder); // Adding things like quartz and the cave carver
			// DefaultBiomeFeatures.withNormalMushroomGeneration(builder);

			return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(2.0F).downfall(0.0F).specialEffects((new BiomeSpecialEffects.Builder()).waterColor(4159204).waterFogColor(329011).fogColor(0x422736).skyColor(defaultSkyColor(2.0F)).ambientLoopSound(NCSounds.AMBIENT_GLOWING_GROVE_LOOP).ambientMoodSound(new AmbientMoodSettings(SoundEvents.AMBIENT_NETHER_WASTES_MOOD, 6000, 8, 2.0D)).ambientAdditionsSound(new AmbientAdditionsSettings(SoundEvents.AMBIENT_NETHER_WASTES_ADDITIONS, 0.0111D)).backgroundMusic(Musics.createGameMusic(NCSounds.MUSIC_NETHER_CLASSIC)).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
		}

		public static Biome createGlowshroomGardenBiome(BootstrapContext<?> bootstrap)
		{
			MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

			spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NCEntityTypes.DARK_ZOMBIE, 60, 4, 4));
			spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NCEntityTypes.BLOODY_ZOMBIE, 40, 2, 2));
			spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NCEntityTypes.CAMOUFLAGE_SPIDER, 50, 1, 3));

			spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.ZOMBIFIED_PIGLIN, 100, 4, 4));
			spawns.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(EntityType.STRIDER, 60, 1, 2));
			spawns.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(NCEntityTypes.IMP, 50, 2, 3));

			RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));
			builder.addCarver(Carvers.NETHER_CAVE);

			NCFeatures.addGardenFeatures(builder);
			NCFeatures.addDefaultNetherFeatures(builder);
			// DefaultBiomeFeatures.withNormalMushroomGeneration(builder);

			return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(2.0F).downfall(0.0F).specialEffects((new BiomeSpecialEffects.Builder()).waterColor(4159204).waterFogColor(329011).fogColor(0x42273b).skyColor(defaultSkyColor(2.0F)).ambientParticle(new AmbientParticleSettings(NCParticles.GLOWSHROOM_SPORE, 0.00725F)).ambientLoopSound(NCSounds.AMBIENT_GLOWSHROOM_GARDEN_LOOP).ambientMoodSound(new AmbientMoodSettings(SoundEvents.AMBIENT_NETHER_WASTES_MOOD, 6000, 8, 2.0D)).ambientAdditionsSound(new AmbientAdditionsSettings(SoundEvents.AMBIENT_NETHER_WASTES_ADDITIONS, 0.0111D)).backgroundMusic(Musics.createGameMusic(NCSounds.MUSIC_NETHER_CLASSIC)).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
		}

		public static Biome createVolcanicRushesBiome(BootstrapContext<?> bootstrap)
		{
			MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

			spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.GHAST, 50, 4, 4));
			spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(NCEntityTypes.LAVA_SLIME, 70, 4, 4));
			spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.ZOMBIFIED_PIGLIN, 100, 4, 4));
			spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.MAGMA_CUBE, 20, 4, 4));

			spawns.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(EntityType.STRIDER, 60, 1, 2));

			RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));
			builder.addCarver(Carvers.NETHER_CAVE);

			NCFeatures.addRushesFeatures(builder);
			NCFeatures.addDefaultNetherFeatures(builder);

			return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(2.5F).downfall(0.0F).specialEffects((new BiomeSpecialEffects.Builder()).waterColor(4159204).waterFogColor(329011).fogColor(0xa24412).skyColor(defaultSkyColor(2.5F)).ambientParticle(new AmbientParticleSettings(ParticleTypes.SMOKE, 0.00825F)).ambientLoopSound(NCSounds.AMBIENT_VOLCANIC_RUSHES_LOOP).ambientMoodSound(new AmbientMoodSettings(SoundEvents.AMBIENT_BASALT_DELTAS_MOOD, 6000, 8, 2.0D)).ambientAdditionsSound(new AmbientAdditionsSettings(SoundEvents.AMBIENT_BASALT_DELTAS_ADDITIONS, 0.0111D)).backgroundMusic(Musics.createGameMusic(NCSounds.MUSIC_NETHER_CLASSIC)).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
		}

		private static int defaultSkyColor(float p_244206_0_)
		{
			float lvt_1_1_ = p_244206_0_ / 3.0F;
			lvt_1_1_ = Mth.clamp(lvt_1_1_, -1.0F, 1.0F);
			return Mth.hsvToRgb(0.62222224F - lvt_1_1_ * 0.05F, 0.5F + lvt_1_1_ * 0.1F, 1.0F);
		}

		public static class RegistrarBuilder extends BiomeGenerationSettings.Builder
		{
			public RegistrarBuilder(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter)
			{
				super(featureGetter, carverGetter);
			}

			public BiomeGenerationSettings.Builder addFeature(GenerationStep.Decoration step, Pointer<PlacedFeature> registrar)
			{
				return super.addFeature(step, registrar.getKey());
			}
		}
	}
	
	public static class RuleSources
	{
	    public static final Lazy<SurfaceRules.RuleSource> NETHER_DIRT = Lazy.of(() -> SurfaceRules.state(NCBlocks.nether_dirt.defaultBlockState()));
	    public static final Lazy<SurfaceRules.RuleSource> HEAT_SAND = Lazy.of(() -> SurfaceRules.state(NCBlocks.heat_sand.defaultBlockState()));
	}

}
