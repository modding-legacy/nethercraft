package com.legacy.nethercraft;

import com.legacy.nethercraft.registry.NCBlockEntityTypes;
import com.legacy.nethercraft.registry.NCBlocks;
import com.legacy.nethercraft.registry.NCEntityTypes;
import com.legacy.nethercraft.registry.NCFeatures;
import com.legacy.nethercraft.registry.NCItems;
import com.legacy.nethercraft.registry.NCBiomes;
import com.legacy.nethercraft.registry.NCParticles;
import com.legacy.nethercraft.registry.NCSounds;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.common.EventBusSubscriber.Bus;
import net.neoforged.neoforge.registries.RegisterEvent;

@EventBusSubscriber(modid = Nethercraft.MODID, bus = Bus.MOD)
public class NCRegistry
{
	@SubscribeEvent
	public static void onRegistry(RegisterEvent event)
	{
		ResourceKey<?> registry = event.getRegistryKey();

		if (registry.equals(Registries.ENTITY_TYPE))
			NCEntityTypes.init(event);
		else if (registry.equals(Registries.ITEM))
			NCItems.init(event);
		else if (registry.equals(Registries.BLOCK))
			NCBlocks.init(event);
		else if (registry.equals(Registries.SOUND_EVENT))
			NCSounds.init();
		else if (registry.equals(Registries.BLOCK_ENTITY_TYPE))
			NCBlockEntityTypes.init(event);
		else if (registry.equals(Registries.BIOME))
			NCBiomes.init(event);
		else if (registry.equals(Registries.FEATURE))
			NCFeatures.init(event);
		else if (registry.equals(Registries.PARTICLE_TYPE))
			NCParticles.init(event);
		/*else if (event.getRegistryKey().equals(NeoForgeRegistries.Keys.ATTACHMENT_TYPES))
			event.register(NeoForgeRegistries.Keys.ATTACHMENT_TYPES, Nethercraft.locate("player_attachment"), (Supplier) PLAYER_ATTACHMENT);*/
	}
}