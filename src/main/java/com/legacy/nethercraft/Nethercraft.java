package com.legacy.nethercraft;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.legacy.nethercraft.client.NCEntityRendering;
import com.legacy.nethercraft.client.NethercraftClient;
import com.legacy.nethercraft.client.resource_pack.NCResourcePackHandler;
import com.legacy.nethercraft.event.NCEvents;
import com.legacy.nethercraft.event.NCPlayerEvents;
import com.legacy.nethercraft.item.util.FuelHandler;
import com.legacy.nethercraft.registry.NCEntityTypes;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.EventPriority;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModContainer;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.common.NeoForge;

@Mod(Nethercraft.MODID)
public class Nethercraft
{
	public static final String MODID = "nethercraft";
	public static final Logger LOGGER = LogManager.getLogger("ModdingLegacy/" + Nethercraft.MODID);

	public static ResourceLocation locate(String name)
	{
		return ResourceLocation.fromNamespaceAndPath(MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}

	public Nethercraft(IEventBus modBus, ModContainer modContainer)
	{
		IEventBus forge = NeoForge.EVENT_BUS;

		if (FMLEnvironment.dist == Dist.CLIENT)
		{
			NCEntityRendering.init(modBus);
			modBus.addListener(NethercraftClient::init);
			modBus.addListener(NCResourcePackHandler::packRegistry);
		}

		modBus.addListener(Nethercraft::commonInit);
		modBus.addListener(NCEntityTypes::onAttributesRegistered);
		modBus.addListener(EventPriority.LOWEST, NCEntityTypes::registerPlacements);
		modBus.addListener(NCEntityTypes::registerPlacementOverrides);
		modBus.register(NCEvents.ModEvents.class);

		//forge.register(NetherEvents.ModEvents.class);
		forge.register(FuelHandler.class);
		forge.register(NCPlayerEvents.class);
	}

	private static void commonInit(final FMLCommonSetupEvent event)
	{
	}
}
