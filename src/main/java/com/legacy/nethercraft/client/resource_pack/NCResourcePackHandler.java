package com.legacy.nethercraft.client.resource_pack;

import java.util.Optional;

import com.legacy.nethercraft.Nethercraft;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.server.packs.PackLocationInfo;
import net.minecraft.server.packs.PackSelectionConfig;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.PathPackResources;
import net.minecraft.server.packs.repository.BuiltInPackSource;
import net.minecraft.server.packs.repository.Pack;
import net.minecraft.server.packs.repository.PackSource;
import net.neoforged.fml.ModList;
import net.neoforged.neoforge.event.AddPackFindersEvent;
import net.neoforged.neoforgespi.locating.IModFile;

public class NCResourcePackHandler
{
	public static final Pack LEGACY_PACK = createPack(Component.literal("Nethercraft Programmer Art"), "legacy_pack").hidden();

	public static void packRegistry(AddPackFindersEvent event)
	{
		if (event.getPackType() == PackType.CLIENT_RESOURCES)
		{
		}
	}

	@SuppressWarnings("unused")
	private static void register(AddPackFindersEvent event, MutableComponent name, String folder)
	{
		event.addRepositorySource((consumer) -> consumer.accept(createPack(name, folder)));
	}

	public static Pack createPack(MutableComponent name, String folder)
	{
		IModFile file = ModList.get().getModFileById(modid()).getFile();

		PackLocationInfo loc = new PackLocationInfo(modid() + ":" + folder, name, PackSource.BUILT_IN, Optional.empty());
		PathPackResources packResources = new PathPackResources(loc, file.findResource("assets/" + modid() + "/" + folder));
		return Pack.readMetaAndCreate(loc, BuiltInPackSource.fixedResources(packResources), PackType.CLIENT_RESOURCES, new PackSelectionConfig(false, Pack.Position.TOP, false));
	}

	private static String modid()
	{
		return Nethercraft.MODID;
	}
}