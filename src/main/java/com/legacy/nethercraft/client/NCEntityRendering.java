package com.legacy.nethercraft.client;

import com.legacy.nethercraft.client.render.NCRenderRefs;
import com.legacy.nethercraft.client.render.entity.BloodyZombieRenderer;
import com.legacy.nethercraft.client.render.entity.CamouflageSpiderRenderer;
import com.legacy.nethercraft.client.render.entity.DarkZombieRenderer;
import com.legacy.nethercraft.client.render.entity.ImpRenderer;
import com.legacy.nethercraft.client.render.entity.LavaSlimeRenderer;
import com.legacy.nethercraft.client.render.entity.projectile.NCArrowRenderer;
import com.legacy.nethercraft.client.render.entity.tribal.TribalArcherRenderer;
import com.legacy.nethercraft.client.render.entity.tribal.TribalTraineeRenderer;
import com.legacy.nethercraft.client.render.entity.tribal.TribalWarriorRenderer;
import com.legacy.nethercraft.client.render.model.ImpModel;
import com.legacy.nethercraft.client.render.tile_entity.NetherChestRenderer;
import com.legacy.nethercraft.registry.NCBlockEntityTypes;
import com.legacy.nethercraft.registry.NCEntityTypes;

import net.minecraft.client.model.HumanoidArmorModel;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.LayerDefinitions;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.renderer.entity.ThrownItemRenderer;
import net.minecraft.client.renderer.entity.TntRenderer;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.neoforge.client.event.EntityRenderersEvent;

public class NCEntityRendering
{
	public static void init(IEventBus modBus)
	{
		modBus.addListener(NCEntityRendering::initLayers);
		modBus.addListener(NCEntityRendering ::initRenders);
	}

	public static void initLayers(EntityRenderersEvent.RegisterLayerDefinitions event)
	{
        LayerDefinition humanoid = LayerDefinition.create(HumanoidModel.createMesh(CubeDeformation.NONE, 0.0F), 64, 64);
		LayerDefinition outerArmor = LayerDefinition.create(HumanoidArmorModel.createBodyLayer(LayerDefinitions.OUTER_ARMOR_DEFORMATION), 64, 32);
		LayerDefinition innerArmor = LayerDefinition.create(HumanoidArmorModel.createBodyLayer(LayerDefinitions.INNER_ARMOR_DEFORMATION), 64, 32);

		event.registerLayerDefinition(NCRenderRefs.IMP, ImpModel::createBodyLayer);
		/*event.registerLayerDefinition(GNSRenderRefs.GUMMY_BEAR, () -> GummyBearModel.createBodyLayer(CubeDeformation.NONE));*/

		event.registerLayerDefinition(NCRenderRefs.DARK_ZOMBIE, () -> humanoid);
		event.registerLayerDefinition(NCRenderRefs.DARK_ZOMBIE_INNER_ARMOR, () -> innerArmor);
		event.registerLayerDefinition(NCRenderRefs.DARK_ZOMBIE_OUTER_ARMOR, () -> outerArmor);
		event.registerLayerDefinition(NCRenderRefs.DARK_ZOMBIE_BABY, () -> humanoid.apply(HumanoidModel.BABY_TRANSFORMER));
		event.registerLayerDefinition(NCRenderRefs.DARK_ZOMBIE_BABY_INNER_ARMOR, () -> innerArmor.apply(HumanoidModel.BABY_TRANSFORMER));
		event.registerLayerDefinition(NCRenderRefs.DARK_ZOMBIE_BABY_OUTER_ARMOR, () -> outerArmor.apply(HumanoidModel.BABY_TRANSFORMER));
		
		event.registerLayerDefinition(NCRenderRefs.TRIBAL_WARRIOR, () -> humanoid);
		event.registerLayerDefinition(NCRenderRefs.TRIBAL_WARRIOR_INNER_ARMOR, () -> innerArmor);
		event.registerLayerDefinition(NCRenderRefs.TRIBAL_WARRIOR_OUTER_ARMOR, () -> outerArmor);
		
		event.registerLayerDefinition(NCRenderRefs.TRIBAL_ARCHER, () -> humanoid);
		event.registerLayerDefinition(NCRenderRefs.TRIBAL_ARCHER_INNER_ARMOR, () -> innerArmor);
		event.registerLayerDefinition(NCRenderRefs.TRIBAL_ARCHER_OUTER_ARMOR, () -> outerArmor);
		
		event.registerLayerDefinition(NCRenderRefs.TRIBAL_TRAINEE, () -> humanoid.apply(HumanoidModel.BABY_TRANSFORMER));
		event.registerLayerDefinition(NCRenderRefs.TRIBAL_TRAINEE_INNER_ARMOR, () -> innerArmor.apply(HumanoidModel.BABY_TRANSFORMER));
		event.registerLayerDefinition(NCRenderRefs.TRIBAL_TRAINEE_OUTER_ARMOR, () -> outerArmor.apply(HumanoidModel.BABY_TRANSFORMER));
	}

	public static void initRenders(EntityRenderersEvent.RegisterRenderers event)
	{
		event.registerEntityRenderer(NCEntityTypes.DARK_ZOMBIE, DarkZombieRenderer::new);
		event.registerEntityRenderer(NCEntityTypes.BLOODY_ZOMBIE, BloodyZombieRenderer::new);
		event.registerEntityRenderer(NCEntityTypes.CAMOUFLAGE_SPIDER, CamouflageSpiderRenderer::new);
		event.registerEntityRenderer(NCEntityTypes.IMP, ImpRenderer::new);
		event.registerEntityRenderer(NCEntityTypes.LAVA_SLIME, LavaSlimeRenderer::new);

		event.registerEntityRenderer(NCEntityTypes.TRIBAL_WARRIOR, TribalWarriorRenderer::new);
		event.registerEntityRenderer(NCEntityTypes.TRIBAL_ARCHER, TribalArcherRenderer::new);
		event.registerEntityRenderer(NCEntityTypes.TRIBAL_TRAINEE, TribalTraineeRenderer::new);

		event.registerEntityRenderer(NCEntityTypes.NETHERRACK_ARROW, NCArrowRenderer.Netherrack::new);
		event.registerEntityRenderer(NCEntityTypes.NERIDIUM_ARROW, NCArrowRenderer.Neridium::new);
		event.registerEntityRenderer(NCEntityTypes.PYRIDIUM_ARROW, NCArrowRenderer.Pyridium::new);
		event.registerEntityRenderer(NCEntityTypes.LINIUM_ARROW, NCArrowRenderer.Linium::new);

		event.registerEntityRenderer(NCEntityTypes.SLIME_EGGS, ThrownItemRenderer::new);
		event.registerEntityRenderer(NCEntityTypes.GHAST_BOMB, TntRenderer::new);

		event.registerBlockEntityRenderer(NCBlockEntityTypes.NETHER_CHEST.get(), NetherChestRenderer::new);

		/*event.registerBlockEntityRenderer(NCBlockEntityTypes.SIGN.get(), SignRenderer::new);
		event.registerBlockEntityRenderer(NCBlockEntityTypes.HANGING_SIGN.get(), HangingSignRenderer::new);*/
	}
}