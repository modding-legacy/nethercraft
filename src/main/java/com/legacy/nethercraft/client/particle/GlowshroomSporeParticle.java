package com.legacy.nethercraft.client.particle;

import net.minecraft.client.particle.SpriteSet;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.TextureSheetParticle;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.core.particles.SimpleParticleType;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class GlowshroomSporeParticle extends TextureSheetParticle
{
	private final SpriteSet spriteSet;

	private GlowshroomSporeParticle(ClientLevel worldIn, double x, double y, double z, double dx, double dy, double dz, SpriteSet spriteSet)
	{
		super(worldIn, x, y, z, dx, dy, dz);
		this.spriteSet = spriteSet;
		this.xd = this.xd * (double) 0.03F + dx;
		this.yd = this.yd * (double) 0.01F + dy + 0.03F;
		this.zd = this.zd * (double) 0.03F + dz;
		this.x += (double) ((this.random.nextFloat() - this.random.nextFloat()) * 0.05F);
		this.y += (double) ((this.random.nextFloat() - this.random.nextFloat()) * 0.05F);
		this.z += (double) ((this.random.nextFloat() - this.random.nextFloat()) * 0.05F);
		this.lifetime = (int) (8.0D / (Math.random() * 0.3D + 0.2D)) + 14;
		this.setSpriteFromAge(spriteSet);
	}

	@Override
	public ParticleRenderType getRenderType()
	{
		return ParticleRenderType.PARTICLE_SHEET_OPAQUE;
	}

	@Override
	public void move(double x, double y, double z)
	{
		this.setBoundingBox(this.getBoundingBox().move(x, y, z));
		this.setLocationFromBoundingbox();
	}

	@Override
	public float getQuadSize(float partialTicks)
	{
		return 0.12F;
	}

	@Override
	public int getLightColor(float partialTick)
	{
		return super.getLightColor(partialTick);
	}

	@Override
	public void tick()
	{
		this.xo = this.x;
		this.yo = this.y;
		this.zo = this.z;

		if (this.age++ >= this.lifetime)
		{
			this.remove();
		}
		else
		{
			this.setSpriteFromAge(spriteSet);
			this.move(this.xd, this.yd, this.zd);

			this.xd *= (double) 0.96F;
			this.yd *= (double) 0.96F;
			this.zd *= (double) 0.96F;

			if (this.onGround)
			{
				this.xd *= (double) 0.7F;
				this.zd *= (double) 0.7F;
			}

			this.yd = this.yd * 1.115;
		}
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory implements ParticleProvider<SimpleParticleType>
	{
		private final SpriteSet spriteSet;

		public Factory(SpriteSet spriteSet)
		{
			this.spriteSet = spriteSet;
		}

		@Override
		public Particle createParticle(SimpleParticleType typeIn, ClientLevel worldIn, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed)
		{
			GlowshroomSporeParticle particle = new GlowshroomSporeParticle(worldIn, x, y, z, xSpeed, ySpeed, zSpeed, spriteSet);
			return particle;
		}
	}
}