package com.legacy.nethercraft.client.render.entity.tribal;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.client.render.NCRenderRefs;
import com.legacy.nethercraft.client.render.entity.state.TribalRenderState;
import com.legacy.nethercraft.entity.tribal.TribalArcherEntity;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;

public class TribalArcherRenderer<T extends TribalArcherEntity> extends AbstractTribalRenderer<T, TribalRenderState>
{
	public TribalArcherRenderer(EntityRendererProvider.Context context)
	{
		super(context, NCRenderRefs.TRIBAL_ARCHER, NCRenderRefs.TRIBAL_ARCHER_INNER_ARMOR, NCRenderRefs.TRIBAL_ARCHER_OUTER_ARMOR);
	}

	/*@Override
	protected void scale(TribalArcherEntity entity, PoseStack stack, float partialTickTime)
	{
		if (entity.isAggressive() && entity.getMainHandItem().getItem() instanceof BowItem)
		{
			this.getModel().leftArmPose = HumanoidModel.ArmPose.BOW_AND_ARROW;
			this.getModel().rightArmPose = HumanoidModel.ArmPose.BOW_AND_ARROW;
		}
		else
		{
			if (entity.getMainArm() == HumanoidArm.RIGHT)
			{
				this.getModel().rightArmPose = HumanoidModel.ArmPose.ITEM;
				this.getModel().leftArmPose = HumanoidModel.ArmPose.EMPTY;
			}
			else
			{
				this.getModel().rightArmPose = HumanoidModel.ArmPose.EMPTY;
				this.getModel().leftArmPose = HumanoidModel.ArmPose.ITEM;
			}
		}
	}*/
	
	@Override
	public void extractRenderState(T entity, TribalRenderState state, float partialTicks)
	{
		super.extractRenderState(entity, state, partialTicks);
		state.variant = entity.getWarriorType();
	}

	@Override
	public ResourceLocation getTextureLocation(TribalRenderState entity)
	{
		return Nethercraft.locate("textures/entity/tribal/tribal_archer_" + entity.variant + ".png");
	}

	@Override
	public TribalRenderState createRenderState()
	{
		return new TribalRenderState();
	}
}