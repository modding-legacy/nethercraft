package com.legacy.nethercraft.client.render.entity;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.client.render.entity.state.LavaSlimeRenderState;
import com.legacy.nethercraft.entity.LavaSlimeEntity;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.SlimeRenderer;
import net.minecraft.client.renderer.entity.state.SlimeRenderState;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.monster.Slime;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class LavaSlimeRenderer extends SlimeRenderer
{
	private static final ResourceLocation ORANGE_SLIME_TEXTURE = Nethercraft.locate("textures/entity/slime/orange_slime.png");
	private static final ResourceLocation RED_SLIME_TEXTURE = Nethercraft.locate("textures/entity/slime/red_slime.png");

	public LavaSlimeRenderer(EntityRendererProvider.Context context)
	{
		super(context);
	}

	@Override
	public LavaSlimeRenderState createRenderState()
	{
		return new LavaSlimeRenderState();
	}

	@Override
	public void extractRenderState(Slime entity, SlimeRenderState state, float partialTicks)
	{
		super.extractRenderState(entity, state, partialTicks);

		if (entity instanceof LavaSlimeEntity ls && state instanceof LavaSlimeRenderState lsState)
			lsState.variant = ls.getSlimeVariant();
	}

	@Override
	public ResourceLocation getTextureLocation(SlimeRenderState state)
	{
		return state instanceof LavaSlimeRenderState ls && ls.variant == 1 ? RED_SLIME_TEXTURE : ORANGE_SLIME_TEXTURE;
	}
}