package com.legacy.nethercraft.client.render.entity.projectile;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.entity.projectile.NCArrowEntity;

import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.state.ArrowRenderState;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public abstract class NCArrowRenderer extends ArrowRenderer<NCArrowEntity, ArrowRenderState>
{
	static final ResourceLocation NETHERRACK = tex("netherrack"), NERIDIUM = tex("neridium"),
			PYRIDIUM = tex("pyridium"), LINIUM = tex("linium");

	public NCArrowRenderer(EntityRendererProvider.Context context)
	{
		super(context);
	}

	private static final ResourceLocation tex(String name)
	{
		return Nethercraft.locate("textures/entity/projectile/NAME_arrow.png".replace("NAME", name));
	}

	@Override
	public ArrowRenderState createRenderState()
	{
		return new ArrowRenderState();
	}

	public static class Netherrack extends NCArrowRenderer
	{
		public Netherrack(EntityRendererProvider.Context context)
		{
			super(context);
		}

		@Override
		public ResourceLocation getTextureLocation(ArrowRenderState entity)
		{
			return NETHERRACK;
		}
	}

	public static class Neridium extends NCArrowRenderer
	{
		public Neridium(EntityRendererProvider.Context context)
		{
			super(context);
		}

		@Override
		public ResourceLocation getTextureLocation(ArrowRenderState entity)
		{
			return NERIDIUM;
		}
	}

	public static class Pyridium extends NCArrowRenderer
	{
		public Pyridium(EntityRendererProvider.Context context)
		{
			super(context);
		}

		@Override
		public ResourceLocation getTextureLocation(ArrowRenderState entity)
		{
			return PYRIDIUM;
		}
	}

	public static class Linium extends NCArrowRenderer
	{
		public Linium(EntityRendererProvider.Context context)
		{
			super(context);
		}

		@Override
		public ResourceLocation getTextureLocation(ArrowRenderState entity)
		{
			return LINIUM;
		}
	}
}