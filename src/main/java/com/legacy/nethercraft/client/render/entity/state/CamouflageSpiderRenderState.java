package com.legacy.nethercraft.client.render.entity.state;

import net.minecraft.client.renderer.entity.state.LivingEntityRenderState;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class CamouflageSpiderRenderState extends LivingEntityRenderState
{
	public int variant;
}
