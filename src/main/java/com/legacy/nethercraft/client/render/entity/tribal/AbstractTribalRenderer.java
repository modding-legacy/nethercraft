package com.legacy.nethercraft.client.render.entity.tribal;

import com.legacy.nethercraft.entity.tribal.AbstractTribalEntity;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.HumanoidMobRenderer;
import net.minecraft.client.renderer.entity.layers.HumanoidArmorLayer;
import net.minecraft.client.renderer.entity.state.HumanoidRenderState;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public abstract class AbstractTribalRenderer<T extends AbstractTribalEntity, S extends HumanoidRenderState> extends HumanoidMobRenderer<T, S, HumanoidModel<S>>
{
	private AbstractTribalRenderer(EntityRendererProvider.Context context, HumanoidModel<S> model, HumanoidModel<S> babyModel, HumanoidModel<S> innerArmor, HumanoidModel<S> outerArmor, HumanoidModel<S> babyInnerArmor, HumanoidModel<S> babyOuterArmor)
	{
		super(context, model, babyModel, 0.5F);
		this.addLayer(new HumanoidArmorLayer<>(this, innerArmor, outerArmor, babyInnerArmor, babyOuterArmor, context.getEquipmentRenderer()));
	}

	public AbstractTribalRenderer(EntityRendererProvider.Context context, ModelLayerLocation baseLayer, ModelLayerLocation innerArmor, ModelLayerLocation outerArmor, ModelLayerLocation babyLayer, ModelLayerLocation babyInnerArmor, ModelLayerLocation babyOuterArmor)
	{
		this(context, new HumanoidModel<>(context.bakeLayer(baseLayer)), new HumanoidModel<>(context.bakeLayer(innerArmor)), new HumanoidModel<>(context.bakeLayer(outerArmor)), new HumanoidModel<>(context.bakeLayer(babyLayer)), new HumanoidModel<>(context.bakeLayer(babyInnerArmor)), new HumanoidModel<>(context.bakeLayer(babyOuterArmor)));
	}

	public AbstractTribalRenderer(EntityRendererProvider.Context context, ModelLayerLocation baseLayer, ModelLayerLocation innerArmor, ModelLayerLocation outerArmor)
	{
		this(context, baseLayer, innerArmor, outerArmor, baseLayer, innerArmor, outerArmor);
	}
}