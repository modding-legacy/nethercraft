package com.legacy.nethercraft.client.render.entity.tribal;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.client.render.NCRenderRefs;
import com.legacy.nethercraft.client.render.entity.state.TribalRenderState;
import com.legacy.nethercraft.entity.tribal.TribalWarriorEntity;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;

public class TribalWarriorRenderer<T extends TribalWarriorEntity> extends AbstractTribalRenderer<T, TribalRenderState>
{
	public TribalWarriorRenderer(EntityRendererProvider.Context context)
	{
		super(context, NCRenderRefs.TRIBAL_WARRIOR, NCRenderRefs.TRIBAL_WARRIOR_INNER_ARMOR, NCRenderRefs.TRIBAL_WARRIOR_OUTER_ARMOR);
	}

	@Override
	public void extractRenderState(T entity, TribalRenderState state, float partialTicks)
	{
		super.extractRenderState(entity, state, partialTicks);
		state.variant = entity.getWarriorType();
	}

	// FIXME: Add "warrior" to name
	@Override
	public ResourceLocation getTextureLocation(TribalRenderState entity)
	{
		return Nethercraft.locate("textures/entity/tribal/tribal_" + entity.variant + ".png");
	}

	@Override
	public TribalRenderState createRenderState()
	{
		return new TribalRenderState();
	}
}