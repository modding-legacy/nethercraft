package com.legacy.nethercraft.client.render.entity.layer;

import com.legacy.nethercraft.Nethercraft;

import net.minecraft.client.model.SpiderModel;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.EyesLayer;
import net.minecraft.client.renderer.entity.state.LivingEntityRenderState;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class CamouflageSpiderEyesLayer<M extends SpiderModel> extends EyesLayer<LivingEntityRenderState, M>
{
	private static final RenderType SPIDER_EYES = RenderType.eyes(Nethercraft.locate("textures/entity/spider/eyes.png"));

	public CamouflageSpiderEyesLayer(RenderLayerParent<LivingEntityRenderState, M> renderer)
	{
		super(renderer);
	}

	@Override
	public RenderType renderType()
	{
		return SPIDER_EYES;
	}
}
