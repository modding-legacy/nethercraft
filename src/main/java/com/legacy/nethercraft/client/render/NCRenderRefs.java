package com.legacy.nethercraft.client.render;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.registry.NCBlocks;

import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.renderer.Sheets;
import net.minecraft.client.renderer.blockentity.ChestRenderer;
import net.minecraft.client.resources.model.Material;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;

// ModelLayers
public interface NCRenderRefs
{
	public static final ChestMaterial GLOWOOD_CHEST_LOCATION = ChestMaterial.of(NCBlocks.glowood_chest, "glowood");
	public static final ResourceLocation GLOWOOD_CHEST_TEXTURE = Nethercraft.locate("glowood");

	public static final ChestMaterial XMAS_CHEST_LOCATION = ChestMaterial.festive(Sheets.CHEST_XMAS_LOCATION, Sheets.CHEST_XMAS_LOCATION_LEFT, Sheets.CHEST_XMAS_LOCATION_RIGHT);

	ModelLayerLocation IMP = layer("imp");

	ModelLayerLocation DARK_ZOMBIE = layer("dark_zombie");
	ModelLayerLocation DARK_ZOMBIE_INNER_ARMOR = innerArmor("dark_zombie");
	ModelLayerLocation DARK_ZOMBIE_OUTER_ARMOR = outerArmor("dark_zombie");
	ModelLayerLocation DARK_ZOMBIE_BABY = layer("dark_zombie_baby");
	ModelLayerLocation DARK_ZOMBIE_BABY_INNER_ARMOR = innerArmor("dark_zombie_baby");
	ModelLayerLocation DARK_ZOMBIE_BABY_OUTER_ARMOR = outerArmor("dark_zombie_baby");

	ModelLayerLocation TRIBAL_TRAINEE = layer("tribal_trainee");
	ModelLayerLocation TRIBAL_TRAINEE_INNER_ARMOR = innerArmor("tribal_trainee");
	ModelLayerLocation TRIBAL_TRAINEE_OUTER_ARMOR = outerArmor("tribal_trainee");

	ModelLayerLocation TRIBAL_WARRIOR = layer("tribal_warrior");
	ModelLayerLocation TRIBAL_WARRIOR_INNER_ARMOR = innerArmor("tribal_warrior");
	ModelLayerLocation TRIBAL_WARRIOR_OUTER_ARMOR = outerArmor("tribal_warrior");

	ModelLayerLocation TRIBAL_ARCHER = layer("tribal_archer");
	ModelLayerLocation TRIBAL_ARCHER_INNER_ARMOR = innerArmor("tribal_archer");
	ModelLayerLocation TRIBAL_ARCHER_OUTER_ARMOR = outerArmor("tribal_archer");

	private static ModelLayerLocation layer(String name)
	{
		return layer(name, "main");
	}

	private static ModelLayerLocation layer(String name, String layer)
	{
		return new ModelLayerLocation(Nethercraft.locate(name), layer);
	}

	private static ModelLayerLocation innerArmor(String path)
	{
		return layer(path, "inner_armor");
	}

	private static ModelLayerLocation outerArmor(String path)
	{
		return layer(path, "outer_armor");
	}

	public static record ChestMaterial(@Nullable Material single, @Nullable Material left, @Nullable Material right, @Nullable Material trappedSingle, @Nullable Material trappedLeft, @Nullable Material trappedRight)
	{

		private static final Map<ResourceKey<Block>, ChestMaterial> BY_BLOCK = new HashMap<>();

		public static ChestMaterial get(BlockState state)
		{
			return state.getBlockHolder().unwrapKey().map(BY_BLOCK::get).orElse(GLOWOOD_CHEST_LOCATION);
		}

		@Nullable
		public static ChestMaterial getFestive()
		{
			return ChestRenderer.xmasTextures() ? XMAS_CHEST_LOCATION : null;
		}

		@SuppressWarnings("deprecation")
		public static ChestMaterial of(Block block, String name)
		{
			String trap = "_trapped";
			var ret = new ChestMaterial(mat(name), mat(name + "_left"), mat(name + "_right"), mat(name + trap), mat(name + trap + "_left"), mat(name + trap + "_right"));
			BY_BLOCK.put(block.builtInRegistryHolder().key(), ret);
			return ret;
		}

		public static ChestMaterial festive(@Nullable String single, @Nullable String left, @Nullable String right)
		{
			Material sing = single == null ? null : mat(single);
			Material l = left == null ? null : mat(left);
			Material r = right == null ? null : mat(right);
			return new ChestMaterial(sing, l, r, sing, l, r);
		}

		public static ChestMaterial festive(@Nullable Material single, @Nullable Material left, @Nullable Material right)
		{
			return new ChestMaterial(single, left, right, single, left, right);
		}

		private static Material mat(String name)
		{
			return new Material(Sheets.CHEST_SHEET, Nethercraft.locate("entity/chest/" + name));
		}

		public Material single(@Nullable ChestMaterial festiveOption)
		{
			return festiveOption != null && festiveOption.single != null ? festiveOption.single : this.single;
		}

		public Material left(@Nullable ChestMaterial festiveOption)
		{
			return festiveOption != null && festiveOption.left != null ? festiveOption.left : this.left;
		}

		public Material right(@Nullable ChestMaterial festiveOption)
		{
			return festiveOption != null && festiveOption.right != null ? festiveOption.right : this.right;
		}
	}

}
