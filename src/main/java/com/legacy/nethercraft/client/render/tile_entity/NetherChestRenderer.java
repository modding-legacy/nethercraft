package com.legacy.nethercraft.client.render.tile_entity;

import com.legacy.nethercraft.client.render.NCRenderRefs;

import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.client.renderer.blockentity.ChestRenderer;
import net.minecraft.client.resources.model.Material;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.LidBlockEntity;
import net.minecraft.world.level.block.state.properties.ChestType;
import net.minecraft.world.phys.AABB;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class NetherChestRenderer<T extends BlockEntity & LidBlockEntity> extends ChestRenderer<T>
{
	public NetherChestRenderer(BlockEntityRendererProvider.Context context)
	{
		super(context);
	}

	@Override
	public AABB getRenderBoundingBox(T entity)
	{
		return AABB.encapsulatingFullBlocks(entity.getBlockPos().offset(-1, 0, -1), entity.getBlockPos().offset(2, 2, 2));
	}

	@Override
	protected Material getMaterial(T blockEntity, ChestType chestType)
	{
		NCRenderRefs.ChestMaterial mat = NCRenderRefs.GLOWOOD_CHEST_LOCATION;
		NCRenderRefs.ChestMaterial festiveMat = NCRenderRefs.ChestMaterial.getFestive();
		return chestType == ChestType.SINGLE ? mat.single(festiveMat) : (chestType == ChestType.LEFT ? mat.left(festiveMat) : mat.right(festiveMat));
	}
}