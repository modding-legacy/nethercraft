package com.legacy.nethercraft.client.render.model;

import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.entity.state.LivingEntityRenderState;
import net.minecraft.util.Mth;

public class ImpModel<T extends LivingEntityRenderState> extends EntityModel<T>
{
	public ModelPart rightLeg;
	public ModelPart leftLeg;
	public ModelPart body;
	public ModelPart leftArm;
	public ModelPart rightArm;
	public ModelPart head;
	public ModelPart tail;
	public ModelPart righthorn;
	public ModelPart lefthorn;
	public ModelPart snout;

	public ImpModel(ModelPart root)
	{
		super(root);
		this.rightLeg = root.getChild("rightLeg");
		this.leftLeg = root.getChild("leftLeg");
		this.body = root.getChild("body");
		this.tail = this.body.getChild("tail");
		this.leftArm = root.getChild("leftArm");
		this.rightArm = root.getChild("rightArm");
		this.head = root.getChild("head");
		this.lefthorn = this.head.getChild("lefthorn");
		this.snout = this.head.getChild("snout");
		this.righthorn = this.head.getChild("righthorn");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition rightLeg = partdefinition.addOrReplaceChild("rightLeg", CubeListBuilder.create().texOffs(0, 16).mirror().addBox(-2.0F, 0.0F, -2.0F, 4.0F, 6.0F, 4.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(2.9F, 18.0F, 0.0F));

		PartDefinition leftLeg = partdefinition.addOrReplaceChild("leftLeg", CubeListBuilder.create().texOffs(0, 16).addBox(-2.0F, 0.0F, -2.0F, 4.0F, 6.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(-2.9F, 18.0F, 0.0F));

		PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(16, 16).addBox(-5.0F, -2.5F, 0.0F, 10.0F, 5.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 19.0F, 0.0F, 1.5708F, 0.0F, 0.0F));

		PartDefinition tail = body.addOrReplaceChild("tail", CubeListBuilder.create().texOffs(0, 22).addBox(0.0F, 0.0F, 0.0F, 0.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 2.5F, 1.0F, 0.6283F, 0.4363F, 1.309F));

		PartDefinition leftArm = partdefinition.addOrReplaceChild("leftArm", CubeListBuilder.create().texOffs(32, 0).addBox(-2.0F, 0.0F, -1.0F, 2.0F, 6.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-5.0F, 10.5F, 0.0F));

		PartDefinition rightArm = partdefinition.addOrReplaceChild("rightArm", CubeListBuilder.create().texOffs(32, 0).mirror().addBox(0.0F, 0.0F, -1.0F, 2.0F, 6.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(5.0F, 10.5F, 0.0F));

		PartDefinition head = partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-5.0F, -8.0F, -4.0F, 10.0F, 8.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 10.0F, 0.0F));

		PartDefinition lefthorn = head.addOrReplaceChild("lefthorn", CubeListBuilder.create().texOffs(0, 2).addBox(2.0F, -9.0F, -3.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition snout = head.addOrReplaceChild("snout", CubeListBuilder.create().texOffs(36, 8).addBox(-2.0F, -4.0F, -5.0F, 4.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition righthorn = head.addOrReplaceChild("righthorn", CubeListBuilder.create().texOffs(0, 0).addBox(-4.0F, -9.0F, -3.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 32);
	}

	@Override
	public void setupAnim(T state)
	{
		float limbSwing = state.walkAnimationPos, limbSwingAmount = state.walkAnimationSpeed;

		this.head.xRot = state.xRot / Mth.RAD_TO_DEG;
		this.head.yRot = state.yRot / Mth.RAD_TO_DEG;

		this.leftLeg.xRot = Mth.cos(limbSwing * 0.6662F + Mth.PI) * 1.4F * limbSwingAmount;
		this.rightLeg.xRot = Mth.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

		this.leftArm.xRot = Mth.cos(limbSwing * 0.6662F) * 2.0F * limbSwingAmount * 0.5F;
		this.rightArm.xRot = Mth.cos(limbSwing * 0.6662F + 3.141593F) * 2.0F * limbSwingAmount * 0.5F;
	}

	/*public ImpModel()
	{
		this.texWidth = 64;
		this.texHeight = 32;
		this.lefthorn = new ModelPart(this, 0, 2);
		this.lefthorn.setPos(0.0F, 0.0F, 0.0F);
		this.lefthorn.addBox(2.0F, -9.0F, -3.0F, 2, 1, 1, 0.0F);
		this.snout = new ModelPart(this, 36, 8);
		this.snout.setPos(0.0F, 0.0F, 0.0F);
		this.snout.addBox(-2.0F, -4.0F, -5.0F, 4, 4, 1, 0.0F);
		this.leftArm = new ModelPart(this, 32, 0);
		this.leftArm.setPos(-5.0F, 10.5F, 0.0F);
		this.leftArm.addBox(-2.0F, 0.0F, -1.0F, 2, 6, 2, 0.0F);
		this.leftLeg = new ModelPart(this, 0, 16);
		this.leftLeg.setPos(-2.9F, 18.0F, 0.0F);
		this.leftLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, 0.0F);
		this.body = new ModelPart(this, 16, 16);
		this.body.setPos(0.0F, 19.0F, 0.0F);
		this.body.addBox(-5.0F, -2.5F, 0.0F, 10, 5, 9, 0.0F);
		this.setRotateAngle(body, 1.5707963267948966F, 0.0F, 0.0F);
		this.rightArm = new ModelPart(this, 32, 0);
		this.rightArm.mirror = true;
		this.rightArm.setPos(5.0F, 10.5F, 0.0F);
		this.rightArm.addBox(0.0F, 0.0F, -1.0F, 2, 6, 2, 0.0F);
		this.tail = new ModelPart(this, 0, 22);
		this.tail.setPos(0.0F, 2.5F, 1.0F);
		this.tail.addBox(0.0F, 0.0F, 0.0F, 0, 4, 4, 0.0F);
		this.setRotateAngle(tail, 0.6283185307179586F, 0.4363323129985824F, 1.3089969389957472F);
		this.righthorn = new ModelPart(this, 0, 0);
		this.righthorn.setPos(0.0F, 0.0F, 0.0F);
		this.righthorn.addBox(-4.0F, -9.0F, -3.0F, 2, 1, 1, 0.0F);
		this.head = new ModelPart(this, 0, 0);
		this.head.setPos(0.0F, 10.0F, 0.0F);
		this.head.addBox(-5.0F, -8.0F, -4.0F, 10, 8, 8, 0.0F);
		this.rightLeg = new ModelPart(this, 0, 16);
		this.rightLeg.mirror = true;
		this.rightLeg.setPos(2.9F, 18.0F, 0.0F);
		this.rightLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, 0.0F);
		this.head.addChild(this.lefthorn);
		this.head.addChild(this.snout);
		this.body.addChild(this.tail);
		this.head.addChild(this.righthorn);
	}*/
}