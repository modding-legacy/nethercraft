package com.legacy.nethercraft.client.render.entity.tribal;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.client.render.NCRenderRefs;
import com.legacy.nethercraft.entity.tribal.TribalTraineeEntity;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.state.HumanoidRenderState;
import net.minecraft.resources.ResourceLocation;

public class TribalTraineeRenderer<T extends TribalTraineeEntity> extends AbstractTribalRenderer<T, HumanoidRenderState>
{
	public TribalTraineeRenderer(EntityRendererProvider.Context context)
	{
		super(context, NCRenderRefs.TRIBAL_TRAINEE, NCRenderRefs.TRIBAL_TRAINEE_INNER_ARMOR, NCRenderRefs.TRIBAL_TRAINEE_OUTER_ARMOR);
	}

	@Override
	public ResourceLocation getTextureLocation(HumanoidRenderState entity)
	{
		return Nethercraft.locate("textures/entity/tribal/tribal_trainee.png");
	}

	@Override
	public HumanoidRenderState createRenderState()
	{
		return new HumanoidRenderState();
	}
}