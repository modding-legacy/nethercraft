package com.legacy.nethercraft.client.render.entity;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.client.render.entity.layer.CamouflageSpiderEyesLayer;
import com.legacy.nethercraft.client.render.entity.state.CamouflageSpiderRenderState;
import com.legacy.nethercraft.entity.CamouflageSpiderEntity;

import net.minecraft.client.model.SpiderModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelLayers;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class CamouflageSpiderRenderer<T extends CamouflageSpiderEntity, S extends CamouflageSpiderEntity> extends MobRenderer<T, CamouflageSpiderRenderState, SpiderModel>
{
	private static final ResourceLocation NETHERRACK_TEXTURE = Nethercraft.locate("textures/entity/spider/spider_netherrack.png");
	private static final ResourceLocation DIRT_TEXTURE = Nethercraft.locate("textures/entity/spider/spider_nether_dirt.png");

	public CamouflageSpiderRenderer(EntityRendererProvider.Context p_174401_)
	{
		this(p_174401_, ModelLayers.SPIDER);
	}

	// I HATE generic types, I DO.
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public CamouflageSpiderRenderer(EntityRendererProvider.Context context, ModelLayerLocation layer)
	{
		super(context, new SpiderModel(context.bakeLayer(layer)), 0.8F);
		this.addLayer(new CamouflageSpiderEyesLayer(this));
	}

	@Override
	protected float getFlipDegrees()
	{
		return 180.0F;
	}

	@Override
	public ResourceLocation getTextureLocation(CamouflageSpiderRenderState state)
	{
		return state.variant == 1 ? NETHERRACK_TEXTURE : DIRT_TEXTURE;
	}

	public CamouflageSpiderRenderState createRenderState()
	{
		return new CamouflageSpiderRenderState();
	}

	public void extractRenderState(T entity, CamouflageSpiderRenderState state, float partialTicks)
	{
		super.extractRenderState(entity, state, partialTicks);
		state.variant = entity.getSpiderType();
	}
}
