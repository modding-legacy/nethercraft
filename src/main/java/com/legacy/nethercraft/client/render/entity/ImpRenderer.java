package com.legacy.nethercraft.client.render.entity;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.client.render.NCRenderRefs;
import com.legacy.nethercraft.client.render.entity.state.ImpRenderState;
import com.legacy.nethercraft.client.render.model.ImpModel;
import com.legacy.nethercraft.entity.ImpEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

public class ImpRenderer<T extends ImpEntity> extends MobRenderer<T, ImpRenderState, ImpModel<ImpRenderState>>
{
	static final ResourceLocation RED_IMP_TEXTURE = tex("red"), ORANGE_IMP_TEXTURE = tex("orange"),
			GREEN_IMP_TEXTURE = tex("green");

	public ImpRenderer(EntityRendererProvider.Context context)
	{
		super(context, new ImpModel<>(context.bakeLayer(NCRenderRefs.IMP)), 0.5F);
	}

	private static final ResourceLocation tex(String name)
	{
		return Nethercraft.locate("textures/entity/imp/imp_NAME.png".replace("NAME", name));
	}

	@Override
	protected void scale(ImpRenderState state, PoseStack stack)
	{
		super.scale(state, stack);

		float scale = state.variant == 2 ? 1.8F : state.variant == 1 ? 1.5F : 1.0F;
		stack.scale(scale, scale, scale);
	}

	@Override
	public void extractRenderState(T entity, ImpRenderState state, float partialTicks)
	{
		super.extractRenderState(entity, state, partialTicks);
		state.variant = entity.getImpType();
	}

	@Override
	public ResourceLocation getTextureLocation(ImpRenderState state)
	{
		return state.variant == 2 ? RED_IMP_TEXTURE : state.variant == 1 ? ORANGE_IMP_TEXTURE : GREEN_IMP_TEXTURE;
	}

	@Override
	public ImpRenderState createRenderState()
	{
		return new ImpRenderState();
	}

}