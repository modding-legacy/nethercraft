package com.legacy.nethercraft.client.render.entity;

import com.legacy.nethercraft.Nethercraft;

import net.minecraft.client.model.ZombieModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelLayers;
import net.minecraft.client.renderer.entity.AbstractZombieRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.state.ZombieRenderState;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.monster.Zombie;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class BloodyZombieRenderer extends AbstractZombieRenderer<Zombie, ZombieRenderState, ZombieModel<ZombieRenderState>>
{
	private static final ResourceLocation BLOODY_ZOMBIE_TEXTURE = Nethercraft.locate("textures/entity/zombie/bloody_zombie.png");

	public BloodyZombieRenderer(EntityRendererProvider.Context p_174456_)
	{
		this(p_174456_, ModelLayers.ZOMBIE, ModelLayers.ZOMBIE_BABY, ModelLayers.ZOMBIE_INNER_ARMOR, ModelLayers.ZOMBIE_OUTER_ARMOR, ModelLayers.ZOMBIE_BABY_INNER_ARMOR, ModelLayers.ZOMBIE_BABY_OUTER_ARMOR);
	}

	public BloodyZombieRenderer(EntityRendererProvider.Context context, ModelLayerLocation zombieLayer, ModelLayerLocation innerArmor, ModelLayerLocation outerArmor, ModelLayerLocation p_362432_, ModelLayerLocation p_361708_, ModelLayerLocation p_365510_)
	{
		super(context, new ZombieModel<>(context.bakeLayer(zombieLayer)), new ZombieModel<>(context.bakeLayer(innerArmor)), new ZombieModel<>(context.bakeLayer(outerArmor)), new ZombieModel<>(context.bakeLayer(p_362432_)), new ZombieModel<>(context.bakeLayer(p_361708_)), new ZombieModel<>(context.bakeLayer(p_365510_)));
	}

	@Override
	public ZombieRenderState createRenderState()
	{
		return new ZombieRenderState();
	}

	@Override
	public ResourceLocation getTextureLocation(ZombieRenderState p_362921_)
	{
		return BLOODY_ZOMBIE_TEXTURE;
	}
}
