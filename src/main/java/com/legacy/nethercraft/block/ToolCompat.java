package com.legacy.nethercraft.block;

import java.util.HashMap;
import java.util.Map;

import com.legacy.nethercraft.registry.NCBlocks;

import net.minecraft.world.level.block.Block;

public class ToolCompat
{
	public static final Map<Block, Block> AXE_STRIPPING = new HashMap<>();
	public static final Map<Block, Block> HOE_TILLING = new HashMap<>();

	static
	{
		axeStripping(NCBlocks.glowood_log, NCBlocks.stripped_glowood_log);
		// axeStripping(NetherBlocks.glowood_wood, NetherBlocks.stripped_glowood_wood);
		// FIXME

		hoeTilling(NCBlocks.nether_dirt, NCBlocks.nether_farmland);
	}

	public static void init()
	{
	}

	static void axeStripping(Block log, Block stripped)
	{
		AXE_STRIPPING.put(log, stripped);
	}

	static void hoeTilling(Block dirt, Block farmland)
	{
		HOE_TILLING.put(dirt, farmland);
	}
}
