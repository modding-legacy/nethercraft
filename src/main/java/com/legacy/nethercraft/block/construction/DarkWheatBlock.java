package com.legacy.nethercraft.block.construction;

import com.legacy.nethercraft.registry.NCItems;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.neoforged.neoforge.common.CommonHooks;

public class DarkWheatBlock extends CropBlock
{
	/*public static final IntegerProperty AGE = BlockStateProperties.AGE_5;
	private static final VoxelShape[] SHAPE = new VoxelShape[] {
			Block.box(0.0D, 0.0D, 0.0D, 16.0D, 2.0D, 16.0D),
			Block.box(0.0D, 0.0D, 0.0D, 16.0D, 4.0D, 16.0D),
			Block.box(0.0D, 0.0D, 0.0D, 16.0D, 6.0D, 16.0D),
			Block.box(0.0D, 0.0D, 0.0D, 16.0D, 8.0D, 16.0D),
			Block.box(0.0D, 0.0D, 0.0D, 16.0D, 10.0D, 16.0D),
			Block.box(0.0D, 0.0D, 0.0D, 16.0D, 12.0D, 16.0D),
			Block.box(0.0D, 0.0D, 0.0D, 16.0D, 14.0D, 16.0D),
			Block.box(0.0D, 0.0D, 0.0D, 16.0D, 16.0D, 16.0D) };*/

	public DarkWheatBlock(BlockBehaviour.Properties properties)
	{
		super(properties);
	}

	@Override
	protected void randomTick(BlockState state, ServerLevel level, BlockPos pos, RandomSource random)
	{
		if (random.nextInt(3) != 0)
			super.randomTick(state, level, pos, random);
	}

	@Override
	protected int getBonemealAgeIncrease(Level level)
	{
		return super.getBonemealAgeIncrease(level) / 3;
	}

	/*@Override
	protected VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context)
	{
		return SHAPE[this.getAge(state)];
	}*/

	/*@Override
	public IntegerProperty getAgeProperty()
	{
		return AGE;
	}
	
	@Override
	public int getMaxAge()
	{
		return 5;
	}*/

	@Override
	protected ItemLike getBaseSeedId()
	{
		return NCItems.dark_seeds;
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		//builder.add(AGE);
	}

	@Override
	public void tick(BlockState state, ServerLevel level, BlockPos pos, RandomSource random)
	{
		if (random.nextInt(3) != 0)
		{
			if (!level.isAreaLoaded(pos, 1))
				return;

			// FIXME: Did this grow at high light level?
			int age = this.getAge(state);
			if (age < this.getMaxAge())
			{
				float f = getGrowthSpeed(state, level, pos);
				if (CommonHooks.canCropGrow(level, pos, state, random.nextInt((int) (25.0F / f) + 1) == 0))
				{
					level.setBlock(pos, this.getStateForAge(age + 1), 2);
					CommonHooks.fireCropGrowPost(level, pos, state);
				}
			}
		}
	}

	@Override
	public boolean canSurvive(BlockState state, LevelReader worldIn, BlockPos pos)
	{
		BlockPos blockpos = pos.below();

		if (state.getBlock() == this)
			return worldIn.getBlockState(blockpos).canSustainPlant(worldIn, blockpos, Direction.UP, state).isTrue();

		return this.mayPlaceOn(worldIn.getBlockState(blockpos), worldIn, blockpos);
	}
}