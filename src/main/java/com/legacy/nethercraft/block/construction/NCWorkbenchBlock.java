package com.legacy.nethercraft.block.construction;

import com.legacy.nethercraft.block.container.NCWorkbenchContainer;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.CraftingTableBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;

public class NCWorkbenchBlock extends CraftingTableBlock
{
	private static final Component GUI_TITLE = Component.translatable("container.crafting");

	public NCWorkbenchBlock(BlockBehaviour.Properties props)
	{
		super(props);
	}

	@Override
	public MenuProvider getMenuProvider(BlockState state, Level worldIn, BlockPos pos)
	{
		return new SimpleMenuProvider((id, inventory, entity) ->
		{
			return new NCWorkbenchContainer(id, inventory, ContainerLevelAccess.create(worldIn, pos), this);
		}, GUI_TITLE);
	}
}