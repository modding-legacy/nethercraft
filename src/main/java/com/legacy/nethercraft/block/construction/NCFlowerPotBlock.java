package com.legacy.nethercraft.block.construction;

import java.util.function.Supplier;

import javax.annotation.Nullable;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;

public class NCFlowerPotBlock extends FlowerPotBlock
{
	public NCFlowerPotBlock(@Nullable Supplier<FlowerPotBlock> emptyPot, Supplier<? extends Block> flower, BlockBehaviour.Properties properties)
	{
		super(emptyPot, flower, properties);
		((FlowerPotBlock) Blocks.FLOWER_POT).addPlant(BuiltInRegistries.BLOCK.getKey(flower.get()), () -> this);
	}

	public NCFlowerPotBlock(Supplier<? extends Block> flower, BlockBehaviour.Properties properties)
	{
		this(() -> (FlowerPotBlock) Blocks.FLOWER_POT, flower, properties);
	}

	public NCFlowerPotBlock(java.util.function.Supplier<? extends Block> flower)
	{
		this(flower, BlockBehaviour.Properties.of().strength(0.0F));
	}
}
