package com.legacy.nethercraft.block.construction;

import java.util.function.Supplier;

import com.legacy.nethercraft.registry.NCBlockEntityTypes;
import com.legacy.nethercraft.tile_entity.NetherChestBlockEntity;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;

public class NetherChestBlock extends ChestBlock
{
	public NetherChestBlock(BlockBehaviour.Properties properties, Supplier<BlockEntityType<? extends ChestBlockEntity>> blockEntity)
	{
		super(blockEntity, properties);
	}

	public NetherChestBlock(BlockBehaviour.Properties properties)
	{
		this(properties, NCBlockEntityTypes.NETHER_CHEST);
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new NetherChestBlockEntity(pos, state);
	}
}
