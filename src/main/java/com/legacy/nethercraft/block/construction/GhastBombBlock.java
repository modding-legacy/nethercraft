package com.legacy.nethercraft.block.construction;

import javax.annotation.Nullable;

import com.legacy.nethercraft.entity.misc.GhastBombEntity;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Explosion;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.redstone.Orientation;
import net.minecraft.world.phys.BlockHitResult;
import net.neoforged.neoforge.common.ItemAbilities;

public class GhastBombBlock extends Block
{
	public static final BooleanProperty UNSTABLE = BlockStateProperties.UNSTABLE;

	public GhastBombBlock(BlockBehaviour.Properties properties)
	{
		super(properties);
		this.registerDefaultState(this.defaultBlockState().setValue(UNSTABLE, Boolean.valueOf(false)));
	}

	@Override
	public void onCaughtFire(BlockState state, Level world, BlockPos pos, @Nullable net.minecraft.core.Direction face, @Nullable LivingEntity igniter)
	{
		explode(world, pos, igniter);
	}

	@Override
	protected void onPlace(BlockState state, Level level, BlockPos pos, BlockState oldState, boolean isMoving)
	{
		if (!oldState.is(state.getBlock()))
		{
			if (level.hasNeighborSignal(pos))
			{
				onCaughtFire(state, level, pos, null, null);
				level.removeBlock(pos, false);
			}
		}
	}

	@Override
	protected void neighborChanged(BlockState state, Level level, BlockPos pos, Block p_57460_, @Nullable Orientation orientation, boolean isMoving)
	{
		if (level.hasNeighborSignal(pos))
		{
			onCaughtFire(state, level, pos, null, null);
			level.removeBlock(pos, false);
		}
	}

	@Override
	public BlockState playerWillDestroy(Level level, BlockPos pos, BlockState state, Player player)
	{
		if (!level.isClientSide() && !player.isCreative() && state.getValue(UNSTABLE))
			onCaughtFire(state, level, pos, null, null);

		return super.playerWillDestroy(level, pos, state, player);
	}

	@Override
	public void wasExploded(ServerLevel level, BlockPos pos, Explosion explosionIn)
	{
		if (!level.isClientSide)
		{
			GhastBombEntity tntentity = new GhastBombEntity(level, (double) ((float) pos.getX() + 0.5F), (double) pos.getY(), (double) ((float) pos.getZ() + 0.5F), explosionIn.getIndirectSourceEntity());
			tntentity.setFuse((short) (level.random.nextInt(tntentity.getFuse() / 4) + tntentity.getFuse() / 8));
			level.addFreshEntity(tntentity);
		}
	}

	public static void explode(Level level, BlockPos posIn)
	{
		explode(level, posIn, (LivingEntity) null);
	}

	private static void explode(Level level, BlockPos posIn, LivingEntity entityIn)
	{
		if (!level.isClientSide)
		{
			GhastBombEntity tntentity = new GhastBombEntity(level, (double) ((float) posIn.getX() + 0.5F), (double) posIn.getY(), (double) ((float) posIn.getZ() + 0.5F), entityIn);
			level.addFreshEntity(tntentity);
			level.playSound((Player) null, tntentity.blockPosition().getX(), tntentity.blockPosition().getY(), tntentity.blockPosition().getZ(), SoundEvents.TNT_PRIMED, SoundSource.BLOCKS, 1.0F, 1.0F);
		}
	}

	@Override
	public InteractionResult useItemOn(ItemStack stack, BlockState state, Level level, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hit)
	{
		ItemStack itemstack = player.getItemInHand(hand);
		if (!itemstack.canPerformAction(ItemAbilities.FIRESTARTER_LIGHT))
			return super.useItemOn(stack, state, level, pos, player, hand, hit);

		explode(level, pos, player);
		level.setBlock(pos, Blocks.AIR.defaultBlockState(), 11);

		if (itemstack.getMaxStackSize() > 1)
			itemstack.consume(1, player);
		else
			itemstack.hurtAndBreak(1, player, LivingEntity.getSlotForHand(hand));

		player.awardStat(Stats.ITEM_USED.get(itemstack.getItem()));
		return InteractionResult.SUCCESS;
	}

	@Override
	public void onProjectileHit(Level level, BlockState state, BlockHitResult hit, Projectile projectile)
	{
		if (!level.isClientSide && projectile instanceof AbstractArrow)
		{
			AbstractArrow abstractarrowentity = (AbstractArrow) projectile;
			Entity entity = abstractarrowentity.getOwner();
			if (abstractarrowentity.isOnFire())
			{
				BlockPos blockpos = hit.getBlockPos();
				explode(level, blockpos, entity instanceof LivingEntity ? (LivingEntity) entity : null);
				level.removeBlock(blockpos, false);
			}
		}
	}

	@Override
	public boolean dropFromExplosion(Explosion explosionIn)
	{
		return false;
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(UNSTABLE);
	}
}