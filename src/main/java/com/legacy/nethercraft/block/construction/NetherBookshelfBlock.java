package com.legacy.nethercraft.block.construction;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.LevelReader;

import net.minecraft.world.level.block.state.BlockBehaviour;

public class NetherBookshelfBlock extends Block
{
	public NetherBookshelfBlock(BlockBehaviour.Properties props)
	{
		super(props);
	}

	@Override
	public float getEnchantPowerBonus(BlockState state, LevelReader world, BlockPos pos)
	{
		return 1.0F;
	}
}
