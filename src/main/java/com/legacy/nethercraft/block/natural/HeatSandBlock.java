package com.legacy.nethercraft.block.natural;

import com.legacy.nethercraft.registry.NCBlocks;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.FallingBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.neoforge.common.NeoForgeMod;
import net.neoforged.neoforge.common.util.TriState;

public class HeatSandBlock extends FallingBlock
{
	public static final MapCodec<HeatSandBlock> CODEC = simpleCodec(HeatSandBlock::new);

	public HeatSandBlock(BlockBehaviour.Properties properties)
	{
		super(properties);
	}

	@Override
	public TriState canSustainPlant(BlockState state, BlockGetter level, BlockPos pos, Direction facing, BlockState plant)
	{
		if (plant.is(NCBlocks.lava_reeds))
		{
			for (Direction dir : Direction.Plane.HORIZONTAL)
				if (level.getBlockState(pos.relative(dir)).getFluidState().getFluidType() == NeoForgeMod.LAVA_TYPE)
					return TriState.TRUE;

			return TriState.FALSE;
		}

		return super.canSustainPlant(state, level, pos, facing, plant);
	}

	@Override
	protected MapCodec<? extends HeatSandBlock> codec()
	{
		return CODEC;
	}

	// FIXME
	@Override
	public int getDustColor(BlockState state, BlockGetter level, BlockPos pos)
	{
		return -16777216;
	}
}