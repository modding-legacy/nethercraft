package com.legacy.nethercraft.block.natural;

import com.legacy.nethercraft.data.NCTags;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SugarCaneBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.neoforge.common.NeoForgeMod;
import net.neoforged.neoforge.common.util.TriState;

public class LavaReedsBlock extends SugarCaneBlock
{
	public LavaReedsBlock(Properties properties)
	{
		super(properties);
	}

	@Override
	protected boolean canSurvive(BlockState state, LevelReader level, BlockPos pos)
	{
		BlockPos soilPos = pos.below();

		BlockState soil = level.getBlockState(soilPos);
		if (soil.is(this))
			return true;

		TriState soilDecision = soil.canSustainPlant(level, soilPos, Direction.UP, state);
		if (!soilDecision.isDefault())
			return soilDecision.isTrue();

		if (soil.is(NCTags.Blocks.LAVA_REEDS_PLANTABLE_ON))
		{
			for (Direction direction : Direction.Plane.HORIZONTAL)
			{
				BlockState soilNeighbor = level.getBlockState(soilPos.relative(direction));
				if (soilNeighbor.getFluidState().getType().getFluidType() == NeoForgeMod.LAVA_TYPE.value() || soilNeighbor.is(Blocks.MAGMA_BLOCK))
					return true;
			}
		}

		return false;
	}
}
