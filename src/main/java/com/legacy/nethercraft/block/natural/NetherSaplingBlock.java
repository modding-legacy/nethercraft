package com.legacy.nethercraft.block.natural;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.grower.TreeGrower;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.neoforge.common.Tags;

public class NetherSaplingBlock extends SaplingBlock
{
	public NetherSaplingBlock(TreeGrower treeGrower, BlockBehaviour.Properties properties)
	{
		super(treeGrower, properties);
	}

	@Override
	public void randomTick(BlockState state, ServerLevel level, BlockPos pos, RandomSource random)
	{
		super.randomTick(state, level, pos, random);

		if (!level.isAreaLoaded(pos, 1))
			return;

		if (random.nextInt(7) == 0)
			this.advanceTree(level, pos, state, random);
	}

	@Override
	protected boolean mayPlaceOn(BlockState state, BlockGetter worldIn, BlockPos pos)
	{
		return state.is(Tags.Blocks.NETHERRACKS) || state.is(BlockTags.DIRT) || super.mayPlaceOn(state, worldIn, pos) || state.is(BlockTags.NYLIUM);
	}
}
