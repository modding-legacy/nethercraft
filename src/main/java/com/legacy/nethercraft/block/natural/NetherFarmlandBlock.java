package com.legacy.nethercraft.block.natural;

import com.legacy.nethercraft.registry.NCBlocks;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FarmBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;

public class NetherFarmlandBlock extends FarmBlock
{
	public NetherFarmlandBlock(Block.Properties builder)
	{
		super(builder);
	}

	@Override
	public void tick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
		if (!state.canSurvive(level, pos))
			turnToNetherDirt(state, level, pos);
	}

	@Override
	public void randomTick(BlockState state, ServerLevel level, BlockPos pos, RandomSource random)
	{
		if (!state.canSurvive(level, pos))
		{
			turnToNetherDirt(state, level, pos);
		}
		else
		{
			int i = state.getValue(MOISTURE);
			if (!isDarkEnough(level, pos))
			{
				if (i > 0)
				{
					level.setBlock(pos, state.setValue(MOISTURE, Integer.valueOf(i - 1)), 2);
				}
				else if (!shouldMaintainFarmland(level, pos))
				{
					turnToNetherDirt(state, level, pos);
				}
			}
			else if (i < 7)
			{
				level.setBlock(pos, state.setValue(MOISTURE, Integer.valueOf(7)), 2);
			}
		}
	}

	private static boolean isDarkEnough(ServerLevel worldIn, BlockPos pos)
	{
		return worldIn.getMaxLocalRawBrightness(pos.above()) <= 0;
	}

	public static void turnToNetherDirt(BlockState state, Level level, BlockPos pos)
	{
		BlockState blockstate = pushEntitiesUp(state, NCBlocks.nether_dirt.defaultBlockState(), level, pos);
		level.setBlockAndUpdate(pos, blockstate);
		level.gameEvent(GameEvent.BLOCK_CHANGE, pos, GameEvent.Context.of(null, blockstate));
	}
}