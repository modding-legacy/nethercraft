package com.legacy.nethercraft.block.natural;

import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.block.MushroomBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;

public class GlowshroomBlock extends MushroomBlock
{
	public GlowshroomBlock(ResourceKey<ConfiguredFeature<?, ?>> feature, BlockBehaviour.Properties properties)
	{
		super(feature, properties);
	}

	/*@Override
	public boolean growMushroom(ServerLevel world, BlockPos pos, BlockState state, RandomSource rand)
	{
		world.removeBlock(pos, false);
		ConfiguredFeature<?, ?> configuredfeature;
	
		if (this == NCBlocks.purple_glowshroom)
		{
			configuredfeature = NetherFeatures.Configured.HUGE_PURPLE_GLOWSHROOM.getHolder(world.registryAccess());
		}
		else
		{
			if (this != NCBlocks.green_glowshroom)
			{
				world.setBlock(pos, state, 3);
				return false;
			}
	
			configuredfeature = NetherFeatures.Configured.HUGE_GREEN_GLOWSHROOM;
		}
	
		//var mushroomFeature = level.registryAccess().registryOrThrow(Registries.CONFIGURED_FEATURE).getHolder(SkiesConfiguredFeatures.GIANT_SNOWCAP_MUSHROOM);
	
		if (mushroomFeature.isPresent())
		{
		if (configuredfeature.place(world, world.getChunkSource().getGenerator(), rand, pos))
		{
			return true;
		}
		else
		{
			world.setBlock(pos, state, 3);
			return false;
		}
	}*/
}
