package com.legacy.nethercraft.data;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Stream;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.block.construction.NCWorkbenchBlock;
import com.legacy.nethercraft.block.construction.NetherBookshelfBlock;
import com.legacy.nethercraft.registry.NCBlocks;
import com.legacy.nethercraft.registry.NCItems;

import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.BiomeTagsProvider;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.tags.BiomeTags;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.ButtonBlock;
import net.minecraft.world.level.block.CeilingHangingSignBlock;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.FenceGateBlock;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.PressurePlateBlock;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.StandingSignBlock;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraft.world.level.block.WallHangingSignBlock;
import net.minecraft.world.level.block.WallSignBlock;
import net.neoforged.neoforge.common.Tags;
import net.neoforged.neoforge.common.data.BlockTagsProvider;

public class NCTagProv
{
	public static class BlockProv extends BlockTagsProvider
	{
		public BlockProv(DataGenerator generatorIn, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, Nethercraft.MODID);
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void addTags(Provider prov)
		{
			nethercraft();
			vanilla();
			forge();

			// seems to need to happen later
			this.tag(NCTags.Blocks.GLOWSHROOM_PLANTABLE_ON).add(NCBlocks.nether_dirt, Blocks.NETHERRACK).addTags(BlockTags.MUSHROOM_GROW_BLOCK, BlockTags.NYLIUM);
		}

		@SuppressWarnings("unchecked")
		void nethercraft()
		{
			this.tag(NCTags.Blocks.PLANKS).add(NCBlocks.glowood_planks);

			this.tag(NCTags.Blocks.GLOWOOD_LOGS).add(NCBlocks.glowood_log, NCBlocks.glowood_wood, NCBlocks.stripped_glowood_log, NCBlocks.stripped_glowood_wood);

			this.tag(NCTags.Blocks.LAVA_REEDS_PLANTABLE_ON).addTags(BlockTags.DIRT, BlockTags.SAND);

		}

		@SuppressWarnings("unchecked")
		void vanilla()
		{
			addMatching(b -> b instanceof SaplingBlock, BlockTags.SAPLINGS);
			addMatching(b -> b instanceof FlowerPotBlock, BlockTags.FLOWER_POTS);
			addMatching(b -> b instanceof LeavesBlock, BlockTags.LEAVES, BlockTags.MINEABLE_WITH_HOE);
			addMatching(b -> b instanceof ButtonBlock, BlockTags.WOODEN_BUTTONS);
			addMatching(b -> b instanceof DoorBlock, BlockTags.WOODEN_DOORS);
			addMatching(b -> b instanceof FenceBlock, BlockTags.WOODEN_FENCES);
			addMatching(b -> b instanceof PressurePlateBlock, BlockTags.WOODEN_PRESSURE_PLATES);
			addMatching(b -> b instanceof SlabBlock, BlockTags.WOODEN_SLABS);
			addMatching(b -> b instanceof StairBlock, BlockTags.WOODEN_STAIRS);
			addMatching(b -> b instanceof TrapDoorBlock, BlockTags.WOODEN_TRAPDOORS);
			addMatching(b -> b instanceof RotatedPillarBlock, BlockTags.LOGS_THAT_BURN);
			addMatching(b -> b instanceof NetherBookshelfBlock || b instanceof NCWorkbenchBlock, BlockTags.MINEABLE_WITH_AXE);
			addMatching(b -> b instanceof WallSignBlock, BlockTags.WALL_SIGNS);
			addMatching(b -> b instanceof StandingSignBlock, BlockTags.STANDING_SIGNS);
			addMatching(b -> b instanceof CeilingHangingSignBlock, BlockTags.CEILING_HANGING_SIGNS);
			addMatching(b -> b instanceof WallHangingSignBlock, BlockTags.WALL_HANGING_SIGNS);

			this.tag(BlockTags.DIRT).add(NCBlocks.nether_dirt);
			this.tag(BlockTags.SAND).add(NCBlocks.heat_sand);
			this.tag(BlockTags.MUSHROOM_GROW_BLOCK).add(NCBlocks.nether_dirt);

			this.tag(BlockTags.PLANKS).addTag(NCTags.Blocks.PLANKS);

			this.tag(BlockTags.LOGS).addTags(NCTags.Blocks.GLOWOOD_LOGS);
		}

		void forge()
		{
			addMatching(b -> b instanceof ChestBlock, Tags.Blocks.CHESTS_WOODEN);
			addMatching(b -> b instanceof FenceBlock, Tags.Blocks.FENCES_WOODEN);
			addMatching(b -> b instanceof FenceGateBlock, Tags.Blocks.FENCE_GATES_WOODEN);

			this.tag(Tags.Blocks.SANDS_RED).add(NCBlocks.heat_sand);
		}

		private Stream<Block> getMatching(Function<Block, Boolean> condition)
		{
			return BuiltInRegistries.BLOCK.stream().filter(block -> BuiltInRegistries.BLOCK.getKey(block).getNamespace().equals(Nethercraft.MODID) && condition.apply(block));
		}

		@SafeVarargs
		private void addMatching(Function<Block, Boolean> condition, TagKey<Block>... tags)
		{
			for (TagKey<Block> tag : tags)
				getMatching(condition).forEach(this.tag(tag)::add);
		}

		@Override
		public String getName()
		{
			return "Nethercraft Block Tags";
		}
	}

	public static class ItemProv extends ItemTagsProvider
	{
		public ItemProv(DataGenerator generatorIn, CompletableFuture<TagLookup<Block>> blocktagProvIn, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, blocktagProvIn, Nethercraft.MODID);
		}

		@Override
		protected void addTags(Provider prov)
		{
			nethercraft();
			vanilla();
			forge();
		}

		void nethercraft()
		{
			this.copy(NCTags.Blocks.PLANKS, NCTags.Items.PLANKS);

			this.copy(NCTags.Blocks.GLOWOOD_LOGS, NCTags.Items.GLOWOOD_LOGS);
		}

		void vanilla()
		{
			this.copy(BlockTags.LEAVES, ItemTags.LEAVES);
			this.copy(BlockTags.LOGS, ItemTags.LOGS);
			this.copy(BlockTags.PLANKS, ItemTags.PLANKS);
			this.copy(BlockTags.SAPLINGS, ItemTags.SAPLINGS);
			// this.copy(BlockTags.SLABS, ItemTags.SLABS);
			// this.copy(BlockTags.STAIRS, ItemTags.STAIRS);
			this.copy(BlockTags.WOODEN_BUTTONS, ItemTags.WOODEN_BUTTONS);
			this.copy(BlockTags.WOODEN_DOORS, ItemTags.WOODEN_DOORS);
			this.copy(BlockTags.WOODEN_FENCES, ItemTags.WOODEN_FENCES);
			this.copy(BlockTags.WOODEN_PRESSURE_PLATES, ItemTags.WOODEN_PRESSURE_PLATES);
			this.copy(BlockTags.WOODEN_SLABS, ItemTags.WOODEN_SLABS);
			this.copy(BlockTags.WOODEN_STAIRS, ItemTags.WOODEN_STAIRS);
			this.copy(BlockTags.WOODEN_TRAPDOORS, ItemTags.WOODEN_TRAPDOORS);
			this.copy(BlockTags.STANDING_SIGNS, ItemTags.SIGNS);
			this.copy(BlockTags.CEILING_HANGING_SIGNS, ItemTags.HANGING_SIGNS);

			this.tag(ItemTags.ARROWS).add(NCItems.netherrack_arrow, NCItems.neridium_arrow, NCItems.pyridium_arrow, NCItems.linium_arrow);
		}

		void forge()
		{
		}

		@Override
		public String getName()
		{
			return "Nethercraft Item Tags";
		}
	}

	public static class BiomeProv extends BiomeTagsProvider
	{
		public BiomeProv(DataGenerator gen, CompletableFuture<Provider> lookup)
		{
			super(gen.getPackOutput(), lookup, Nethercraft.MODID);
		}

		@Override
		protected void addTags(Provider prov)
		{
			nethercraft();
			vanilla();
			forge();
		}

		void nethercraft()
		{
			this.tag(NCTags.Biomes.HAS_FOULITE_ORE).addTag(BiomeTags.IS_NETHER);
			this.tag(NCTags.Biomes.HAS_NERIDIUM_ORE).addTag(BiomeTags.IS_NETHER);
			this.tag(NCTags.Biomes.HAS_PYRIDIUM_ORE).addTag(BiomeTags.IS_NETHER);
			this.tag(NCTags.Biomes.HAS_LINIUM_ORE).addTag(BiomeTags.IS_NETHER);
			this.tag(NCTags.Biomes.HAS_W_ORE).addTag(BiomeTags.IS_NETHER);
		}

		void vanilla()
		{
		}

		void forge()
		{
		}

		@Override
		public String getName()
		{
			return "Nethercraft Biome Tags";
		}
	}
}
