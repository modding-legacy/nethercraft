package com.legacy.nethercraft.data;

import java.util.ArrayList;
import java.util.List;

import com.legacy.nethercraft.registry.NCBlocks;

import net.minecraft.data.BlockFamily;
import net.minecraft.data.BlockFamily.Builder;
import net.minecraft.world.level.block.Block;

public class NCBlockFamilies
{
	private static final List<BlockFamily> FAMILIES = new ArrayList<>();
	public static final BlockFamily GLOWOOD_FAMILY = family(NCBlocks.glowood_planks).button(NCBlocks.glowood_button).fence(NCBlocks.glowood_fence).fenceGate(NCBlocks.glowood_fence_gate).pressurePlate(NCBlocks.glowood_pressure_plate)/*.sign(NCBlocks.glowood_sign, NCBlocks.glowood_wall_sign)*/.slab(NCBlocks.glowood_slab).stairs(NCBlocks.glowood_stairs).door(NCBlocks.glowood_door).trapdoor(NCBlocks.glowood_trapdoor).recipeGroupPrefix("wooden").recipeUnlockedBy("has_planks").getFamily();
	private static Builder family(Block block)
	{
		var builder = new BlockFamily.Builder(block);

		FAMILIES.add(builder.getFamily());

		return builder;
	}

	public static List<BlockFamily> getFamilies()
	{
		return FAMILIES;
	}
}