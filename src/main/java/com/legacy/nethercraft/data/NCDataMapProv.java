package com.legacy.nethercraft.data;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import com.legacy.nethercraft.registry.NCBlocks;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.world.level.block.Block;
import net.neoforged.neoforge.common.data.DataMapProvider;
import net.neoforged.neoforge.registries.datamaps.builtin.Compostable;
import net.neoforged.neoforge.registries.datamaps.builtin.NeoForgeDataMaps;

public class NCDataMapProv extends DataMapProvider
{
	public NCDataMapProv(PackOutput packOutput, CompletableFuture<HolderLookup.Provider> lookupProvider)
	{
		super(packOutput, lookupProvider);
	}

	private final Map<Block, Block> strips = new HashMap<>();

	@SuppressWarnings("deprecation")
	@Override
	protected void gather(HolderLookup.Provider provider)
	{
		Map<Block, Float> compostables = Map.of(NCBlocks.glowood_sapling, 0.3F, NCBlocks.glowood_leaves, 0.3F, NCBlocks.glowroots, 0.3F, NCBlocks.glowshroom_stem, 0.65F, NCBlocks.green_glowshroom, 0.65F, NCBlocks.purple_glowshroom, 0.65F, NCBlocks.green_glowshroom_block, 0.85F, NCBlocks.purple_glowshroom_block, 0.85F);

		var compBuilder = this.builder(NeoForgeDataMaps.COMPOSTABLES);

		for (var comp : compostables.entrySet())
			compBuilder.add(comp.getKey().asItem().builtInRegistryHolder().getKey(), new Compostable(comp.getValue()), false);

		axeStripping(NCBlocks.glowood_log, NCBlocks.stripped_glowood_log);

		axeStripping(NCBlocks.glowood_wood, NCBlocks.stripped_glowood_wood);
	}

	void axeStripping(Block log, Block stripped)
	{
		this.strips.put(log, stripped);
	}
}