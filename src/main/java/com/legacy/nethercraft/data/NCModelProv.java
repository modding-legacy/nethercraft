package com.legacy.nethercraft.data;

import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.client.render.NCRenderRefs;
import com.legacy.nethercraft.registry.NCBlocks;
import com.legacy.nethercraft.registry.NCItems;

import net.minecraft.client.data.models.BlockModelGenerators;
import net.minecraft.client.data.models.ItemModelGenerators;
import net.minecraft.client.data.models.ItemModelOutput;
import net.minecraft.client.data.models.ModelProvider;
import net.minecraft.client.data.models.blockstates.BlockStateGenerator;
import net.minecraft.client.data.models.blockstates.Condition;
import net.minecraft.client.data.models.blockstates.MultiPartGenerator;
import net.minecraft.client.data.models.blockstates.MultiVariantGenerator;
import net.minecraft.client.data.models.blockstates.Variant;
import net.minecraft.client.data.models.blockstates.VariantProperties;
import net.minecraft.client.data.models.model.ModelInstance;
import net.minecraft.client.data.models.model.ModelTemplate;
import net.minecraft.client.data.models.model.ModelTemplates;
import net.minecraft.client.data.models.model.TextureMapping;
import net.minecraft.client.data.models.model.TextureSlot;
import net.minecraft.client.data.models.model.TexturedModel;
import net.minecraft.data.BlockFamily;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;

public class NCModelProv extends ModelProvider
{
	public NCModelProv(PackOutput output)
	{
		super(output, Nethercraft.MODID);
	}

	@Override
	public CompletableFuture<?> run(CachedOutput output)
	{
		return super.run(output);
	}

	@Override
	protected void registerModels(BlockModelGenerators blockModels, ItemModelGenerators itemModels)
	{
		var items = new Items(itemModels.itemModelOutput, itemModels.modelOutput);
		items.run();

		var states = new States(blockModels.blockStateOutput, blockModels.itemModelOutput, blockModels.modelOutput);
		states.run();
	}

	public static class Items extends ItemModelGenerators
	{
		public Items(ItemModelOutput output, BiConsumer<ResourceLocation, ModelInstance> models)
		{
			super(output, models);
		}

		@Override
		public void run()
		{
			this.handheldItem(NCItems.glowood_sword);
			this.handheldItem(NCItems.glowood_pickaxe);
			this.handheldItem(NCItems.glowood_axe);
			this.handheldItem(NCItems.glowood_shovel);
			this.handheldItem(NCItems.glowood_hoe);

			this.handheldItem(NCItems.netherrack_sword);
			this.handheldItem(NCItems.netherrack_pickaxe);
			this.handheldItem(NCItems.netherrack_axe);
			this.handheldItem(NCItems.netherrack_shovel);
			this.handheldItem(NCItems.netherrack_hoe);

			this.handheldItem(NCItems.neridium_sword);
			this.handheldItem(NCItems.neridium_pickaxe);
			this.handheldItem(NCItems.neridium_axe);
			this.handheldItem(NCItems.neridium_shovel);
			this.handheldItem(NCItems.neridium_hoe);

			this.handheldItem(NCItems.pyridium_sword);
			this.handheldItem(NCItems.pyridium_pickaxe);
			this.handheldItem(NCItems.pyridium_axe);
			this.handheldItem(NCItems.pyridium_shovel);
			this.handheldItem(NCItems.pyridium_hoe);

			this.handheldItem(NCItems.linium_sword);
			this.handheldItem(NCItems.linium_pickaxe);
			this.handheldItem(NCItems.linium_axe);
			this.handheldItem(NCItems.linium_shovel);
			this.handheldItem(NCItems.linium_hoe);

			this.handheldItem(NCItems.glowood_stick);

			this.basicItem(NCItems.foulite_dust);
			this.basicItem(NCItems.magma_caramel);
			this.basicItem(NCItems.imp_skin);
			this.handheldItem(NCItems.ghast_rod);
			this.basicItem(NCItems.red_feather);
			this.basicItem(NCItems.lava_book);
			this.basicItem(NCItems.lava_paper);
			this.basicItem(NCItems.ghast_bones);
			this.basicItem(NCItems.ghast_marrow);
			this.basicItem(NCItems.neridium_ingot);
			this.basicItem(NCItems.pyridium_ingot);
			this.basicItem(NCItems.linium_ingot);
			this.basicItem(NCItems.w_dust);
			this.basicItem(NCItems.w_obsidian_ingot);

			/*this.basicItem(NCItems.dark_seeds);*/
			this.basicItem(NCItems.dark_wheat);
			this.basicItem(NCItems.devil_bread);
			this.basicItem(NCItems.lava_reeds);
			this.basicItem(NCItems.glow_stew);
			this.basicItem(NCItems.glow_apple);
			this.basicItem(NCItems.glowood_bowl);
			this.basicItem(NCItems.netherrack_arrow);
			this.basicItem(NCItems.neridium_arrow);
			this.basicItem(NCItems.pyridium_arrow);
			this.basicItem(NCItems.linium_arrow);
			this.basicItem(NCItems.neridium_lighter);
			this.basicItem(NCItems.slime_eggs);
			this.basicItem(NCItems.imp_helmet);
			this.basicItem(NCItems.imp_chestplate);
			this.basicItem(NCItems.imp_leggings);
			this.basicItem(NCItems.imp_boots);

			this.basicItem(NCItems.neridium_helmet);
			this.basicItem(NCItems.neridium_chestplate);
			this.basicItem(NCItems.neridium_leggings);
			this.basicItem(NCItems.neridium_boots);

			this.basicItem(NCItems.w_obsidian_helmet);
			this.basicItem(NCItems.w_obsidian_chestplate);
			this.basicItem(NCItems.w_obsidian_leggings);
			this.basicItem(NCItems.w_obsidian_boots);

			this.basicItem(NCItems.neridium_bucket);
			this.basicItem(NCItems.neridium_lava_bucket);

			/*this.basicItem(NCItems.charcoal_torch);
			this.basicItem(NCItems.foulite_torch);*/

			this.basicItem(NCItems.imp_disc);

			// FIXME: Bow strengths
			this.generateBow(NCItems.netherrack_bow);
			this.generateBow(NCItems.neridium_bow);
			this.generateBow(NCItems.pyridium_bow);
			this.generateBow(NCItems.linium_bow);

			this.generateSpawnEgg(NCItems.imp_spawn_egg, 0x1f4d14, 0x8e3801);
			this.generateSpawnEgg(NCItems.dark_zombie_spawn_egg, 0x700c0c, 0x323131);
			this.generateSpawnEgg(NCItems.bloody_zombie_spawn_egg, 0x700c0c, 0x351212);
			this.generateSpawnEgg(NCItems.lava_slime_spawn_egg, 0xa43434, 0xbb5a29);
			this.generateSpawnEgg(NCItems.camouflage_spider_spawn_egg, 0x3f172f, 0x710e20);
			this.generateSpawnEgg(NCItems.tribal_trainee_spawn_egg, 0x787878, 0x710e20);
			this.generateSpawnEgg(NCItems.tribal_warrior_spawn_egg, 0x787878, 0x500e71);
			this.generateSpawnEgg(NCItems.tribal_archer_spawn_egg, 0x787878, 0x7a5b89);
		}

		private void basicItem(Item item)
		{
			this.generateFlatItem(item, ModelTemplates.FLAT_ITEM);
		}

		private void handheldItem(Item item)
		{
			this.generateFlatItem(item, ModelTemplates.FLAT_HANDHELD_ITEM);
		}
	}

	public static class States extends BlockModelGenerators
	{
		public States(Consumer<BlockStateGenerator> blockStateOutput, ItemModelOutput itemModelOutput, BiConsumer<ResourceLocation, ModelInstance> modelOutput)
		{
			super(blockStateOutput, itemModelOutput, modelOutput);
		}

		@Override
		public void run()
		{
			NCBlockFamilies.getFamilies().stream().filter(BlockFamily::shouldGenerateModel).forEach(fam -> this.family(fam.getBaseBlock()).generateFor(fam));

			this.createCropBlock(NCBlocks.dark_wheat, BlockStateProperties.AGE_7, 0, 1, 2, 3, 4, 5, 6, 7);

			this.createCrossBlock(NCBlocks.glowroots, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createCrossBlock(NCBlocks.lava_reeds, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createPlantWithDefaultItem(NCBlocks.green_glowshroom, NCBlocks.potted_green_glowshroom, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createPlantWithDefaultItem(NCBlocks.purple_glowshroom, NCBlocks.potted_purple_glowshroom, BlockModelGenerators.PlantType.NOT_TINTED);

			this.createRotatedVariantBlock(NCBlocks.nether_dirt);
			this.createRotatedVariantBlock(NCBlocks.heat_sand);

			this.createNetherFarmland(NCBlocks.nether_dirt, NCBlocks.nether_farmland);

			// ChestSpecialRenderer.NORMAL_CHEST_TEXTURE
			this.createChest(NCBlocks.glowood_chest, NCBlocks.glowood_planks, NCRenderRefs.GLOWOOD_CHEST_TEXTURE, true);

			this.createMushroomBlock(NCBlocks.green_glowshroom_block);
			this.createMushroomBlock(NCBlocks.purple_glowshroom_block);
			this.createMushroomBlock(NCBlocks.glowshroom_stem);

			this.createFurnace(NCBlocks.netherrack_furnace, TexturedModel.ORIENTABLE_ONLY_TOP);
			this.simpleBlock(NCBlocks.neridium_block);
			this.simpleBlock(NCBlocks.pyridium_block);
			this.simpleBlock(NCBlocks.linium_block);
			this.simpleBlock(NCBlocks.ghast_bomb);

			this.simpleBlock(NCBlocks.foulite_ore);
			this.simpleBlock(NCBlocks.neridium_ore);
			this.simpleBlock(NCBlocks.pyridium_ore);
			this.simpleBlock(NCBlocks.linium_ore);
			this.simpleBlock(NCBlocks.w_ore);

			this.createNormalTorch(NCBlocks.charcoal_torch, NCBlocks.charcoal_wall_torch);
			this.createNormalTorch(NCBlocks.foulite_torch, NCBlocks.foulite_wall_torch);

			this.createGlassBlocks(NCBlocks.heat_glass, NCBlocks.heat_glass_pane);
			this.createGlassBlocks(NCBlocks.soul_glass, NCBlocks.soul_glass_pane);

			this.woodProvider(NCBlocks.glowood_log).logWithHorizontal(NCBlocks.glowood_log).wood(NCBlocks.glowood_wood);
			this.woodProvider(NCBlocks.stripped_glowood_log).logWithHorizontal(NCBlocks.stripped_glowood_log).wood(NCBlocks.stripped_glowood_wood);
			/*this.createHangingSign(NCBlocks.stripped_glowood_log, NCBlocks.glowood_hanging_sign, NCBlocks.glowood_wall_hanging_sign);*/
			this.createPlantWithDefaultItem(NCBlocks.glowood_sapling, NCBlocks.potted_glowood_sapling, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createTrivialBlock(NCBlocks.glowood_leaves, TexturedModel.LEAVES);
			this.bookshelf(NCBlocks.glowood_bookshelf, NCBlocks.glowood_planks);
			this.craftingTable(NCBlocks.glowood_crafting_table, NCBlocks.glowood_planks);
			this.vine(NCBlocks.glowood_ladder);

			this.createSmoothSlab(NCBlocks.smooth_netherrack_slab, NCBlocks.smooth_netherrack);
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public void createPlant(Block flower, Block pot, BlockModelGenerators.PlantType type)
		{
			this.createCrossBlock(flower, type);
			TextureMapping texturemapping = type.getPlantTextureMapping(flower);
			ResourceLocation resourcelocation = type.getCrossPot().extend().renderType(ResourceLocation.parse("cutout")).build().create(pot, texturemapping, this.modelOutput);
			this.blockStateOutput.accept(createSimpleBlock(pot, resourcelocation));
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public void createCrossBlock(Block flower, BlockModelGenerators.PlantType type, TextureMapping mapping)
		{
			ResourceLocation resourcelocation = type.getCross().extend().renderType(ResourceLocation.parse("cutout")).build().create(flower, mapping, this.modelOutput);
			this.blockStateOutput.accept(createSimpleBlock(flower, resourcelocation));
		}

		/*public static MultiVariantGenerator createSimpleBlock(Block block, ResourceLocation model)
		{
			return MultiVariantGenerator.multiVariant(block, Variant.variant().with(VariantProperties.MODEL, model));
		}*/

		public void createSmoothSlab(Block slab, Block parent)
		{
			TextureMapping texturemapping = TextureMapping.cube(parent);
			TextureMapping texturemapping1 = TextureMapping.column(TextureMapping.getBlockTexture(slab, "_side"), texturemapping.get(TextureSlot.TOP));
			ResourceLocation resourcelocation = ModelTemplates.SLAB_BOTTOM.create(slab, texturemapping1, this.modelOutput);
			ResourceLocation resourcelocation1 = ModelTemplates.SLAB_TOP.create(slab, texturemapping1, this.modelOutput);
			ResourceLocation resourcelocation2 = ModelTemplates.CUBE_COLUMN.createWithOverride(slab, "_double", texturemapping1, this.modelOutput);
			this.blockStateOutput.accept(createSlab(slab, resourcelocation, resourcelocation1, resourcelocation2));
			this.blockStateOutput.accept(createSimpleBlock(parent, ModelTemplates.CUBE_ALL.create(parent, texturemapping, this.modelOutput)));
		}

		public void vine(Block b)
		{
			this.createMultifaceBlockStates(b);
			ResourceLocation resourcelocation = this.createFlatItemModelWithBlockTexture(b.asItem(), b);
			this.registerSimpleItemModel(b, resourcelocation);
		}

		public void craftingTable(Block block, Block bottom)
		{
			this.createCraftingTableLike(block, bottom, TextureMapping::craftingTable);
		}

		/*public void ladder(Block block)
		{
			ResourceLocation baseModel = ModelTemplates.CUBE_ALL.create(block, TextureMapping.cube(block), this.modelOutput);
			ResourceLocation matureModel = this.createSuffixedVariant(block, "_mature", ModelTemplates.CUBE_ALL, TextureMapping::cube);
			ResourceLocation goldenModel = this.createSuffixedVariant(block, "_golden", ModelTemplates.CUBE_ALL, TextureMapping::cube);
		
			this.registerSimpleItemModel(block, matureModel);
		
			this.blockStateOutput.accept(MultiVariantGenerator.multiVariant(block).with(PropertyDispatch.properties(AppleLeavesBlock.MATURE, AppleLeavesBlock.GOLDEN).generate((mature, golden) -> Variant.variant().with(VariantProperties.MODEL, mature ? (golden ? goldenModel : matureModel) : baseModel))));
		
		
			var texture = this.blockTexture(block);
		
			var vineModel = this.models().withExistingParent(name(block), "block/ladder").texture("texture", texture).texture("particle", "#texture").renderType("cutout");
			var ladderStateBuilder = this.getMultipartBuilder(block);
		
			for (Direction dir : DirectionUtil.HORIZONTAL)
			{
				int y = (dir.getOpposite().get2DDataValue() * 90);
				ladderStateBuilder.part().modelFile(vineModel).uvLock(true).rotationX(0).rotationY(y % 360).addModel().condition(LadderBlock.FACING, dir).end();
			}
		
			// this.createNonTemplateHorizontalBlock(block);
			this.registerSimpleFlatItemModel(block);
		}*/

		public void createNetherFarmland(Block dirt, Block farmland)
		{
			TextureMapping texturemapping = new TextureMapping().put(TextureSlot.DIRT, TextureMapping.getBlockTexture(dirt)).put(TextureSlot.TOP, TextureMapping.getBlockTexture(farmland));
			TextureMapping texturemapping1 = new TextureMapping().put(TextureSlot.DIRT, TextureMapping.getBlockTexture(dirt)).put(TextureSlot.TOP, TextureMapping.getBlockTexture(farmland, "_moist"));
			ResourceLocation resourcelocation = ModelTemplates.FARMLAND.create(farmland, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation1 = ModelTemplates.FARMLAND.create(TextureMapping.getBlockTexture(farmland, "_moist"), texturemapping1, this.modelOutput);
			this.blockStateOutput.accept(MultiVariantGenerator.multiVariant(farmland).with(createEmptyOrFullDispatch(BlockStateProperties.MOISTURE, 7, resourcelocation1, resourcelocation)));
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public void createDoor(Block door)
		{
			String renderType = "cutout";
			TextureMapping texturemapping = TextureMapping.door(door);
			ResourceLocation resourcelocation = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_LEFT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation1 = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_LEFT_OPEN).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation2 = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_RIGHT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation3 = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_RIGHT_OPEN).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation4 = extendWithType(renderType, ModelTemplates.DOOR_TOP_LEFT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation5 = extendWithType(renderType, ModelTemplates.DOOR_TOP_LEFT_OPEN).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation6 = extendWithType(renderType, ModelTemplates.DOOR_TOP_RIGHT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation7 = extendWithType(renderType, ModelTemplates.DOOR_TOP_RIGHT_OPEN).create(door, texturemapping, this.modelOutput);
			this.registerSimpleFlatItemModel(door.asItem());
			this.blockStateOutput.accept(createDoor(door, resourcelocation, resourcelocation1, resourcelocation2, resourcelocation3, resourcelocation4, resourcelocation5, resourcelocation6, resourcelocation7));
		}

		@Override
		public void createGlassBlocks(Block block, Block pane)
		{
			this.createTrivialCube(block);
			
			String renderType = "cutout";
			TextureMapping texturemapping = TextureMapping.pane(block, pane);
			ResourceLocation resourcelocation = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_POST).create(pane, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation1 = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_SIDE).create(pane, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation2 = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_SIDE_ALT).create(pane, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation3 = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_NOSIDE).create(pane, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation4 = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_NOSIDE_ALT).create(pane, texturemapping, this.modelOutput);
			Item item = pane.asItem();
			this.registerSimpleItemModel(item, this.createFlatItemModelWithBlockTexture(item, block));
			this.blockStateOutput.accept(MultiPartGenerator.multiPart(pane).with(Variant.variant().with(VariantProperties.MODEL, resourcelocation)).with(Condition.condition().term(BlockStateProperties.NORTH, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation1)).with(Condition.condition().term(BlockStateProperties.EAST, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation1).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R90)).with(Condition.condition().term(BlockStateProperties.SOUTH, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation2)).with(Condition.condition().term(BlockStateProperties.WEST, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation2).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R90)).with(Condition.condition().term(BlockStateProperties.NORTH, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation3)).with(Condition.condition().term(BlockStateProperties.EAST, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation4)).with(Condition.condition().term(BlockStateProperties.SOUTH, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation4).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R90)).with(Condition.condition().term(BlockStateProperties.WEST, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation3).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R270)));
		}

		private static ModelTemplate extendWithType(String renderType, ModelTemplate template)
		{
			return template.extend().renderType(ResourceLocation.parse(renderType)).build();
		}

		public void bookshelf(Block block, Block plank)
		{
			TextureMapping texturemapping = TextureMapping.column(TextureMapping.getBlockTexture(block), TextureMapping.getBlockTexture(plank));
			ResourceLocation resourcelocation = ModelTemplates.CUBE_COLUMN.create(block, texturemapping, this.modelOutput);
			this.blockStateOutput.accept(createSimpleBlock(block, resourcelocation));
		}

		public void simpleBlock(Block block)
		{
			this.simpleBlock(block, (String) null);
		}

		public void simpleBlock(Block block, @Nullable String renderType)
		{
			this.createTrivialBlock(block, renderType != null ? TexturedModel.createDefault(TextureMapping::cube, ModelTemplates.CUBE_ALL.extend().renderType(ResourceLocation.parse(renderType)).build()) : TexturedModel.CUBE);
		}
	}
}
