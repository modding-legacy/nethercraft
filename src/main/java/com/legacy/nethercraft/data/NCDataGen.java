package com.legacy.nethercraft.data;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.data.NCTagProv.BlockProv;
import com.legacy.nethercraft.registry.NCBiomeModifiers;
import com.legacy.nethercraft.registry.NCBiomes;
import com.legacy.nethercraft.registry.NCFeatures;

import net.minecraft.DetectedVersion;
import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.RegistrySetBuilder;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.data.metadata.PackMetadataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.metadata.pack.PackMetadataSection;
import net.minecraft.util.InclusiveRange;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.common.EventBusSubscriber.Bus;
import net.neoforged.neoforge.common.data.DatapackBuiltinEntriesProvider;
import net.neoforged.neoforge.data.event.GatherDataEvent;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

@EventBusSubscriber(modid = Nethercraft.MODID, bus = Bus.MOD)
public class NCDataGen
{
	private static final RegistrySetBuilder BUILDER = new RegistrySetBuilder().add(Registries.CONFIGURED_FEATURE, NCFeatures.Configured::bootstrap).add(Registries.PLACED_FEATURE, NCFeatures.Placements::bootstrap).add(NeoForgeRegistries.Keys.BIOME_MODIFIERS, NCBiomeModifiers::bootstrap).add(Registries.BIOME, NCBiomes::bootstrap);

	@SubscribeEvent
	public static void gatherData(GatherDataEvent.Client event) throws InterruptedException, ExecutionException
	{
		DataGenerator gen = event.getGenerator();
		CompletableFuture<Provider> lookup = event.getLookupProvider();
		PackOutput output = gen.getPackOutput();
		boolean run = true;

		gen.addProvider(run, new NCLootProv(gen, lookup));

		BlockProv prov = new NCTagProv.BlockProv(gen, lookup);

		gen.addProvider(run, prov);
		gen.addProvider(run, new NCTagProv.ItemProv(gen, prov.contentsGetter(), lookup));
		gen.addProvider(run, new NCTagProv.BiomeProv(gen, lookup));
		gen.addProvider(run, new NCRecipeProv.Runner(output, lookup));
		gen.addProvider(run, new NCDataMapProv(output, lookup));

		gen.addProvider(run, new DatapackBuiltinEntriesProvider(output, event.getLookupProvider(), BUILDER, Set.of(Nethercraft.MODID)));

		gen.addProvider(run, new NCModelProv(output));

		gen.addProvider(run, packMcmeta(output, "Nethercraft resources"));

		PackOutput legacyPackOutput = gen.getPackOutput("assets/nethercraft/legacy_pack");
		gen.addProvider(run, packMcmeta(legacyPackOutput, "The classic look of Nethercraft from beta"));
	}

	private static final DataProvider packMcmeta(PackOutput output, String description)
	{
		int serverVersion = DetectedVersion.BUILT_IN.getPackVersion(PackType.SERVER_DATA);
		return NestedDataProvider.of(new PackMetadataGenerator(output).add(PackMetadataSection.TYPE, new PackMetadataSection(Component.literal(description), serverVersion, Optional.of(new InclusiveRange<>(0, Integer.MAX_VALUE)))), description);
	}

	private record NestedDataProvider<D extends DataProvider>(D provider, String namePrefix) implements DataProvider
	{

		public static <D extends DataProvider> NestedDataProvider<D> of(D provider, String namePrefix)
		{
			return new NestedDataProvider<>(provider, namePrefix);
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cachedOutput)
		{
			return this.provider.run(cachedOutput);
		}

		@Override
		public String getName()
		{
			return this.namePrefix + "/" + this.provider.getName();
		}
	}
}