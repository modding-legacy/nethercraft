package com.legacy.nethercraft.data;

import com.legacy.nethercraft.Nethercraft;

import net.minecraft.core.registries.Registries;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;

public class NCTags
{
	public static interface Blocks
	{
		TagKey<Block> PLANKS = tag("planks");

		TagKey<Block> GLOWOOD_LOGS = tag("glowood_logs");

		TagKey<Block> GLOWSHROOM_PLANTABLE_ON = tag("glowshroom_plantable_on");
		TagKey<Block> LAVA_REEDS_PLANTABLE_ON = tag("lava_reeds_plantable_on");

		private static TagKey<Block> tag(String name)
		{
			return BlockTags.create(Nethercraft.locate(name));
		}
	}

	public static interface Items
	{
		TagKey<Item> PLANKS = tag("planks");

		TagKey<Item> GLOWOOD_LOGS = tag("glowood_logs");

		private static TagKey<Item> tag(String name)
		{
			return ItemTags.create(Nethercraft.locate(name));
		}
	}

	public static interface Biomes
	{
		TagKey<Biome> HAS_FOULITE_ORE = tag("has_foulite_ore");
		TagKey<Biome> HAS_NERIDIUM_ORE = tag("has_neridium_ore");
		TagKey<Biome> HAS_PYRIDIUM_ORE = tag("has_pyridium_ore");
		TagKey<Biome> HAS_LINIUM_ORE = tag("has_linium_ore");
		TagKey<Biome> HAS_W_ORE = tag("has_w_ore");

		private static TagKey<Biome> tag(String name)
		{
			return TagKey.create(Registries.BIOME, Nethercraft.locate(name));
		}
	}
}
