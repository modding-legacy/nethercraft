package com.legacy.nethercraft.data;

import java.util.concurrent.CompletableFuture;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.registry.NCBlocks;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.BlockFamily;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.packs.VanillaRecipeProvider;
import net.minecraft.world.flag.FeatureFlagSet;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Block;

public class NCRecipeProv extends VanillaRecipeProvider
{
	public static class Runner extends RecipeProvider.Runner
	{
		public Runner(PackOutput output, CompletableFuture<HolderLookup.Provider> registries)
		{
			super(output, registries);
		}

		@Override
		protected RecipeProvider createRecipeProvider(HolderLookup.Provider lookup, RecipeOutput output)
		{
			return new NCRecipeProv(lookup, output);
		}

		@Override
		public String getName()
		{
			return Nethercraft.MODID + " Recipe Gen";
		}
	}

	private HolderGetter<Item> items;

	private NCRecipeProv(HolderLookup.Provider lookupProvider, RecipeOutput output)
	{
		super(lookupProvider, output);
		this.items = lookupProvider.lookupOrThrow(Registries.ITEM);
	}

	@Override
	protected void buildRecipes()
	{
		generateForEnabledBlockFamilies(FeatureFlagSet.of(FeatureFlags.VANILLA));

		bookshelf(NCBlocks.glowood_bookshelf, NCBlocks.glowood_planks);
		planksFromLogs(NCBlocks.glowood_planks, NCTags.Items.GLOWOOD_LOGS, 4);
		woodFromLogs(NCBlocks.glowood_wood, NCBlocks.glowood_log);
		woodFromLogs(NCBlocks.stripped_glowood_wood, NCBlocks.stripped_glowood_log);
		twoByTwoPacker(RecipeCategory.DECORATIONS, NCBlocks.glowood_crafting_table, NCBlocks.glowood_planks);
		/*hangingSign(NCItems.glowood_hanging_sign, NCBlocks.stripped_glowood_log);*/

		// backup chest recipe, since Quark deletes vanillas
		/*ShapedRecipeBuilder.shaped(this.items, RecipeCategory.DECORATIONS, Blocks.CHEST).define('#', NCTags.Items.PLANKS).pattern("###").pattern("# #").pattern("###").showNotification(false).unlockedBy("impossible", CriteriaTriggers.IMPOSSIBLE.createCriterion(new ImpossibleTrigger.TriggerInstance())).save(this.output, ResourceKey.create(Registries.RECIPE, PremiumWoodMod.locate("chest_backup")));*/
	}

	private void bookshelf(Block bookshelf, Block planks)
	{
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.BUILDING_BLOCKS, bookshelf).define('#', planks).define('X', Items.BOOK).pattern("###").pattern("XXX").pattern("###").unlockedBy("has_book", has(Items.BOOK)).save(this.output);
	}

	@Override
	protected void generateForEnabledBlockFamilies(FeatureFlagSet flags)
	{
		/*protected void generateForEnabledBlockFamilies(FeatureFlagSet p_251836_) {
		    BlockFamilies.getAllFamilies().filter(BlockFamily::shouldGenerateRecipe).forEach(p_359455_ -> this.generateRecipes(p_359455_, p_251836_));
		}*/
		NCBlockFamilies.getFamilies().stream().filter(BlockFamily::shouldGenerateRecipe).forEach((fam) -> generateRecipes(fam, flags));
	}
}
