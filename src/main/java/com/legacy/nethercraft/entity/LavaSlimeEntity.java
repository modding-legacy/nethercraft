package com.legacy.nethercraft.entity;

import javax.annotation.Nullable;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.entity.EntitySpawnReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.monster.Slime;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;

public class LavaSlimeEntity extends Slime
{
	public static final EntityDataAccessor<Integer> SLIME_TYPE = SynchedEntityData.<Integer>defineId(LavaSlimeEntity.class, EntityDataSerializers.INT);

	public LavaSlimeEntity(EntityType<? extends LavaSlimeEntity> type, Level worldIn)
	{
		super(type, worldIn);
	}

	// FIXME: How do slimes do this normally?
	public static AttributeSupplier.Builder createAttributes()
	{
		return Mob.createMobAttributes().add(Attributes.ATTACK_DAMAGE, 4.0D);
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor level, DifficultyInstance difficulty, EntitySpawnReason reason, @Nullable SpawnGroupData data)
	{
		this.entityData.set(SLIME_TYPE, this.random.nextInt(2));
		return super.finalizeSpawn(level, difficulty, reason, data);
	}

	@Override
	protected void defineSynchedData(SynchedEntityData.Builder builder)
	{
		super.defineSynchedData(builder);
		builder.define(SLIME_TYPE, 0);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putInt("slime_type", this.entityData.get(SLIME_TYPE));
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);
		this.entityData.set(SLIME_TYPE, compound.getInt("slime_type"));
	}
	
	public int getSlimeVariant()
	{
		return this.getEntityData().get(SLIME_TYPE);
	}
}