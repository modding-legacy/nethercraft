package com.legacy.nethercraft.entity;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.level.Level;

public class BloodyZombieEntity extends DarkZombieEntity
{
	public BloodyZombieEntity(EntityType<? extends BloodyZombieEntity> type, Level world)
	{
		super(type, world);
	}

	public static AttributeSupplier.Builder createAttributes()
	{
		return DarkZombieEntity.createDarkZombieAttributes().add(Attributes.MAX_HEALTH, 35.0F).add(Attributes.MOVEMENT_SPEED, 0.28F);
	}
}