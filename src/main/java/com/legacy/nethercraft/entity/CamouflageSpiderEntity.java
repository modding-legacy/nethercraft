package com.legacy.nethercraft.entity;

import javax.annotation.Nullable;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntitySpawnReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.monster.Spider;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;

public class CamouflageSpiderEntity extends Spider
{
	public static final EntityDataAccessor<Integer> SPIDER_TYPE = SynchedEntityData.<Integer>defineId(CamouflageSpiderEntity.class, EntityDataSerializers.INT);

	public CamouflageSpiderEntity(EntityType<? extends CamouflageSpiderEntity> type, Level worldIn)
	{
		super(type, worldIn);
		/*this.dataManager.set(SPIDER_TYPE, this.rand.nextInt(2));*/
	}

	@Override
	protected void defineSynchedData(SynchedEntityData.Builder builder)
	{
		super.defineSynchedData(builder);
		builder.define(SPIDER_TYPE, 0);
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor level, DifficultyInstance difficulty, EntitySpawnReason reason, @Nullable SpawnGroupData data)
	{
		int type = this.random.nextInt(2);
		this.entityData.set(SPIDER_TYPE, type);

		// FIXME: Change how this is done. Maybe match how Zombies do it
		if (type == 1)
			this.getAttribute(Attributes.MAX_HEALTH).setBaseValue(40.0D);

		return super.finalizeSpawn(level, difficulty, reason, data);
	}

	public static AttributeSupplier.Builder createAttributes()
	{
		return Spider.createAttributes().add(Attributes.MAX_HEALTH, 35.0D).add(Attributes.MOVEMENT_SPEED, 0.3F);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putInt("spider_type", this.entityData.get(SPIDER_TYPE));
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);
		this.entityData.set(SPIDER_TYPE, compound.getInt("spider_type"));
	}

	public int getSpiderType()
	{
		return this.entityData.get(SPIDER_TYPE);
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		this.playMaterialSound();
		return SoundEvents.SPIDER_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		this.playMaterialSound();
		return SoundEvents.SPIDER_DEATH;
	}

	private void playMaterialSound()
	{
		if (this.getSpiderType() == 0)
		{
			this.level().playSound(null, this.getX(), this.getY(), this.getZ(), SoundEvents.GRAVEL_BREAK, this.getSoundSource(), 1.0F, 1.0F);
		}
		else if (this.getSpiderType() == 1)
		{
			this.level().playSound(null, this.getX(), this.getY(), this.getZ(), SoundEvents.STONE_BREAK, this.getSoundSource(), 1.0F, 0.7F);
		}
	}
}