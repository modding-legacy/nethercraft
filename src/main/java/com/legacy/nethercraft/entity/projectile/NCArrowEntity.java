package com.legacy.nethercraft.entity.projectile;

import javax.annotation.Nullable;

import com.legacy.nethercraft.registry.NCEntityTypes;
import com.legacy.nethercraft.registry.NCItems;

import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;

public abstract class NCArrowEntity extends AbstractArrow
{
	public NCArrowEntity(EntityType<? extends NCArrowEntity> entity, Level level)
	{
		super(entity, level);
		this.setNoGravity(true);
	}

	public NCArrowEntity(EntityType<? extends AbstractArrow> entityType, LivingEntity shooter, Level level, ItemStack pickupStack, @Nullable ItemStack firedFromWeapon)
	{
		super(entityType, shooter, level, pickupStack, firedFromWeapon);
		this.setNoGravity(true);
	}

	public NCArrowEntity(EntityType<? extends AbstractArrow> entityType, Level level, double x, double y, double z, ItemStack pickupStack, @Nullable ItemStack firedFromWeapon)
	{
		super(entityType, x, y, z, level, pickupStack, firedFromWeapon);
		this.setNoGravity(true);
	}

	@Override
	public void tick()
	{
		super.tick();

		if (!this.level().isClientSide())
		{
			if (this.getArrowGravity() != 0)
			{
				this.setDeltaMovement(this.getDeltaMovement().scale(0.99F));

				if (!this.isNoPhysics())
				{
					Vec3 vec3d3 = this.getDeltaMovement();
					this.setDeltaMovement(vec3d3.x, vec3d3.y - this.getArrowGravity(), vec3d3.z);
				}
			}
		}
		else if (!this.isInGround())
			this.level().addParticle(ParticleTypes.SMOKE, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D, 0.0D);
	}

	@Override
	protected void doPostHurtEffects(LivingEntity living)
	{
		super.doPostHurtEffects(living);
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
	}

	abstract double getArrowGravity();

	public static class Netherrack extends NCArrowEntity
	{
		public Netherrack(EntityType<? extends NCArrowEntity> entity, Level level)
		{
			super(entity, level);
			this.setNoGravity(true);
		}

		public Netherrack(Level level, LivingEntity shooter, ItemStack pickupStack, @Nullable ItemStack firedFromWeapon)
		{
			super(NCEntityTypes.NETHERRACK_ARROW, shooter, level, pickupStack, firedFromWeapon);
		}

		public Netherrack(Level level, double x, double y, double z, ItemStack pickupStack, @Nullable ItemStack firedFromWeapon)
		{
			super(NCEntityTypes.NETHERRACK_ARROW, level, x, y, z, pickupStack, firedFromWeapon);
		}

		@Override
		protected ItemStack getDefaultPickupItem()
		{
			return NCItems.netherrack_arrow.getDefaultInstance();
		}

		@Override
		double getArrowGravity()
		{
			return 0.03D;
		}
	}

	public static class Neridium extends NCArrowEntity
	{
		public Neridium(EntityType<? extends NCArrowEntity> entity, Level level)
		{
			super(entity, level);
			this.setNoGravity(true);
		}

		public Neridium(Level level, LivingEntity shooter, ItemStack pickupStack, @Nullable ItemStack firedFromWeapon)
		{
			super(NCEntityTypes.NERIDIUM_ARROW, shooter, level, pickupStack, firedFromWeapon);
			this.setNoGravity(true);
		}

		public Neridium(Level level, double x, double y, double z, ItemStack pickupStack, @Nullable ItemStack firedFromWeapon)
		{
			super(NCEntityTypes.NERIDIUM_ARROW, level, x, y, z, pickupStack, firedFromWeapon);
			this.setNoGravity(true);
		}

		@Override
		protected ItemStack getDefaultPickupItem()
		{
			return NCItems.neridium_arrow.getDefaultInstance();
		}

		@Override
		double getArrowGravity()
		{
			return 0.02D;
		}
	}

	public static class Pyridium extends NCArrowEntity
	{
		public Pyridium(EntityType<? extends NCArrowEntity> entity, Level level)
		{
			super(entity, level);
			this.setNoGravity(true);
		}

		public Pyridium(Level level, LivingEntity shooter, ItemStack pickupStack, @Nullable ItemStack firedFromWeapon)
		{
			super(NCEntityTypes.PYRIDIUM_ARROW, shooter, level, pickupStack, firedFromWeapon);
			this.setNoGravity(true);
		}

		public Pyridium(Level level, double x, double y, double z, ItemStack pickupStack, @Nullable ItemStack firedFromWeapon)
		{
			super(NCEntityTypes.PYRIDIUM_ARROW, level, x, y, z, pickupStack, firedFromWeapon);
			this.setNoGravity(true);
		}

		@Override
		protected ItemStack getDefaultPickupItem()
		{
			return NCItems.pyridium_arrow.getDefaultInstance();
		}

		@Override
		double getArrowGravity()
		{
			return 0.01D;
		}
	}

	public static class Linium extends NCArrowEntity
	{
		public Linium(EntityType<? extends NCArrowEntity> entity, Level level)
		{
			super(entity, level);
			this.setNoGravity(true);
		}

		public Linium(Level level, LivingEntity shooter, ItemStack pickupStack, @Nullable ItemStack firedFromWeapon)
		{
			super(NCEntityTypes.LINIUM_ARROW, shooter, level, pickupStack, firedFromWeapon);
			this.setNoGravity(true);
		}

		public Linium(Level level, double x, double y, double z, ItemStack pickupStack, @Nullable ItemStack firedFromWeapon)
		{
			super(NCEntityTypes.LINIUM_ARROW, level, x, y, z, pickupStack, firedFromWeapon);
			this.setNoGravity(true);
		}

		@Override
		protected ItemStack getDefaultPickupItem()
		{
			return NCItems.linium_arrow.getDefaultInstance();
		}

		@Override
		double getArrowGravity()
		{
			return 0.0D;
		}
	}
}