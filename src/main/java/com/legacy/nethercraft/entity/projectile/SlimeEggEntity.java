package com.legacy.nethercraft.entity.projectile;

import com.legacy.nethercraft.entity.LavaSlimeEntity;
import com.legacy.nethercraft.registry.NCEntityTypes;
import com.legacy.nethercraft.registry.NCItems;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ItemParticleOption;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntitySpawnReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.monster.Blaze;
import net.minecraft.world.entity.projectile.ThrowableItemProjectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.BaseFireBlock;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class SlimeEggEntity extends ThrowableItemProjectile
{
	public SlimeEggEntity(EntityType<? extends SlimeEggEntity> entityType, Level level)
	{
		super(entityType, level);
	}

	public SlimeEggEntity(Level level, LivingEntity shooter, ItemStack thrownStack)
	{
		super(NCEntityTypes.SLIME_EGGS, shooter, level, thrownStack);
	}

	public SlimeEggEntity(Level level, double x, double y, double z, ItemStack thrownStack)
	{
		super(NCEntityTypes.SLIME_EGGS, x, y, z, level, thrownStack);
	}

	@Override
	protected Item getDefaultItem()
	{
		return NCItems.slime_eggs;
	}

	@OnlyIn(Dist.CLIENT)
	private ParticleOptions getParticle()
	{
		return new ItemParticleOption(ParticleTypes.ITEM, new ItemStack(NCItems.slime_eggs));
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void handleEntityEvent(byte id)
	{
		if (id == 3)
		{
			ParticleOptions iparticledata = this.getParticle();
			for (int i = 0; i < 8; ++i)
			{
				this.level().addParticle(iparticledata, this.blockPosition().getX(), this.blockPosition().getY(), this.blockPosition().getZ(), 0.0D, 0.0D, 0.0D);
			}
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onHit(HitResult result)
	{
		if (result.getType() == HitResult.Type.ENTITY)
		{
			Entity entity = ((EntityHitResult) result).getEntity();
			int i = entity instanceof Blaze ? 3 : 0;
			entity.hurt(this.damageSources().thrown(this, this.getOwner()), (float) i);
		}

		if (result.getType() == HitResult.Type.BLOCK && !this.level().isClientSide && this.level() instanceof ServerLevelAccessor)
		{
			if (this.random.nextInt(6) == 0)
			{
				int size = 1;

				if (this.random.nextInt(6) == 0)
				{
					size = 3;
				}

				for (int slime = 0; slime < size; ++slime)
				{
					LavaSlimeEntity netherSlime = new LavaSlimeEntity(NCEntityTypes.LAVA_SLIME, this.level());
					netherSlime.finalizeSpawn((ServerLevelAccessor) this.level(), this.level().getCurrentDifficultyAt(this.blockPosition()), EntitySpawnReason.MOB_SUMMONED, (SpawnGroupData) null);
					netherSlime.absMoveTo(this.blockPosition().getX(), this.blockPosition().getY(), this.blockPosition().getZ(), this.getYRot(), this.getXRot());
					this.level().addFreshEntity(netherSlime);
				}
			}

			int x = Mth.floor(result.getLocation().x);
			int y = Mth.floor(result.getLocation().y);
			int z = Mth.floor(result.getLocation().z);
			BlockPos pos = new BlockPos(x, y, z);

			if (this.level().isEmptyBlock(pos) && BaseFireBlock.canBePlacedAt(this.level(), pos, Direction.DOWN))
			{
				this.level().setBlockAndUpdate(pos, BaseFireBlock.getState(this.level(), pos));
			}
			else if (this.level().isEmptyBlock(pos) && BaseFireBlock.canBePlacedAt(this.level(), pos.above(), Direction.DOWN))
			{
				this.level().setBlockAndUpdate(pos.above(), BaseFireBlock.getState(this.level(), pos.above()));
			}

			this.level().addParticle(ParticleTypes.SMOKE, this.blockPosition().getX(), this.blockPosition().getY(), this.blockPosition().getY(), 0.0D, 0.0D, 0.0D);

			this.discard();
		}

		if (!this.level().isClientSide())
		{
			this.level().broadcastEntityEvent(this, (byte) 3);
			this.discard();
		}
	}
}