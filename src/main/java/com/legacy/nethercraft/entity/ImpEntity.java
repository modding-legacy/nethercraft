package com.legacy.nethercraft.entity;

import java.util.ArrayList;

import javax.annotation.Nullable;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.ConversionParams;
import net.minecraft.world.entity.ConversionType;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntitySpawnReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.PanicGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.scores.PlayerTeam;
import net.neoforged.neoforge.event.EventHooks;

public class ImpEntity extends PathfinderMob
{
	public static final EntityDataAccessor<Integer> IMP_TYPE = SynchedEntityData.<Integer>defineId(ImpEntity.class, EntityDataSerializers.INT);

	public ImpEntity(EntityType<? extends ImpEntity> type, Level worldIn)
	{
		super(type, worldIn);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(1, new PanicGoal(this, 1.25D));
		this.goalSelector.addGoal(6, new WaterAvoidingRandomStrollGoal(this, 1.0D));
		this.goalSelector.addGoal(7, new LookAtPlayerGoal(this, Player.class, 6.0F));
		this.goalSelector.addGoal(8, new RandomLookAroundGoal(this));
	}

	@SuppressWarnings("deprecation")
	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor level, DifficultyInstance difficulty, EntitySpawnReason reason, @Nullable SpawnGroupData data)
	{
		this.setImpType(this.random.nextInt(3));
		return super.finalizeSpawn(level, difficulty, reason, data);
	}

	public static AttributeSupplier.Builder createAttributes()
	{
		return Monster.createMonsterAttributes().add(Attributes.MAX_HEALTH, 30.0D).add(Attributes.MOVEMENT_SPEED, 0.25F);
	}

	@Override
	protected void defineSynchedData(SynchedEntityData.Builder builder)
	{
		super.defineSynchedData(builder);
		builder.define(IMP_TYPE, 0);
	}

	@Override
	public EntityDimensions getDefaultDimensions(Pose pose)
	{
		return super.getDefaultDimensions(pose).scale(this.getImpScale());
	}

	private float getImpScale()
	{
		return this.getImpType() == 2 ? 1.45F : this.getImpType() == 0 ? 0.8F : 1.2F;
	}

	@SuppressWarnings("unchecked")
	@Override
	public EntityType<? extends ImpEntity> getType()
	{
		return (EntityType<? extends ImpEntity>) super.getType();
	}

	@SuppressWarnings("resource")
	@Override
	public void remove(Entity.RemovalReason reason)
	{
		int i = this.getImpType();
		if (!this.level().isClientSide && this.isDeadOrDying())
		{
			if (i > 0)
			{

				float f = this.getDimensions(this.getPose()).width();
				float f1 = f / 2.0F;
				int j = i - 1;
				int k = 2 + this.random.nextInt(3);
				PlayerTeam playerteam = this.getTeam();

				var children = new ArrayList<Mob>();

				this.preventConversionSpawns = true;
				for (int l = 0; l < k; l++)
				{
					float f2 = ((float) (l % 2) - 0.5F) * f1;
					float f3 = ((float) (l / 2) - 0.5F) * f1;
					ImpEntity imp = this.convertTo(this.getType(), new ConversionParams(ConversionType.SPLIT_ON_DEATH, false, false, playerteam), EntitySpawnReason.TRIGGERED, splitee ->
					{
						splitee.setImpType(j);
						splitee.moveTo(this.getX() + (double) f2, this.getY(), this.getZ() + (double) f3, this.random.nextFloat() * 360.0F, 0.0F);
					});
					children.add(imp);
				}

				this.preventConversionSpawns = false;

				if (!EventHooks.onMobSplit(this, children).isCanceled())
					children.forEach(this.level()::addFreshEntity);
			}
			else
				this.level().explode(this, this.getX(), this.getY(), this.getZ(), 1.8F, false, Level.ExplosionInteraction.NONE);

		}

		super.remove(reason);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putInt("impType", this.getImpType());
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);
		this.setImpType(compound.getInt("impType"));
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return SoundEvents.PIG_AMBIENT;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource source)
	{
		return SoundEvents.PIG_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return SoundEvents.PIG_DEATH;
	}

	public void setImpType(int type)
	{
		this.refreshDimensions();
		this.entityData.set(IMP_TYPE, type);
	}

	public int getImpType()
	{
		return this.entityData.get(IMP_TYPE);
	}

	@Override
	public void onSyncedDataUpdated(EntityDataAccessor<?> key)
	{
		if (IMP_TYPE.equals(key))
		{
			this.refreshDimensions();
			this.setYRot(this.yHeadRot);
			this.yBodyRot = this.yHeadRot;

			if (this.isInWater() && this.random.nextInt(20) == 0)
				this.doWaterSplashEffect();
		}

		super.onSyncedDataUpdated(key);
	}

	/*protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return 0.825F * sizeIn.height;
	}*/
}