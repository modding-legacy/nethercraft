package com.legacy.nethercraft.entity.tribal;

import javax.annotation.Nullable;

import com.legacy.nethercraft.entity.goal.TribalRangedBowAttackGoal;
import com.legacy.nethercraft.registry.NCItems;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.entity.EntitySpawnReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.monster.RangedAttackMob;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.entity.projectile.ProjectileUtil;
import net.minecraft.world.item.BowItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ProjectileWeaponItem;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;

public class TribalArcherEntity extends AbstractTribalEntity implements RangedAttackMob
{
	public static final EntityDataAccessor<Integer> WARRIOR_TYPE = SynchedEntityData.<Integer>defineId(TribalArcherEntity.class, EntityDataSerializers.INT);

	public TribalArcherEntity(EntityType<? extends TribalArcherEntity> type, Level world)
	{
		super(type, world);
	}

	@Override
	protected void registerGoals()
	{
		super.registerGoals();
		this.goalSelector.addGoal(2, new TribalRangedBowAttackGoal<>(this, 1.0D, 20, 15.0F));
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor level, DifficultyInstance difficulty, EntitySpawnReason reason, @Nullable SpawnGroupData data)
	{
		this.setWarriorType(this.random.nextInt(2));
		return super.finalizeSpawn(level, difficulty, reason, data);
	}

	public static AttributeSupplier.Builder createAttributes()
	{
		return AbstractTribalEntity.createTribalAttributes().add(Attributes.MAX_HEALTH, 25.0F);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putInt("WarriorType", this.getWarriorType());
	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);
		this.setWarriorType(compound.getInt("WarriorType"));
	}

	@Override
	protected void defineSynchedData(SynchedEntityData.Builder builder)
	{
		super.defineSynchedData(builder);
		builder.define(WARRIOR_TYPE, 0);
	}

	@Override
	protected void populateDefaultEquipmentSlots(RandomSource rand, DifficultyInstance difficulty)
	{
		this.setItemSlot(EquipmentSlot.MAINHAND, new ItemStack(NCItems.neridium_bow));
	}

	public void setWarriorType(int type)
	{
		this.entityData.set(WARRIOR_TYPE, type);
	}

	public int getWarriorType()
	{
		return this.entityData.get(WARRIOR_TYPE);
	}

	@Override
	public void performRangedAttack(LivingEntity target, float distanceFactor)
	{
		ItemStack weapon = this.getItemInHand(ProjectileUtil.getWeaponHoldingHand(this, item -> item instanceof BowItem));

		// FIXME: Make sure there's not a better way to do this
		ItemStack itemstack = new ItemStack(NCItems.netherrack_arrow);
		AbstractArrow abstractarrowentity = ProjectileUtil.getMobArrow(this, itemstack, distanceFactor, weapon);

		if (this.getMainHandItem().getItem() instanceof ProjectileWeaponItem projectileWeapon)
			abstractarrowentity = projectileWeapon.customArrow(abstractarrowentity, itemstack, weapon);

		double d0 = target.getX() - this.getX();
		double d1 = target.getBoundingBox().minY + (double) (target.getBbHeight() / 3.0F) - abstractarrowentity.getY();
		double d2 = target.getZ() - this.getZ();
		double d3 = Mth.sqrt((float) (d0 * d0 + d2 * d2));
		abstractarrowentity.shoot(d0, d1 + d3 * 0.2D, d2, 1.6F, (float) (14 - this.level().getDifficulty().getId() * 4));
		this.playSound(SoundEvents.ARROW_SHOOT, 1.0F, 1.0F / (this.getRandom().nextFloat() * 0.4F + 0.8F));
		this.level().addFreshEntity(abstractarrowentity);
	}
}
