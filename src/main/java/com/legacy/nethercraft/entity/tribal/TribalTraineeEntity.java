package com.legacy.nethercraft.entity.tribal;

import com.legacy.nethercraft.registry.NCItems;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.level.Level;

public class TribalTraineeEntity extends AbstractTribalEntity
{
	public TribalTraineeEntity(EntityType<? extends TribalTraineeEntity> type, Level world)
	{
		super(type, world);
	}

	@Override
	protected void registerGoals()
	{
		super.registerGoals();
		this.goalSelector.addGoal(2, new MeleeAttackGoal(this, 1.0D, false));
	}

	public static AttributeSupplier.Builder createAttributes()
	{
		return AbstractTribalEntity.createTribalAttributes().add(Attributes.ATTACK_DAMAGE, 3.0F);
	}

	@Override
	protected void populateDefaultEquipmentSlots(RandomSource rand, DifficultyInstance difficulty)
	{
		this.setItemSlot(EquipmentSlot.MAINHAND, new ItemStack(NCItems.netherrack_sword));
	}

	@Override
	public float getVoicePitch()
	{
		return (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.3F;
	}
}
