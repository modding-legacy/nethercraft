package com.legacy.nethercraft.entity.tribal;

import javax.annotation.Nullable;

import com.legacy.nethercraft.entity.BloodyZombieEntity;
import com.legacy.nethercraft.entity.CamouflageSpiderEntity;
import com.legacy.nethercraft.entity.DarkZombieEntity;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntitySpawnReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.entity.monster.ZombifiedPiglin;
import net.minecraft.world.entity.monster.piglin.Piglin;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.pathfinder.PathType;

public abstract class AbstractTribalEntity extends Monster
{
	public AbstractTribalEntity(EntityType<? extends AbstractTribalEntity> type, Level world)
	{
		super(type, world);
		this.setPathfindingMalus(PathType.LAVA, 8.0F);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(7, new WaterAvoidingRandomStrollGoal(this, 1.0D));
		this.targetSelector.addGoal(1, new AbstractTribalEntity.HurtByAggressorGoal(this));
		this.goalSelector.addGoal(8, new LookAtPlayerGoal(this, Player.class, 8.0F));
		this.goalSelector.addGoal(8, new RandomLookAroundGoal(this));
		this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<Piglin>(this, Piglin.class, true));
		this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<ZombifiedPiglin>(this, ZombifiedPiglin.class, true));
		this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<DarkZombieEntity>(this, DarkZombieEntity.class, true));
		this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<BloodyZombieEntity>(this, BloodyZombieEntity.class, true));
		this.targetSelector.addGoal(4, new NearestAttackableTargetGoal<CamouflageSpiderEntity>(this, CamouflageSpiderEntity.class, true));
	}

	/*protected void registerAttributes()
	{
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(25.0D);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue((double) 0.30F);
	}*/

	public static AttributeSupplier.Builder createTribalAttributes()
	{
		return Monster.createMonsterAttributes().add(Attributes.MAX_HEALTH, 25.0F).add(Attributes.MOVEMENT_SPEED, 0.30F);
	}

	@SuppressWarnings("deprecation")
	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor level, DifficultyInstance difficulty, EntitySpawnReason reason, @Nullable SpawnGroupData data)
	{
		data = super.finalizeSpawn(level, difficulty, reason, data);
		this.populateDefaultEquipmentSlots(level.getRandom(), difficulty);
		this.populateDefaultEquipmentEnchantments(level, level.getRandom(), difficulty);
		return data;
	}

	/*public boolean isNotColliding(IWorldReader worldIn)
	{
		return worldIn.checkNoEntityCollision(this) && !worldIn.containsAnyLiquid(this.getBoundingBox());
	}*/
	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);
	}

	@Override
	public boolean hurtServer(ServerLevel level, DamageSource source, float amount)
	{
		return super.hurtServer(level, source, amount);
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		this.playSound(SoundEvents.ZOMBIFIED_PIGLIN_AMBIENT, this.getSoundVolume(), this.getVoicePitch() - 0.3F);
		return null;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return SoundEvents.GENERIC_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return SoundEvents.GENERIC_BIG_FALL;
	}

	@Override
	public float getVoicePitch()
	{
		// FIXME: Add a base pitch instead of specifying per-tribal. This is not needed
		// once they have original sounds.
		return super.getVoicePitch();
	}

	@Override
	protected void populateDefaultEquipmentSlots(RandomSource rand, DifficultyInstance difficulty)
	{
	}

	// FIXME
	/*@Override
	public boolean isPreventingPlayerRest(PlayerEntity playerIn)
	{
		return false;
	}*/

	// FIXME: Tag for allies
	@Override
	public boolean considersEntityAsAlly(Entity entityIn)
	{
		if (entityIn == null)
		{
			return false;
		}
		else if (entityIn == this)
		{
			return true;
		}
		else if (super.considersEntityAsAlly(entityIn))
		{
			return true;
		}
		else
		{
			return entityIn instanceof AbstractTribalEntity && entityIn.getTeam() == null;
		}
	}

	static class HurtByAggressorGoal extends HurtByTargetGoal
	{

		public HurtByAggressorGoal(AbstractTribalEntity p_i45828_1_)
		{
			super(p_i45828_1_);
			this.setAlertOthers(new Class[] { Zombie.class });
		}

		@Override
		protected void alertOther(Mob mobIn, LivingEntity targetIn)
		{
			if (mobIn instanceof AbstractTribalEntity && this.mob.getSensing().hasLineOfSight(targetIn))
				mobIn.setTarget(targetIn);
		}
	}
}