package com.legacy.nethercraft.entity.tribal;

import javax.annotation.Nullable;

import com.legacy.nethercraft.registry.NCItems;

import net.minecraft.world.entity.EntitySpawnReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;

public class TribalWarriorEntity extends AbstractTribalEntity
{
	public static final EntityDataAccessor<Integer> WARRIOR_TYPE = SynchedEntityData.<Integer>defineId(TribalWarriorEntity.class, EntityDataSerializers.INT);

	public TribalWarriorEntity(EntityType<? extends TribalWarriorEntity> type, Level world)
	{
		super(type, world);
	}

	@Override
	protected void registerGoals()
	{
		super.registerGoals();
		this.goalSelector.addGoal(2, new MeleeAttackGoal(this, 1.0D, false));
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor level, DifficultyInstance difficulty, EntitySpawnReason reason, @Nullable SpawnGroupData data)
	{
		this.setWarriorType(this.random.nextInt(5));
		return super.finalizeSpawn(level, difficulty, reason, data);
	}

	public static AttributeSupplier.Builder createAttributes()
	{
		return AbstractTribalEntity.createTribalAttributes().add(Attributes.MAX_HEALTH, 30.0F).add(Attributes.ATTACK_DAMAGE, 3.0F);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putInt("WarriorType", this.getWarriorType());
	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);
		this.setWarriorType(compound.getInt("WarriorType"));
	}

	@Override
	protected void defineSynchedData(SynchedEntityData.Builder builder)
	{
		super.defineSynchedData(builder);
		builder.define(WARRIOR_TYPE, 0);
	}

	@Override
	protected void populateDefaultEquipmentSlots(RandomSource rand, DifficultyInstance difficulty)
	{
		this.setItemSlot(EquipmentSlot.MAINHAND, new ItemStack(NCItems.neridium_sword));
	}

	public void setWarriorType(int type)
	{
		this.entityData.set(WARRIOR_TYPE, type);
	}

	public int getWarriorType()
	{
		return this.entityData.get(WARRIOR_TYPE);
	}
}
