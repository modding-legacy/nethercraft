package com.legacy.nethercraft.entity;

import com.legacy.nethercraft.registry.NCItems;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.util.RandomSource;
import net.minecraft.world.Difficulty;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.level.Level;

public class DarkZombieEntity extends Zombie
{
	public DarkZombieEntity(EntityType<? extends DarkZombieEntity> type, Level world)
	{
		super(type, world);
	}

	public static AttributeSupplier.Builder createDarkZombieAttributes()
	{
		return Zombie.createAttributes().add(Attributes.MAX_HEALTH, 50.0F);
	}

	@Override
	protected boolean isSunSensitive()
	{
		return false;
	}

	@Override
	protected boolean convertsInWater()
	{
		return false;
	}

	@Override
	public float getVoicePitch()
	{
		return this.isBaby() ? (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.5F : (this.random.nextFloat() - this.random.nextFloat()) * 0.1F + 0.7F;
	}

	@Override
	protected void populateDefaultEquipmentSlots(RandomSource random, DifficultyInstance difficulty)
	{
		if (this.random.nextFloat() < 0.15F * difficulty.getSpecialMultiplier())
		{
			int i = this.random.nextInt(2);
			float f = this.level().getDifficulty() == Difficulty.HARD ? 0.1F : 0.25F;

			for (int e = 0; e < 3; ++e)
			{
				if (this.random.nextFloat() < 0.095F)
					++i;
			}

			boolean flag = true;

			// TODO EQUIPMENT_POPULATION_ORDER
			for (EquipmentSlot equipmentslot : EquipmentSlot.values())
			{
				ItemStack itemstack = this.getItemBySlot(equipmentslot);

				if (!flag && random.nextFloat() < f)
					break;

				flag = false;

				if (itemstack.isEmpty())
				{
					Item item = getEquipmentForSlot(equipmentslot, i);

					if (item != null)
						this.setItemSlot(equipmentslot, new ItemStack(item));
				}
			}
		}

		if (this.random.nextFloat() < (this.level().getDifficulty() == Difficulty.HARD ? 0.05F : 0.01F))
		{
			int i = this.random.nextInt(3);
			this.setItemSlot(EquipmentSlot.MAINHAND, new ItemStack(i == 0 ? NCItems.neridium_sword : NCItems.neridium_shovel));
		}
	}

	public static Item getArmorByChance(EquipmentSlot slotIn, int chance)
	{
		switch (slotIn)
		{
		case HEAD:
			if (chance == 0)
			{
				return NCItems.imp_helmet;
			}
			else if (chance == 1)
			{
				return Items.GOLDEN_HELMET;
			}
			else if (chance == 2)
			{
				return Items.CHAINMAIL_HELMET;
			}
			else if (chance == 3)
			{
				return NCItems.neridium_helmet;
			}
			else if (chance == 4)
			{
				return NCItems.w_obsidian_helmet;
			}
		case CHEST:
			if (chance == 0)
			{
				return NCItems.imp_chestplate;
			}
			else if (chance == 1)
			{
				return Items.GOLDEN_CHESTPLATE;
			}
			else if (chance == 2)
			{
				return Items.CHAINMAIL_CHESTPLATE;
			}
			else if (chance == 3)
			{
				return NCItems.neridium_chestplate;
			}
			else if (chance == 4)
			{
				return NCItems.w_obsidian_chestplate;
			}
		case LEGS:
			if (chance == 0)
			{
				return NCItems.imp_leggings;
			}
			else if (chance == 1)
			{
				return Items.GOLDEN_LEGGINGS;
			}
			else if (chance == 2)
			{
				return Items.CHAINMAIL_LEGGINGS;
			}
			else if (chance == 3)
			{
				return NCItems.neridium_leggings;
			}
			else if (chance == 4)
			{
				return NCItems.w_obsidian_leggings;
			}
		case FEET:
			if (chance == 0)
			{
				return NCItems.imp_boots;
			}
			else if (chance == 1)
			{
				return Items.GOLDEN_BOOTS;
			}
			else if (chance == 2)
			{
				return Items.CHAINMAIL_BOOTS;
			}
			else if (chance == 3)
			{
				return NCItems.neridium_boots;
			}
			else if (chance == 4)
			{
				return NCItems.w_obsidian_boots;
			}
		default:
			return null;
		}
	}
}