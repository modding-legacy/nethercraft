package com.legacy.nethercraft.entity.misc;

import javax.annotation.Nullable;

import com.legacy.nethercraft.registry.NCEntityTypes;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.item.PrimedTnt;
import net.minecraft.world.level.Level;

public class GhastBombEntity extends PrimedTnt
{
	public GhastBombEntity(EntityType<? extends GhastBombEntity> type, Level level)
	{
		super(type, level);
	}

	public GhastBombEntity(Level level, double x, double y, double z, @Nullable LivingEntity owner)
	{
		this(NCEntityTypes.GHAST_BOMB, level);
		this.setPos(x, y, z);
		double d0 = level.random.nextDouble() * (float) (Math.PI * 2);
		this.setDeltaMovement(-Math.sin(d0) * 0.02, 0.2F, -Math.cos(d0) * 0.02);
		this.setFuse(80);
		this.xo = x;
		this.yo = y;
		this.zo = z;
		this.owner = owner;
	}
}