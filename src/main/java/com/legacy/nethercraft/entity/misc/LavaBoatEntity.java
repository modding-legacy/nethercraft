package com.legacy.nethercraft.entity.misc;

/**
 * FIXME: What do we even do with this...? The Strider exists so we need to
 * rework this or remove it.
 */
@Deprecated
public class LavaBoatEntity // extends Entity
{
	/*private static final DataParameter<Integer> TIME_SINCE_HIT = EntityDataManager.defineId(LavaBoatEntity.class, DataSerializers.INT);
	private static final DataParameter<Integer> FORWARD_DIRECTION = EntityDataManager.defineId(LavaBoatEntity.class, DataSerializers.INT);
	private static final DataParameter<Float> DAMAGE_TAKEN = EntityDataManager.defineId(LavaBoatEntity.class, DataSerializers.FLOAT);
	private static final DataParameter<Boolean> DATA_ID_PADDLE_LEFT = EntityDataManager.defineId(LavaBoatEntity.class, DataSerializers.BOOLEAN);
	private static final DataParameter<Boolean> DATA_ID_PADDLE_RIGHT = EntityDataManager.defineId(LavaBoatEntity.class, DataSerializers.BOOLEAN);
	private static final DataParameter<Integer> ROCKING_TICKS = EntityDataManager.defineId(LavaBoatEntity.class, DataSerializers.INT);
	private final float[] paddlePositions = new float[2];
	private float momentum;
	private float outOfControlTicks;
	private float deltaRotation;
	private int lerpSteps;
	private double lerpX;
	private double lerpY;
	private double lerpZ;
	private double lerpYaw;
	private double lerpPitch;
	private boolean leftInputDown;
	private boolean rightInputDown;
	private boolean forwardInputDown;
	private boolean backInputDown;
	private double lavaLevel;
	private float boatGlide;
	private LavaBoatEntity.Status status;
	private LavaBoatEntity.Status previousStatus;
	private double lastYd;
	private boolean rocking;
	private boolean bubbleColumnDirectionIsDown;
	private float rockingIntensity;
	private float rockingAngle;
	private float prevRockingAngle;
	private boolean removed = !this.isAlive();
	
	public LavaBoatEntity(EntityType<? extends LavaBoatEntity> p_i50129_1_, Level p_i50129_2_)
	{
		super(p_i50129_1_, p_i50129_2_);
		this.blocksBuilding = true;
	}
	
	public LavaBoatEntity(Level worldIn, double x, double y, double z)
	{
		this(NetherEntityTypes.LAVA_BOAT, worldIn);
		this.setPos(x, y, z);
		this.setDeltaMovement(Vec3.ZERO);
		this.xo = x;
		this.yo = y;
		this.zo = z;
	}
	
	public LavaBoatEntity(FMLPlayMessages.SpawnEntity spawnEntity, Level world)
	{
		this(NetherEntityTypes.LAVA_BOAT, world);
	}
	
	@Override
	public void tick()
	{
		this.remove();
		super.tick();
	}
	
	protected boolean isMovementNoisy()
	{
		return false;
	}
	
	protected void registerData()
	{
		this.dataManager.register(TIME_SINCE_HIT, 0);
		this.dataManager.register(FORWARD_DIRECTION, 1);
		this.dataManager.register(DAMAGE_TAKEN, 0.0F);
		this.dataManager.register(DATA_ID_PADDLE_LEFT, false);
		this.dataManager.register(DATA_ID_PADDLE_RIGHT, false);
		this.dataManager.register(ROCKING_TICKS, 0);
	}
	
	@Nullable
	public AxisAlignedBB getCollisionBox(Entity entityIn)
	{
		return entityIn.canBePushed() ? entityIn.getBoundingBox() : null;
	}
	
	@Nullable
	public AxisAlignedBB getCollisionBoundingBox()
	{
		return this.getBoundingBox();
	}
	
	public boolean canBePushed()
	{
		return true;
	}
	
	public double getMountedYOffset()
	{
		return -0.1D;
	}
	
	public boolean attackEntityFrom(DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(source))
		{
			return false;
		}
		else if (!this.world.isRemote && !this.removed)
		{
			if (source instanceof IndirectEntityDamageSource && source.getTrueSource() != null && this.isPassenger(source.getTrueSource()))
			{
				return false;
			}
			else
			{
				this.setForwardDirection(-this.getForwardDirection());
				this.setTimeSinceHit(10);
				this.setDamageTaken(this.getDamageTaken() + amount * 10.0F);
				this.markVelocityChanged();
				boolean flag = source.getTrueSource() instanceof PlayerEntity && ((PlayerEntity) source.getTrueSource()).abilities.isCreativeMode;
				if (flag || this.getDamageTaken() > 40.0F)
				{
					if (!flag && this.world.getGameRules().getBoolean(GameRules.DO_ENTITY_DROPS))
					{
						this.entityDropItem(this.getItemBoat());
					}
	
					this.remove();
				}
	
				return true;
			}
		}
		else
		{
			return true;
		}
	}
	
	public void onEnterBubbleColumnWithAirAbove(boolean downwards)
	{
		if (!this.world.isRemote)
		{
			this.rocking = true;
			this.bubbleColumnDirectionIsDown = downwards;
			if (this.getRockingTicks() == 0)
			{
				this.setRockingTicks(60);
			}
		}
	
		this.world.addParticle(ParticleTypes.SPLASH, this.getPosX() + (double) this.rand.nextFloat(), this.getPosY() + 0.7D, this.getPosZ() + (double) this.rand.nextFloat(), 0.0D, 0.0D, 0.0D);
		if (this.rand.nextInt(20) == 0)
		{
			this.world.playSound(this.getPosX(), this.getPosY(), this.getPosZ(), this.getSplashSound(), this.getSoundCategory(), 1.0F, 0.8F + 0.4F * this.rand.nextFloat(), false);
		}
	
	}
	
	*//**
		 * Applies a velocity to the entities, to push them away from eachother.
		 *//*
			public void applyEntityCollision(Entity entityIn)
			{
			if (entityIn instanceof LavaBoatEntity)
			{
				if (entityIn.getBoundingBox().minY < this.getBoundingBox().maxY)
				{
					super.applyEntityCollision(entityIn);
				}
			}
			else if (entityIn.getBoundingBox().minY <= this.getBoundingBox().minY)
			{
				super.applyEntityCollision(entityIn);
			}
			
			}
			
			public Item getItemBoat()
			{
			return NetherItems.lava_boat;
			}
			
			@OnlyIn(Dist.CLIENT)
			public void performHurtAnimation()
			{
			this.setForwardDirection(-this.getForwardDirection());
			this.setTimeSinceHit(10);
			this.setDamageTaken(this.getDamageTaken() * 11.0F);
			}
			
			public boolean canBeCollidedWith()
			{
			return !this.removed;
			}
			
			@OnlyIn(Dist.CLIENT)
			public void setPositionAndRotationDirect(double x, double y, double z, float yaw, float pitch, int posRotationIncrements, boolean teleport)
			{
			this.lerpX = x;
			this.lerpY = y;
			this.lerpZ = z;
			this.lerpYaw = (double) yaw;
			this.lerpPitch = (double) pitch;
			this.lerpSteps = 10;
			}
			
			public Direction getAdjustedHorizontalFacing()
			{
			return this.getHorizontalFacing().rotateY();
			}
			
			// TODO
			@Override
			public void tick()
			{
			this.previousStatus = this.status;
			this.status = this.getBoatStatus();
			if (this.status != LavaBoatEntity.Status.UNDER_LAVA && this.status != LavaBoatEntity.Status.UNDER_FLOWING_LAVA)
			{
				this.outOfControlTicks = 0.0F;
			}
			else
			{
				++this.outOfControlTicks;
			}
			
			if (!this.world.isRemote && this.outOfControlTicks >= 60.0F)
			{
				this.removePassengers();
			}
			
			if (this.getTimeSinceHit() > 0)
			{
				this.setTimeSinceHit(this.getTimeSinceHit() - 1);
			}
			
			if (this.getDamageTaken() > 0.0F)
			{
				this.setDamageTaken(this.getDamageTaken() - 1.0F);
			}
			
			super.tick();
			this.tickLerp();
			if (this.canPassengerSteer())
			{
				if (this.getPassengers().size() == 0 || !(this.getPassengers().get(0) instanceof PlayerEntity))
				{
					this.setPaddleState(false, false);
				}
			
				this.updateMotion();
			
				if (this.isBeingRidden())
				{
					if (this.getPassengers().get(0) instanceof PlayerEntity)
					{
						PlayerEntity entity = (PlayerEntity) this.getPassengers().get(0);
			
						if (entity.moveStrafing != 0 || entity.moveForward != 0)
						{
							this.controlBoat();
			
						}
			
						if (entity.moveForward != 0)
						{
							this.updateInputs(false, false, entity.moveForward > 0.01F, entity.moveForward < 0.01F);
						}
			
						if (entity.moveStrafing != 0)
						{
							this.updateInputs(entity.moveStrafing > 0.01F, entity.moveStrafing < 0.01F, false, false);
						}
					}
				}
			
				this.move(MoverType.SELF, this.getMotion());
			}
			else
			{
				this.setMotion(Vector3d.ZERO);
			}
			
			this.updateRocking();
			
			for (int i = 0; i <= 1; ++i)
			{
				if (this.getPaddleState(i))
				{
					if (!this.isSilent() && (double) (this.paddlePositions[i] % ((float) Math.PI * 2F)) <= (double) ((float) Math.PI / 4F) && ((double) this.paddlePositions[i] + (double) ((float) Math.PI / 8F)) % (double) ((float) Math.PI * 2F) >= (double) ((float) Math.PI / 4F))
					{
						SoundEvent soundevent = this.getPaddleSound();
						if (soundevent != null)
						{
							Vector3d vec3d = this.getLook(1.0F);
							double d0 = i == 1 ? -vec3d.z : vec3d.z;
							double d1 = i == 1 ? vec3d.x : -vec3d.x;
							this.world.playSound((PlayerEntity) null, this.getPosX() + d0, this.getPosY(), this.getPosZ() + d1, soundevent, this.getSoundCategory(), 1.0F, 0.8F + 0.4F * this.rand.nextFloat());
						}
					}
			
					this.paddlePositions[i] = (float) ((double) this.paddlePositions[i] + (double) ((float) Math.PI / 8F));
				}
				else
				{
					this.paddlePositions[i] = 0.0F;
				}
			}
			
			this.doBlockCollisions();
			List<Entity> list = this.world.getEntitiesInAABBexcluding(this, this.getBoundingBox().grow((double) 0.2F, (double) -0.01F, (double) 0.2F), EntityPredicates.pushableBy(this));
			if (!list.isEmpty())
			{
				boolean flag = !this.world.isRemote && !(this.getControllingPassenger() instanceof PlayerEntity);
			
				for (int j = 0; j < list.size(); ++j)
				{
					Entity entity = list.get(j);
					if (!entity.isPassenger(this))
					{
						if (flag && this.getPassengers().size() < 2 && !entity.isPassenger() && entity.getWidth() < this.getWidth() && entity instanceof LivingEntity && !(entity instanceof WaterMobEntity) && !(entity instanceof PlayerEntity))
						{
							entity.startRiding(this);
						}
						else
						{
							this.applyEntityCollision(entity);
						}
					}
				}
			}
			}
			
			private void updateRocking()
			{
			if (this.world.isRemote)
			{
				int i = this.getRockingTicks();
				if (i > 0)
				{
					this.rockingIntensity += 0.05F;
				}
				else
				{
					this.rockingIntensity -= 0.1F;
				}
			
				this.rockingIntensity = MathHelper.clamp(this.rockingIntensity, 0.0F, 1.0F);
				this.prevRockingAngle = this.rockingAngle;
				this.rockingAngle = 10.0F * (float) Math.sin((double) (0.5F * (float) this.world.getGameTime())) * this.rockingIntensity;
			}
			else
			{
				if (!this.rocking)
				{
					this.setRockingTicks(0);
				}
			
				int k = this.getRockingTicks();
				if (k > 0)
				{
					--k;
					this.setRockingTicks(k);
					int j = 60 - k - 1;
					if (j > 0 && k == 0)
					{
						this.setRockingTicks(0);
						Vector3d vec3d = this.getMotion();
						if (this.bubbleColumnDirectionIsDown)
						{
							this.setMotion(vec3d.add(0.0D, -0.7D, 0.0D));
							this.removePassengers();
						}
						else
						{
							this.setMotion(vec3d.x, this.isPassenger(PlayerEntity.class) ? 2.7D : 0.6D, vec3d.z);
						}
					}
			
					this.rocking = false;
				}
			}
			
			}
			
			@Nullable
			protected SoundEvent getPaddleSound()
			{
			switch (this.getBoatStatus())
			{
			case IN_LAVA:
			case UNDER_LAVA:
			case UNDER_FLOWING_LAVA:
				return SoundEvents.ENTITY_BOAT_PADDLE_WATER;
			case ON_LAND:
				return SoundEvents.ENTITY_BOAT_PADDLE_LAND;
			case IN_AIR:
			default:
				return null;
			}
			}
			
			private void tickLerp()
			{
			if (this.canPassengerSteer())
			{
				this.lerpSteps = 0;
				this.setPacketCoordinates(this.getPosX(), this.getPosY(), this.getPosZ());
			}
			
			if (this.lerpSteps > 0)
			{
				double d0 = this.getPosX() + (this.lerpX - this.getPosX()) / (double) this.lerpSteps;
				double d1 = this.getPosY() + (this.lerpY - this.getPosY()) / (double) this.lerpSteps;
				double d2 = this.getPosZ() + (this.lerpZ - this.getPosZ()) / (double) this.lerpSteps;
				double d3 = MathHelper.wrapDegrees(this.lerpYaw - (double) this.rotationYaw);
				this.rotationYaw = (float) ((double) this.rotationYaw + d3 / (double) this.lerpSteps);
				this.rotationPitch = (float) ((double) this.rotationPitch + (this.lerpPitch - (double) this.rotationPitch) / (double) this.lerpSteps);
				--this.lerpSteps;
				this.setPosition(d0, d1, d2);
				this.setRotation(this.rotationYaw, this.rotationPitch);
			}
			}
			
			public void setPaddleState(boolean left, boolean right)
			{
			this.dataManager.set(DATA_ID_PADDLE_LEFT, left);
			this.dataManager.set(DATA_ID_PADDLE_RIGHT, right);
			}
			
			@OnlyIn(Dist.CLIENT)
			public float getRowingTime(int side, float limbSwing)
			{
			return this.getPaddleState(side) ? (float) MathHelper.clampedLerp((double) this.paddlePositions[side] - (double) ((float) Math.PI / 8F), (double) this.paddlePositions[side], (double) limbSwing) : 0.0F;
			}
			
			private LavaBoatEntity.Status getBoatStatus()
			{
			LavaBoatEntity.Status LavaBoatEntity$status = this.getUnderLavaStatus();
			if (LavaBoatEntity$status != null)
			{
				this.lavaLevel = this.getBoundingBox().maxY;
				return LavaBoatEntity$status;
			}
			else if (this.checkInLava())
			{
				return LavaBoatEntity.Status.IN_LAVA;
			}
			else
			{
				float f = this.getBoatGlide();
				if (f > 0.0F)
				{
					this.boatGlide = f;
					return LavaBoatEntity.Status.ON_LAND;
				}
				else
				{
					return LavaBoatEntity.Status.IN_AIR;
				}
			}
			}
			
			public float getLavaLevelAbove()
			{
			AxisAlignedBB axisalignedbb = this.getBoundingBox();
			int i = MathHelper.floor(axisalignedbb.minX);
			int j = MathHelper.ceil(axisalignedbb.maxX);
			int k = MathHelper.floor(axisalignedbb.maxY);
			int l = MathHelper.ceil(axisalignedbb.maxY - this.lastYd);
			int i1 = MathHelper.floor(axisalignedbb.minZ);
			int j1 = MathHelper.ceil(axisalignedbb.maxZ);
			
			try (BlockPos.PooledMutable blockpos$pooledmutable = BlockPos.PooledMutable.retain())
			{
				label161: for (int k1 = k; k1 < l; ++k1)
				{
					float f = 0.0F;
			
					for (int l1 = i; l1 < j; ++l1)
					{
						for (int i2 = i1; i2 < j1; ++i2)
						{
							blockpos$pooledmutable.setPos(l1, k1, i2);
							IFluidState ifluidstate = this.world.getFluidState(blockpos$pooledmutable);
							if (ifluidstate.isTagged(FluidTags.LAVA))
							{
								f = Math.max(f, ifluidstate.getActualHeight(this.world, blockpos$pooledmutable));
							}
			
							if (f >= 1.0F)
							{
								continue label161;
							}
						}
					}
			
					if (f < 1.0F)
					{
						float f2 = (float) blockpos$pooledmutable.getY() + f;
						return f2;
					}
				}
			
				float f1 = (float) (l + 1);
				return f1;
			}
			}
			
			public float getBoatGlide()
			{
			AxisAlignedBB axisalignedbb = this.getBoundingBox();
			AxisAlignedBB axisalignedbb1 = new AxisAlignedBB(axisalignedbb.minX, axisalignedbb.minY - 0.001D, axisalignedbb.minZ, axisalignedbb.maxX, axisalignedbb.minY, axisalignedbb.maxZ);
			int i = MathHelper.floor(axisalignedbb1.minX) - 1;
			int j = MathHelper.ceil(axisalignedbb1.maxX) + 1;
			int k = MathHelper.floor(axisalignedbb1.minY) - 1;
			int l = MathHelper.ceil(axisalignedbb1.maxY) + 1;
			int i1 = MathHelper.floor(axisalignedbb1.minZ) - 1;
			int j1 = MathHelper.ceil(axisalignedbb1.maxZ) + 1;
			VoxelShape voxelshape = VoxelShapes.create(axisalignedbb1);
			float f = 0.0F;
			int k1 = 0;
			
			try (BlockPos.PooledMutable blockpos$pooledmutable = BlockPos.PooledMutable.retain())
			{
				for (int l1 = i; l1 < j; ++l1)
				{
					for (int i2 = i1; i2 < j1; ++i2)
					{
						int j2 = (l1 != i && l1 != j - 1 ? 0 : 1) + (i2 != i1 && i2 != j1 - 1 ? 0 : 1);
						if (j2 != 2)
						{
							for (int k2 = k; k2 < l; ++k2)
							{
								if (j2 <= 0 || k2 != k && k2 != l - 1)
								{
									blockpos$pooledmutable.setPos(l1, k2, i2);
									BlockState blockstate = this.world.getBlockState(blockpos$pooledmutable);
									if (!(blockstate.getBlock() instanceof LilyPadBlock) && VoxelShapes.compare(blockstate.getCollisionShape(this.world, blockpos$pooledmutable).withOffset((double) l1, (double) k2, (double) i2), voxelshape, IBooleanFunction.AND))
									{
										f += blockstate.getSlipperiness(this.world, blockpos$pooledmutable, this);
										++k1;
									}
								}
							}
						}
					}
				}
			}
			
			return f / (float) k1;
			}
			
			private boolean checkInLava()
			{
			AxisAlignedBB axisalignedbb = this.getBoundingBox();
			int i = MathHelper.floor(axisalignedbb.minX);
			int j = MathHelper.ceil(axisalignedbb.maxX);
			int k = MathHelper.floor(axisalignedbb.minY);
			int l = MathHelper.ceil(axisalignedbb.minY + 0.001D);
			int i1 = MathHelper.floor(axisalignedbb.minZ);
			int j1 = MathHelper.ceil(axisalignedbb.maxZ);
			boolean flag = false;
			this.lavaLevel = Double.MIN_VALUE;
			
			try (BlockPos.PooledMutable blockpos$pooledmutable = BlockPos.PooledMutable.retain())
			{
				for (int k1 = i; k1 < j; ++k1)
				{
					for (int l1 = k; l1 < l; ++l1)
					{
						for (int i2 = i1; i2 < j1; ++i2)
						{
							blockpos$pooledmutable.setPos(k1, l1, i2);
							IFluidState ifluidstate = this.world.getFluidState(blockpos$pooledmutable);
							if (ifluidstate.isTagged(FluidTags.LAVA))
							{
								float f = (float) l1 + ifluidstate.getActualHeight(this.world, blockpos$pooledmutable);
								this.lavaLevel = Math.max((double) f, this.lavaLevel);
								flag |= axisalignedbb.minY < (double) f;
							}
						}
					}
				}
			}
			
			return flag;
			}
			
			@Nullable
			private LavaBoatEntity.Status getUnderLavaStatus()
			{
			AxisAlignedBB axisalignedbb = this.getBoundingBox();
			double d0 = axisalignedbb.maxY + 0.001D;
			int i = MathHelper.floor(axisalignedbb.minX);
			int j = MathHelper.ceil(axisalignedbb.maxX);
			int k = MathHelper.floor(axisalignedbb.maxY);
			int l = MathHelper.ceil(d0);
			int i1 = MathHelper.floor(axisalignedbb.minZ);
			int j1 = MathHelper.ceil(axisalignedbb.maxZ);
			boolean flag = false;
			
			try (BlockPos.PooledMutable blockpos$pooledmutable = BlockPos.PooledMutable.retain())
			{
				for (int k1 = i; k1 < j; ++k1)
				{
					for (int l1 = k; l1 < l; ++l1)
					{
						for (int i2 = i1; i2 < j1; ++i2)
						{
							blockpos$pooledmutable.setPos(k1, l1, i2);
							IFluidState ifluidstate = this.world.getFluidState(blockpos$pooledmutable);
							if (ifluidstate.isTagged(FluidTags.LAVA) && d0 < (double) ((float) blockpos$pooledmutable.getY() + ifluidstate.getActualHeight(this.world, blockpos$pooledmutable)))
							{
								if (!ifluidstate.isSource())
								{
									LavaBoatEntity.Status LavaBoatEntity$status = LavaBoatEntity.Status.UNDER_FLOWING_LAVA;
									return LavaBoatEntity$status;
								}
			
								flag = true;
							}
						}
					}
				}
			}
			
			return flag ? LavaBoatEntity.Status.UNDER_LAVA : null;
			}
			
			private void updateMotion()
			{
			double d0 = (double) -0.04F;
			double d1 = this.hasNoGravity() ? 0.0D : -0.03999999910593033D;
			double d2 = 0.0D;
			this.momentum = 0.05F;
			if (this.previousStatus == LavaBoatEntity.Status.IN_AIR && this.status != LavaBoatEntity.Status.IN_AIR && this.status != LavaBoatEntity.Status.ON_LAND)
			{
				this.lavaLevel = this.getBoundingBox().minY + (double) this.getHeight();
				this.setPosition(this.blockPosition().getX(), (double) (this.getLavaLevelAbove() - this.getHeight()) + 0.101D, this.blockPosition().getZ());
				this.setMotion(this.getMotion().mul(1.0D, 0.0D, 1.0D));
				this.lastYd = 0.0D;
				this.status = LavaBoatEntity.Status.IN_LAVA;
			}
			else
			{
				if (this.status == LavaBoatEntity.Status.IN_LAVA)
				{
					d2 = (this.lavaLevel - this.getBoundingBox().minY) / (double) this.getHeight();
					this.momentum = 0.9F;
				}
				else if (this.status == LavaBoatEntity.Status.UNDER_FLOWING_LAVA)
				{
					d1 = -7.0E-4D;
					this.momentum = 0.9F;
				}
				else if (this.status == LavaBoatEntity.Status.UNDER_LAVA)
				{
					d2 = (double) 0.01F;
					this.momentum = 0.45F;
				}
				else if (this.status == LavaBoatEntity.Status.IN_AIR)
				{
					this.momentum = 0.9F;
				}
				else if (this.status == LavaBoatEntity.Status.ON_LAND)
				{
					this.momentum = this.boatGlide;
					if (this.getControllingPassenger() instanceof PlayerEntity)
					{
						this.boatGlide /= 2.0F;
					}
				}
				Vector3d vec3d = this.getMotion();
				this.setMotion(vec3d.x * (double) this.momentum, vec3d.y + d1, vec3d.z * (double) this.momentum);
				this.deltaRotation *= this.momentum;
				if (d2 > 0.0D)
				{
					Vector3d vec3d1 = this.getMotion();
					this.setMotion(vec3d1.x, d2 * (-d0 / 0.65D), vec3d1.z);
				}
			}
			
			}
			
			private void controlBoat()
			{
			if (this.isBeingRidden())
			{
				float f = 0.005F;
				if (this.leftInputDown)
				{
					--this.deltaRotation;
				}
				if (this.rightInputDown)
				{
					++this.deltaRotation;
				}
				if (this.rightInputDown != this.leftInputDown && !this.forwardInputDown && !this.backInputDown)
				{
					f += 0.005F;
				}
				this.rotationYaw += this.deltaRotation;
				if (this.forwardInputDown)
				{
					f += 0.04F;
				}
				if (this.backInputDown)
				{
					f -= 0.005F;
				}
				this.setMotion(this.getMotion().add((double) (MathHelper.sin(-this.rotationYaw * ((float) Math.PI / 180F)) * f), 0.0D, (double) (MathHelper.cos(this.rotationYaw * ((float) Math.PI / 180F)) * f)));
				this.setPaddleState(this.rightInputDown && !this.leftInputDown || this.forwardInputDown, this.leftInputDown && !this.rightInputDown || this.forwardInputDown);
			}
			}
			
			@Override
			public void updatePassenger(Entity passenger)
			{
			if (this.isPassenger(passenger))
			{
				float f = 0.0F;
				float f1 = (float) ((this.removed ? (double) 0.01F : this.getMountedYOffset()) + passenger.getYOffset());
				if (this.getPassengers().size() > 1)
				{
					int i = this.getPassengers().indexOf(passenger);
					if (i == 0)
					{
						f = 0.2F;
					}
					else
					{
						f = -0.6F;
					}
			
					if (passenger instanceof AnimalEntity)
					{
						f = (float) ((double) f + 0.2D);
					}
				}
			
				Vector3d vec3d = (new Vector3d((double) f, 0.0D, 0.0D)).rotateYaw(-this.rotationYaw * ((float) Math.PI / 180F) - ((float) Math.PI / 2F));
				passenger.setPosition(this.getPosX() + vec3d.x, this.getPosY() + (double) f1, this.getPosZ() + vec3d.z);
				passenger.rotationYaw += this.deltaRotation;
				passenger.setRotationYawHead(passenger.getRotationYawHead() + this.deltaRotation);
				this.applyYawToEntity(passenger);
			
				if (passenger instanceof AnimalEntity && this.getPassengers().size() > 1)
				{
					int j = passenger.getEntityId() % 2 == 0 ? 90 : 270;
					passenger.setRenderYawOffset(((AnimalEntity) passenger).renderYawOffset + (float) j);
					passenger.setRotationYawHead(passenger.getRotationYawHead() + (float) j);
				}
			}
			}
			
			protected void applyYawToEntity(Entity entityToUpdate)
			{
			entityToUpdate.setRenderYawOffset(this.rotationYaw);
			float f = MathHelper.wrapDegrees(entityToUpdate.rotationYaw - this.rotationYaw);
			float f1 = MathHelper.clamp(f, -105.0F, 105.0F);
			entityToUpdate.prevRotationYaw += f1 - f;
			entityToUpdate.rotationYaw += f1 - f;
			entityToUpdate.setRotationYawHead(entityToUpdate.rotationYaw);
			}
			
			@OnlyIn(Dist.CLIENT)
			@Override
			public void applyOrientationToEntity(Entity entityToUpdate)
			{
			this.applyYawToEntity(entityToUpdate);
			}
			
			@Override
			protected void writeAdditional(CompoundNBT compound)
			{
			}
			
			@Override
			protected void readAdditional(CompoundNBT compound)
			{
			}
			
			@Override
			public boolean processInitialInteract(PlayerEntity player, Hand hand)
			{
			if (player.isSecondaryUseActive())
			{
				return false;
			}
			else
			{
				return !this.world.isRemote && this.outOfControlTicks < 60.0F ? player.startRiding(this) : false;
			}
			}
			
			@Override
			protected void updateFallState(double y, boolean onGroundIn, BlockState state, BlockPos pos)
			{
			this.lastYd = this.getMotion().y;
			
			if (!this.isPassenger())
			{
				if (onGroundIn)
				{
					if (this.fallDistance > 3.0F)
					{
						if (this.status != LavaBoatEntity.Status.ON_LAND)
						{
							this.fallDistance = 0.0F;
							return;
						}
			
						this.onLivingFall(this.fallDistance, 1.0F);
						if (!this.world.isRemote && !this.removed)
						{
							this.remove();
							if (this.world.getGameRules().getBoolean(GameRules.DO_ENTITY_DROPS))
							{
								for (int i = 0; i < 3; ++i)
								{
									this.entityDropItem(NetherBlocks.glowood_planks.asItem());
								}
			
								for (int j = 0; j < 2; ++j)
								{
									this.entityDropItem(NetherItems.glowood_stick);
								}
							}
						}
					}
			
					this.fallDistance = 0.0F;
				}
				else if (!this.world.getFluidState((new BlockPos(this)).down()).isTagged(FluidTags.LAVA) && y < 0.0D)
				{
					this.fallDistance = (float) ((double) this.fallDistance - y);
				}
			
			}
			}
			
			public boolean getPaddleState(int side)
			{
			return this.dataManager.<Boolean>get(side == 0 ? DATA_ID_PADDLE_LEFT : DATA_ID_PADDLE_RIGHT) && this.getControllingPassenger() != null;
			}
			
			public void setDamageTaken(float damageTaken)
			{
			this.dataManager.set(DAMAGE_TAKEN, damageTaken);
			}
			
			public float getDamageTaken()
			{
			return this.dataManager.get(DAMAGE_TAKEN);
			}
			
			public void setTimeSinceHit(int timeSinceHit)
			{
			this.dataManager.set(TIME_SINCE_HIT, timeSinceHit);
			}
			
			public int getTimeSinceHit()
			{
			return this.dataManager.get(TIME_SINCE_HIT);
			}
			
			private void setRockingTicks(int p_203055_1_)
			{
			this.dataManager.set(ROCKING_TICKS, p_203055_1_);
			}
			
			private int getRockingTicks()
			{
			return this.dataManager.get(ROCKING_TICKS);
			}
			
			@OnlyIn(Dist.CLIENT)
			public float getRockingAngle(float partialTicks)
			{
			return MathHelper.lerp(partialTicks, this.prevRockingAngle, this.rockingAngle);
			}
			
			public void setForwardDirection(int forwardDirection)
			{
			this.dataManager.set(FORWARD_DIRECTION, forwardDirection);
			}
			
			public int getForwardDirection()
			{
			return this.dataManager.get(FORWARD_DIRECTION);
			}
			
			protected boolean canFitPassenger(Entity passenger)
			{
			return this.getPassengers().size() < 2 && !this.areEyesInFluid(FluidTags.LAVA);
			}
			
			@Nullable
			public Entity getControllingPassenger()
			{
			List<Entity> list = this.getPassengers();
			return list.isEmpty() ? null : list.get(0);
			}
			
			@OnlyIn(Dist.CLIENT)
			public void updateInputs(boolean p_184442_1_, boolean p_184442_2_, boolean p_184442_3_, boolean p_184442_4_)
			{
			this.leftInputDown = p_184442_1_;
			this.rightInputDown = p_184442_2_;
			this.forwardInputDown = p_184442_3_;
			this.backInputDown = p_184442_4_;
			}
			
			
			
			@Override
			protected void addPassenger(Entity passenger)
			{
			super.addPassenger(passenger);
			if (this.canPassengerSteer() && this.lerpSteps > 0)
			{
				this.lerpSteps = 0;
				this.setPositionAndRotation(this.lerpX, this.lerpY, this.lerpZ, (float) this.lerpYaw, (float) this.lerpPitch);
			}
			}
			
			public static enum Status
			{
			IN_LAVA, UNDER_LAVA, UNDER_FLOWING_LAVA, ON_LAND, IN_AIR;
			}
			
			@Override
			public ItemEntity entityDropItem(ItemStack stack, float offsetY)
			{
			if (stack.isEmpty())
			{
				return null;
			}
			else if (this.world.isRemote)
			{
				return null;
			}
			else
			{
				ItemEntity itementity = new ItemEntity(this.world, this.blockPosition().getX(), this.blockPosition().getY() + (double) offsetY, this.blockPosition().getZ(), stack);
				itementity.setInvulnerable(true);
				itementity.setMotion(0, 0.3F, 0);
				if (captureDrops() != null)
					captureDrops().add(itementity);
				else
					this.world.addEntity(itementity);
				return itementity;
			}
			}
			
			@Override
			public Packet<?> getAddEntityPacket()
			{
			return NetworkHooks.getEntitySpawningPacket(this);
			}
			
			@Override
			protected void defineSynchedData()
			{
			// TODO Auto-generated method stub
			
			}
			
			@Override
			protected void readAdditionalSaveData(CompoundTag compound)
			{
			// TODO Auto-generated method stub
			
			}
			
			@Override
			protected void addAdditionalSaveData(CompoundTag compound)
			{
			// TODO Auto-generated method stub
			
			}*/
}