package com.legacy.nethercraft.entity.misc;

/**
 * What do we do with this? Paintings have been changed, probably just worth
 * adding the unique ones individually instead.
 */
@Deprecated
public class NetherPaintingEntity // extends Painting
{
	/*public NetherPaintingEntity(EntityType<? extends NetherPaintingEntity> type, Level world)
	{
		super(type, world);
	}
	
	@SuppressWarnings("deprecation")
	public NetherPaintingEntity(Level worldIn, BlockPos pos, Direction facing)
	{
		super(worldIn, pos, facing);
		List<Motive> list = Lists.newArrayList();
		int i = 0;
		for (Motive paintingtype : Registry.MOTIVE)
		{
			this.motive = paintingtype;
			this.setDirection(facing);
			if (this.survives())
			{
				list.add(paintingtype);
				int j = paintingtype.getWidth() * paintingtype.getHeight();
				if (j > i)
				{
					i = j;
				}
			}
		}
		if (!list.isEmpty())
		{
			Iterator<Motive> iterator = list.iterator();
			while (iterator.hasNext())
			{
				Motive paintingtype1 = iterator.next();
				if (paintingtype1.getWidth() * paintingtype1.getHeight() < i)
				{
					iterator.remove();
				}
			}
			this.motive = list.get(this.random.nextInt(list.size()));
		}
		this.setDirection(facing);
	}
	
	public NetherPaintingEntity(FMLPlayMessages.SpawnEntity spawnEntity, Level world)
	{
		this(NetherEntityTypes.NETHER_PAINTING, world);
	}
	
	@Override
	public void dropItem(@Nullable Entity brokenEntity)
	{
		if (this.level.getGameRules().getBoolean(GameRules.RULE_DOENTITYDROPS))
		{
			this.playSound(SoundEvents.PAINTING_BREAK, 1.0F, 1.0F);
			if (brokenEntity instanceof Player)
			{
				Player playerentity = (Player) brokenEntity;
				if (playerentity.abilities.instabuild)
				{
					return;
				}
			}
			this.spawnAtLocation(NetherItems.nether_painting);
		}
	}
	
	@Override
	public Packet<?> getAddEntityPacket()
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}*/
}
