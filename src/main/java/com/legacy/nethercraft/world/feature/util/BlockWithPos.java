package com.legacy.nethercraft.world.feature.util;

import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.core.BlockPos;

public class BlockWithPos
{
	private BlockState state;
	private int x, y, z;

	public BlockWithPos(BlockState state, BlockPos pos)
	{
		new BlockWithPos(state, pos.getX(), pos.getY(), pos.getZ());
	}

	public BlockWithPos(BlockState state, int x, int y, int z)
	{
		this.state = state;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public BlockState getState()
	{
		return this.state;
	}

	public BlockPos getPos()
	{
		return new BlockPos(x, y, z);
	}
}
