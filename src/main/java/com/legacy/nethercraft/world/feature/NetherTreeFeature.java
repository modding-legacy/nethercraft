package com.legacy.nethercraft.world.feature;

import com.mojang.serialization.Codec;

import net.minecraft.world.level.levelgen.feature.TreeFeature;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;

@Deprecated
public class NetherTreeFeature extends TreeFeature
{
	public NetherTreeFeature(Codec<TreeConfiguration> config)
	{
		super(config);
	}

	/*@Override
	public boolean doPlace(LevelSimulatedRW generationReader, Random rand, BlockPos positionIn, Set<BlockPos> p_225557_4_, Set<BlockPos> p_225557_5_, BoundingBox boundingBoxIn, TreeConfiguration configIn)
	{
		int i = configIn.trunkPlacer.getTreeHeight(rand);
		int j = configIn.foliagePlacer.foliageHeight(rand, i, configIn);
		int k = i - j;
		int l = configIn.foliagePlacer.foliageRadius(rand, k);
		BlockPos blockpos = positionIn;
	
		if (blockpos.getY() >= 1 && blockpos.getY() + i + 1 <= 256)
		{
			if (!canPlaceOnNether(generationReader, blockpos.below()))
			{
				return false;
			}
			else
			{
				OptionalInt optionalint = configIn.minimumSize.minClippedHeight();
				int l1 = this.getMaxFreeTreeHeight(generationReader, i, blockpos, configIn);
				if (l1 >= i || optionalint.isPresent() && l1 >= optionalint.getAsInt())
				{
					List<FoliagePlacer.FoliageAttachment> list = configIn.trunkPlacer.placeTrunk(generationReader, rand, l1, blockpos, p_225557_4_, boundingBoxIn, configIn);
					list.forEach((p_236407_8_) ->
					{
						configIn.foliagePlacer.createFoliage(generationReader, rand, configIn, l1, p_236407_8_, j, l, p_225557_5_, boundingBoxIn);
					});
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}
	
	private static boolean canPlaceOnNether(LevelSimulatedReader worldReader, BlockPos posIn)
	{
		return worldReader.isStateAtPosition(posIn, (state) ->
		{
			Block block = state.getBlock();
			return isDirt(block) || block == Blocks.FARMLAND || block == NCBlocks.nether_dirt;
		});
	}*/
}
