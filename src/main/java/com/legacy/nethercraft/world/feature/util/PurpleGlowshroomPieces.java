package com.legacy.nethercraft.world.feature.util;

import com.legacy.nethercraft.registry.NCBlocks;

import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.HugeMushroomBlock;
import net.minecraft.core.BlockPos;

public class PurpleGlowshroomPieces 
{	
	public static BlockLayout top;
	
	private final static BlockState MUSHROOM = NCBlocks.purple_glowshroom_block.defaultBlockState().setValue(HugeMushroomBlock.DOWN, false);
	private final static BlockState STEM = NCBlocks.glowshroom_stem.defaultBlockState().setValue(HugeMushroomBlock.DOWN, false).setValue(HugeMushroomBlock.UP, false);

	private final static BlockState MUSHROOM_UP = MUSHROOM.setValue(HugeMushroomBlock.NORTH, false).setValue(HugeMushroomBlock.SOUTH, false).setValue(HugeMushroomBlock.EAST, false).setValue(HugeMushroomBlock.WEST, false);

	public static void init()
	{
	}

	static
	{
		createPieces();
	}

	public static void createPieces()
	{
		BlockState[] states = new BlockState[] {
				MUSHROOM,
				STEM,
				MUSHROOM_UP
			};
		String[][] pallet = new String[][] {
			{
				"  00000  ",
				" 0222220 ",
				"022   220",
				"02     20",
				"02  1  20",
				"02     20",
				"022   220",
				" 0222220 ",
				"  00000  "
			},
			{
				"         ",
				"         ",
				"   000   ",
				"  00000  ",
				"  00000  ",
				"  00000  ",
				"   000   ",
				"         ",
				"         "
			}
		};
		
		top = new BlockLayout(states.clone(), pallet.clone(), new BlockPos(1, 0, 1));
	}
}
