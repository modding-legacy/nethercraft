package com.legacy.nethercraft.world.feature.util;

import com.legacy.nethercraft.registry.NCBlocks;

import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.HugeMushroomBlock;
import net.minecraft.core.BlockPos;

public class GreenGlowshroomPieces 
{	
	public static BlockLayout top, extraStates;
	
	private final static BlockState MUSHROOM = NCBlocks.green_glowshroom_block.defaultBlockState().setValue(HugeMushroomBlock.DOWN, false).setValue(HugeMushroomBlock.UP, true);
	private final static BlockState STEM = NCBlocks.glowshroom_stem.defaultBlockState().setValue(HugeMushroomBlock.DOWN, false).setValue(HugeMushroomBlock.UP, false);
	
	private final static BlockState MUSHROOM_ALL = MUSHROOM.setValue(HugeMushroomBlock.NORTH, true).setValue(HugeMushroomBlock.SOUTH, true).setValue(HugeMushroomBlock.EAST, true).setValue(HugeMushroomBlock.WEST, true);

	private final static BlockState MUSHROOM_EAST = MUSHROOM_ALL.setValue(HugeMushroomBlock.EAST, false);
	private final static BlockState MUSHROOM_WEST = MUSHROOM_ALL.setValue(HugeMushroomBlock.WEST, false);
	
	private final static BlockState MUSHROOM_NORTH = MUSHROOM_ALL.setValue(HugeMushroomBlock.NORTH, false);
	private final static BlockState MUSHROOM_NORTH_EAST = MUSHROOM_NORTH.setValue(HugeMushroomBlock.EAST, false);
	private final static BlockState MUSHROOM_NORTH_WEST = MUSHROOM_NORTH.setValue(HugeMushroomBlock.WEST, false);
	
	private final static BlockState MUSHROOM_SOUTH = MUSHROOM_ALL.setValue(HugeMushroomBlock.SOUTH, false);
	private final static BlockState MUSHROOM_SOUTH_EAST = MUSHROOM_SOUTH.setValue(HugeMushroomBlock.EAST, false);
	private final static BlockState MUSHROOM_SOUTH_WEST = MUSHROOM_SOUTH.setValue(HugeMushroomBlock.WEST, false);

	public static void init()
	{
	}

	static
	{
		createPieces();
	}

	
	/**
	 * W
	 *S  N
	 * E
	 */
	public static void createPieces()
	{
		BlockState[] states = new BlockState[] {
				MUSHROOM, // 0
				STEM, // 1
				MUSHROOM_NORTH, // 2
				MUSHROOM_SOUTH, // 3
				MUSHROOM_EAST, // 4
				MUSHROOM_WEST, // 5
			};
		
		String[][] pallet = new String[][] {
			{
				"   3   ",
				"       ",
				"       ",
				"4  1  5",
				"       ",
				"       ",
				"   2   ",
			},
			{
				"       ",
				"   3   ",
				"       ",
				" 4 1 5 ",
				"       ",
				"   2   ",
				"       "
			},
			{
				"       ",
				"   3   ",
				"       ",
				" 4 1 5 ",
				"       ",
				"   2   ",
				"       "
			},
			{
				"       ",
				"       ",
				"   3   ",
				"  415  ",
				"   2   ",
				"       ",
				"       "
			},
			{
				"       ",
				"       ",
				"   0   ",
				"  010  ",
				"   0   ",
				"       ",
				"       ",
			},
			{
				"       ",
				"       ",
				"       ",
				"   0   ",
				"       ",
				"       ",
				"       "
			}
		};
		
		top = new BlockLayout(states.clone(), pallet.clone(), new BlockPos(1, 0, 1));
		
		states = new BlockState[] {
				MUSHROOM, // 0
				MUSHROOM_NORTH_EAST, // 1
				MUSHROOM_NORTH_WEST, // 2
				MUSHROOM_SOUTH_EAST, // 3
				MUSHROOM_SOUTH_WEST // 4
			};
		
		pallet = new String[][] {
			{
				"       ",
				"  3 4  ",
				" 3   4 ",
				"       ",
				" 1   2 ",
				"  1 2  ",
				"       ",
			},
			{
				"       ",
				"  3 4  ",
				" 3   4 ",
				"       ",
				" 1   2 ",
				"  1 2  ",
				"       "
			},
			{
				"       ",
				"       ",
				"  3 4  ",
				"       ",
				"  1 2  ",
				"       ",
				"       "
			},
			{
				"       ",
				"       ",
				"  3 4  ",
				"       ",
				"  1 2  ",
				"       ",
				"       "
			}
		};

		extraStates = new BlockLayout(states.clone(), pallet.clone(), new BlockPos(1, 0, 1));
	}
}
