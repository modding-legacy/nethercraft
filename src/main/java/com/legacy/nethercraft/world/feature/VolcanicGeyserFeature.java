package com.legacy.nethercraft.world.feature;

import java.util.List;

import com.legacy.nethercraft.registry.NCBlocks;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;
import net.minecraft.world.level.material.Fluids;

public class VolcanicGeyserFeature extends Feature<NoneFeatureConfiguration>
{
	public VolcanicGeyserFeature(Codec<NoneFeatureConfiguration> codecIn)
	{
		super(codecIn);
	}

	@Override
	public boolean place(FeaturePlaceContext<NoneFeatureConfiguration> context)
	{
		WorldGenLevel worldIn = context.level();
		BlockPos pos = context.origin();
		RandomSource rand = context.random();
		
		List<Block> allowedBlocks = List.of(Blocks.NETHERRACK, Blocks.MAGMA_BLOCK, NCBlocks.heat_sand);

		int safetyDistance = 2;

		if (!allowedBlocks.contains(worldIn.getBlockState(pos.below()).getBlock()))
			return false;

		if (worldIn.getBlockState(pos.offset(safetyDistance, -1, safetyDistance)).isAir() || worldIn.getBlockState(pos.offset(-safetyDistance, -1, -safetyDistance)).isAir() || worldIn.getBlockState(pos.offset(safetyDistance, -1, -safetyDistance)).isAir() || worldIn.getBlockState(pos.offset(-3, -1, safetyDistance)).isAir())
			return false;

		BlockState state = Blocks.BASALT.defaultBlockState();

		int width = 2 + (rand.nextBoolean() ? 1 : 0);
		int bottomWidth = width + 1;
		for (int x = -bottomWidth; x <= bottomWidth; x++)
		{
			for (int z = -bottomWidth; z <= bottomWidth; z++)
			{
				worldIn.setBlock(pos.offset(x, -1, z), rand.nextFloat() < 0.40F ? (rand.nextBoolean() ? state : NCBlocks.heat_sand.defaultBlockState()) : Blocks.AIR.defaultBlockState(), z);
			}
		}

		for (int x = -width; x <= width; x++)
		{
			for (int z = -width; z <= width; z++)
			{
				if (!(Math.abs(x) == width && Math.abs(x) == Math.abs(z)))
				{
					worldIn.setBlock(pos.offset(x, -1, z), state, z);
					worldIn.setBlock(pos.offset(x, 0, z), state, z);

					if (!(Math.abs(x) == width ^ Math.abs(z) == width))
					{
						worldIn.setBlock(pos.offset(x, 2, z), state, z);
						worldIn.setBlock(pos.offset(x, 1, z), state, z);

						if (rand.nextFloat() < 0.3F)
						{
							if (rand.nextBoolean())
								worldIn.setBlock(pos.offset(x, 3, z), state, z);

							BlockState lava = Blocks.LAVA.defaultBlockState();
							worldIn.setBlock(pos.offset(0, 3, 0), lava, 2);

							worldIn.scheduleTick(pos.offset(0, 3, 0), Fluids.LAVA, 0);
						}
						else if (rand.nextBoolean())
							worldIn.setBlock(pos.offset(x, 3, z), Blocks.MAGMA_BLOCK.defaultBlockState(), z);

					}
					else if (rand.nextBoolean())
					{
						worldIn.setBlock(pos.offset(x, 1, z), state, z);
					}
				}
			}
		}

		return true;
	}
}