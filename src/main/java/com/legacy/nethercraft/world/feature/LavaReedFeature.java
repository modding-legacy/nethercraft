package com.legacy.nethercraft.world.feature;

import com.legacy.nethercraft.registry.NCBlocks;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;

public class LavaReedFeature extends Feature<NoneFeatureConfiguration>
{
	public LavaReedFeature(Codec<NoneFeatureConfiguration> config)
	{
		super(config);
	}

	// place
	@Override
	public boolean place(FeaturePlaceContext<NoneFeatureConfiguration> context)
	{
		WorldGenLevel level = context.level();
		BlockPos pos = context.origin();
		RandomSource rand = context.random();

		int i = 0;
		for (int j = 0; j < 20; ++j)
		{
			BlockPos blockpos = pos.offset(rand.nextInt(4) - rand.nextInt(4), 0, rand.nextInt(4) - rand.nextInt(4));
			if (level.isEmptyBlock(blockpos))
			{
				BlockPos blockpos1 = blockpos.below();
				if (level.getFluidState(blockpos1.west()).is(FluidTags.LAVA) || level.getFluidState(blockpos1.east()).is(FluidTags.LAVA) || level.getFluidState(blockpos1.north()).is(FluidTags.LAVA) || level.getFluidState(blockpos1.south()).is(FluidTags.LAVA))
				{
					int k = 2 + rand.nextInt(rand.nextInt(3) + 1);
					for (int l = 0; l < k; ++l)
					{
						if (Blocks.SUGAR_CANE.defaultBlockState().canSurvive(level, blockpos) || NCBlocks.lava_reeds.defaultBlockState().canSurvive(level, blockpos))
						{
							level.setBlock(blockpos.above(l), NCBlocks.lava_reeds.defaultBlockState(), 2);
							++i;
						}
					}
				}
			}
		}
		return i > 0;
	}
}