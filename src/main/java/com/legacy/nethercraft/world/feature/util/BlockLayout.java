package com.legacy.nethercraft.world.feature.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.WorldGenLevel;

@SuppressWarnings("deprecation")
public class BlockLayout
{
	private List<BlockWithPos> blocks = new ArrayList<BlockWithPos>();
	private String[][] strings;
	private BlockState[] states;
	private BlockPos center;
	private Random rand = new Random();

	public BlockLayout(BlockState[] states, String[][] strings, BlockPos center)
	{
		this.states = states;
		this.strings = strings;
		this.center = center;
		registerBlocks();
	}

	private void registerBlocks() // Y POS IS NOT CHANGING
	{
		int z = 0;
		int y = 0;

		for (String[] s : this.strings)
		{
			for (String d : s)
			{
				for (int i = 0; i < d.length(); i++)
				{
					if (d.charAt(i) != ' ' && getInt(d, i) < 10)
					{
						try
						{
							BlockWithPos bwp = new BlockWithPos(this.states[getInt(d, i)], i, y, z);
							blocks.add(bwp);
						}
						catch (IndexOutOfBoundsException e)
						{
							System.out.println(e);
						}
					}
				}
				z++;
			}
			z = 0;
			y++;
		}
	}

	public void placeBlocks(WorldGenLevel worldIn, BlockPos pos)
	{
		placeBlocks(worldIn, pos, false);
	}

	public void placeBlocks(WorldGenLevel worldIn, BlockPos pos, float decay)
	{
		placeBlocks(worldIn, pos, false, decay);
	}

	public void placeBlocks(WorldGenLevel worldIn, BlockPos pos, Rotation rot)
	{
		placeBlocks(worldIn, pos, false, rot);
	}

	public void placeBlocks(WorldGenLevel worldIn, BlockPos pos, Rotation rot, float decay)
	{
		placeBlocks(worldIn, pos, false, rot, decay);
	}

	public void placeBlocks(WorldGenLevel worldIn, BlockPos pos, boolean replaceBlocks)
	{
		pos = pos.subtract(this.center);

		if (replaceBlocks)
		{
			for (BlockWithPos b : blocks)
			{
				worldIn.setBlock(pos.offset(b.getPos()), b.getState(), 2);
			}
		}
		else
		{
			for (BlockWithPos b : blocks)
			{
				BlockPos pos1 = pos.offset(b.getPos());

				if (!worldIn.getBlockState(pos1).isSolidRender())
				{
					worldIn.setBlock(pos1, b.getState(), 2);
				}
			}
		}
	}

	public void placeBlocks(WorldGenLevel worldIn, BlockPos pos, boolean replaceBlocks, Rotation rot)
	{
		pos = pos.subtract(rotatedBlockPos(this.center, rot));

		if (replaceBlocks)
		{
			for (BlockWithPos b : blocks)
			{
				worldIn.setBlock(pos.offset(rotatedBlockPos(b.getPos(), rot)), b.getState().rotate(rot), 2);
			}
		}
		else
		{
			for (BlockWithPos b : blocks)
			{
				BlockPos pos1 = pos.offset(rotatedBlockPos(b.getPos(), rot));

				if (!worldIn.getBlockState(pos1).isSolidRender())
				{
					worldIn.setBlock(pos1, b.getState().rotate(rot), 2);
				}
			}
		}
	}

	public void placeBlocks(WorldGenLevel worldIn, BlockPos pos, boolean replaceBlocks, float decay)
	{
		pos = pos.subtract(this.center);

		if (replaceBlocks)
		{
			for (BlockWithPos b : blocks)
			{
				if (rand.nextFloat() > decay)
				{
					worldIn.setBlock(pos.offset(b.getPos()), b.getState(), 2);
				}
			}
		}
		else
		{
			for (BlockWithPos b : blocks)
			{
				if (rand.nextFloat() > decay)
				{
					BlockPos pos1 = pos.offset(b.getPos());

					if (!worldIn.getBlockState(pos1).isSolidRender())
					{
						worldIn.setBlock(pos1, b.getState(), 2);
					}
				}
			}
		}
	}

	public void placeBlocks(WorldGenLevel worldIn, BlockPos pos, boolean replaceBlocks, Rotation rot, float decay)
	{
		pos = pos.subtract(rotatedBlockPos(this.center, rot));

		if (replaceBlocks)
		{
			for (BlockWithPos b : blocks)
			{
				if (rand.nextFloat() > decay)
				{
					worldIn.setBlock(pos.offset(rotatedBlockPos(b.getPos(), rot)), b.getState().rotate(rot), 2);
				}
			}
		}
		else
		{
			for (BlockWithPos b : blocks)
			{
				if (rand.nextFloat() > decay)
				{
					BlockPos pos1 = pos.offset(rotatedBlockPos(b.getPos(), rot));

					if (!worldIn.getBlockState(pos1).isSolidRender())
					{
						worldIn.setBlock(pos1, b.getState().rotate(rot), 2);
					}
				}
			}
		}
	}

	private int getInt(String s, int i)
	{
		try
		{
			return Integer.parseInt(s.substring(i, i + 1));
		}
		catch (NumberFormatException e)
		{
			return 0;
		}
		catch (Exception e)
		{
			return 0;
		}
	}

	private BlockPos rotatedBlockPos(BlockPos pos, Rotation rotationIn)
	{
		int i = pos.getX();
		int j = pos.getY();
		int k = pos.getZ();
		boolean flag = true;

		switch (rotationIn)
		{
		case COUNTERCLOCKWISE_90:
			return new BlockPos(k, j, -i);
		case CLOCKWISE_90:
			return new BlockPos(-k, j, i);
		case CLOCKWISE_180:
			return new BlockPos(-i, j, -k);
		default:
			return flag ? new BlockPos(i, j, k) : pos;
		}
	}
}
