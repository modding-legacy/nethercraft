package com.legacy.nethercraft.world.feature;

import com.legacy.nethercraft.data.NCTags;
import com.legacy.nethercraft.registry.NCBlocks;
import com.legacy.nethercraft.world.feature.util.PurpleGlowshroomPieces;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.HugeMushroomBlock;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;

public class HugePurpleGlowshroomFeature extends Feature<NoneFeatureConfiguration>
{
	public HugePurpleGlowshroomFeature(Codec<NoneFeatureConfiguration> codecIn)
	{
		super(codecIn);
	}

	@Override
	public boolean place(FeaturePlaceContext<NoneFeatureConfiguration> context)
	{
		WorldGenLevel level = context.level();
		BlockPos pos = context.origin();
		RandomSource rand = context.random();

		if (level.getBlockState(pos.below()).is(NCTags.Blocks.GLOWSHROOM_PLANTABLE_ON) && !level.getBlockState(pos).isSolidRender())
		{
			int height = this.getStemHeight(rand);

			for (int y = 0; y < height + 2; ++y)
			{
				if (!level.getBlockState(pos.offset(0, y, 0)).isAir())
					return false;

				for (int x = -1; x < 1; ++x)
					for (int z = -1; z < 1; ++z)
						if (level.getBlockState(pos.offset(x, y, z)).getBlock() instanceof HugeMushroomBlock)
							return false;
			}

			PurpleGlowshroomPieces.top.placeBlocks(level, pos.offset(-3, height, -3));

			for (int y = 0; y < height; ++y)
				this.setBlock(level, pos.offset(0, y, 0), NCBlocks.glowshroom_stem.defaultBlockState().setValue(HugeMushroomBlock.DOWN, false).setValue(HugeMushroomBlock.UP, false));

			return true;
		}

		return false;
	}

	protected int getStemHeight(RandomSource randIn)
	{
		return 3 + randIn.nextInt(7);
	}
}